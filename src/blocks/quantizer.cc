/* SF_Quantizer
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/quantizer.hh>

SF_Quantizer::SF_Quantizer (const char* name)
    : SF_Block (false, false,	// no source nor sink
		"Quantizer",
		name),
      s_y (0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    in_bits = new SF_Input_Terminal ("bits", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_input_terminal (in_bits);
    add_output_terminal (out_y);
}

SF_Quantizer::~SF_Quantizer ()
{
    remove_input_terminal (in_x);
    remove_input_terminal (in_bits);
    remove_output_terminal (out_y);
    delete in_x;
    delete in_bits;
    delete out_y;
    in_x = 0;
    in_bits = 0;
    out_y = 0;
}

void
SF_Quantizer::initialize ()
{
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Quantizer::execute ()
{

    SF_Frame *x, *b;
    SF_Sample *s_x, *s_b;
    SF_Length i, l;
    SF_Sample delta;

    x = in_x->get_source_frame (0);
    b = in_bits->get_source_frame (0);
    s_x = x->get_signal ();
    s_b = b->get_signal ();
    l = out_y->get_frame ()->get_num_rows ();

    for (i = 0; i < l; i++)
    {
	SF_Sample bi;  // integer approximation for number of bits
	bi = rint (s_b[i]);
	// (FIXME): determine if s_b[i] is "close enough" to integer
	if (fabs (bi - s_b[i]) >= 0.1)
	{
	    throw SF_Exception (this, "Number of bits must be integer");
	}
	// 1-bit quantizer is an exception
	if (bi == 1)
	{
	    s_y[i] = copysign (1.0, s_x[i]);
	}
	else if (bi < 1 || bi > 32)
	{
	    throw SF_Exception (this, "Number of bits must be between 1 and 32");
	}
	else
	{
	    delta = pow (2, 1 - bi);
	    s_y[i] = delta * floor (s_x[i] / delta + 0.5);
	}
    }
}

void
SF_Quantizer::finish ()
{
}

/* EOF */
