/* SF_Attack_Release
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/attack_release.hh>

#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)>(b))?(b):(a))

SF_Attack_Release::SF_Attack_Release (const char* name)
    : SF_Block (false,
		false,
		"Attack/release",
		name),
      s_y(0),
      prev_y (0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    in_attack = new SF_Input_Terminal ("attack", this, false);
    in_release = new SF_Input_Terminal ("release", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_input_terminal (in_attack);
    add_input_terminal (in_release);
    add_output_terminal (out_y);
}

SF_Attack_Release::~SF_Attack_Release ()
{
    remove_input_terminal (in_x);
    remove_input_terminal (in_attack);
    remove_input_terminal (in_release);
    remove_output_terminal (out_y);
    delete in_x;
    delete in_attack;
    delete in_release;
    delete out_y;
    in_x = 0;
    in_attack = 0;
    in_release = 0;
    out_y = 0;
}

void
SF_Attack_Release::initialize ()
{
    s_x = in_x->get_source_frame (0)->get_signal ();
    s_attack = in_attack->get_source_frame (0)->get_signal ();
    s_release = in_release->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
    prev_y = 0;
    time_k = in_x->get_sample_rate () / 1000;
}

void
SF_Attack_Release::execute ()
{
    SF_Length i, l;
    SF_Length al, rl; // attack portion length, release portion length
    SF_Coefficient ak, rk; // attack portion signal coefficient, release ditto
    l = out_y->get_frame ()->get_num_rows ();

    // FIXME: currently attack and release times are not observed during attack
    // and release portions
    al = (SF_Length) rint (s_attack[0] * time_k);
    rl = (SF_Length) rint (s_release[0] * time_k);

    for (i = 0; i < l; i++)
    {
	// Check for proper attack and release times
	if (s_attack[i] <= 0 || s_release[i] <= 0)
	{
	    throw SF_Exception (this, "Nonpositive attack and/or release time(s)");
	}

	if (s_x[i] > prev_y)
	{
	    // input signal is above past output
	    if (al > 0)
	    {
		// within attack portion
		ak = pow ((s_x[i] / prev_y), (1.0 / (SF_Coefficient)al));
		s_y[i] = prev_y * ak;
		s_y[i] = min (s_y[i], s_x[i]);
		rl = (SF_Length) rint (s_release[i] * time_k);
		al = al - 1;
	    }
	    else
	    {
		// end of attack portion: just pass the signal through
		// unaffected
		s_y[i] = s_x[i];
	    }
	}
	else if (s_x[i] < prev_y)
	{
	    // input signal is below past output
	    if (rl > 0)
	    {
		// within release portion
		rk = pow ((s_x[i] / prev_y), (1.0 / (SF_Coefficient)rl));
		s_y[i] = prev_y * rk;
		s_y[i] = max (s_y[i], s_x[i]);
		al = (SF_Length) rint (s_attack[i] * time_k);
		rl = rl - 1;
	    }
	    else
	    {
		// end of release portion: just pass the signal through
		// unaffected
		s_y[i] = s_x[i];
	    }
	}
	else
	{
	    // input signal output signal follows the input signal
	    al = (SF_Length) rint (s_attack[i] * time_k);
	    rl = (SF_Length) rint (s_release[i] * time_k);
	    s_y[i] = s_x[i];
	}
	prev_y = s_y[i];
    }
}

void
SF_Attack_Release::finish ()
{
}

/* EOF */
