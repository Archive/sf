/* SF_Biquad_Filter
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/biquad_filter.hh>

SF_Biquad_Filter::SF_Biquad_Filter (bool producer,
				    bool consumer,
				    const char* class_name,
				    const char* name)
    : SF_Block (producer, consumer, class_name, name),
      a_0 (0), a_1 (0), a_2 (0), b_0 (0), b_1 (0), b_2 (0),
      fs (0), s_x (0), s_y (0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_output_terminal (out_y);
    past_x[0] = 0;
    past_x[1] = 0;
    past_y[0] = 0;
    past_y[1] = 0;
}

SF_Biquad_Filter::~SF_Biquad_Filter ()
{
    remove_input_terminal (in_x);
    remove_output_terminal (out_y);
    delete in_x;
    delete out_y;
    in_x = 0;
    out_y = 0;
}

void
SF_Biquad_Filter::initialize ()
{
    fs = in_x->get_sample_rate ();
    s_x = in_x->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
    past_x[0] = 0;
    past_x[1] = 0;
    past_y[0] = 0;
    past_y[1] = 0;
}

void
SF_Biquad_Filter::execute ()
{
}

void
SF_Biquad_Filter::finish ()
{
}

/* EOF */
