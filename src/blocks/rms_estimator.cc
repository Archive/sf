/* SF_Rms_Estimator
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/rms_estimator.hh>

SF_Rms_Estimator::SF_Rms_Estimator (const char* name)
    : SF_Block (false,
		false,
		"RMS estimator",
		name),
      b0 (0.01),
      a1 (-0.99),
      past_y (0),
      s_x (0),
      s_rms (0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    out_rms = new SF_Output_Terminal ("RMS", this);
    add_input_terminal (in_x);
    add_output_terminal (out_rms);
}

SF_Rms_Estimator::~SF_Rms_Estimator ()
{
    remove_input_terminal (in_x);
    remove_output_terminal (out_rms);
    delete in_x;
    delete out_rms;
    in_x = 0;
    out_rms = 0;
}

void
SF_Rms_Estimator::initialize ()
{
    s_x = in_x->get_source_frame (0)->get_signal ();
    s_rms = out_rms->get_frame ()->get_signal ();
    past_y = 0;
}

void
SF_Rms_Estimator::execute ()
{
    SF_Length i, l;
    l = out_rms->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	SF_Coefficient rms;
	rms = pow (s_x[i], 2);		// the 'square'
	rms = b0 * rms - a1 * past_y;	// the 'mean'
	past_y = rms;
	rms = sqrt (rms);		// the 'root'

	// 1st-order filter => no overshoot => no complex numbers
	s_rms[i] = rms;
    }
}

void
SF_Rms_Estimator::finish ()
{
}

/* EOF */
