/* SF_Peaking_Filter
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/peaking_filter.hh>

SF_Peaking_Filter::SF_Peaking_Filter (const char* name)
    : SF_Biquad_Filter (false,
			false,
			"Peaking filter",
			name),
      s_f1 (0), s_f2 (0), s_gain (0)
{
    in_f1 = new SF_Input_Terminal ("f1", this, false);
    in_f2 = new SF_Input_Terminal ("f2", this, false);
    in_gain = new SF_Input_Terminal ("gain", this, false);
    add_input_terminal (in_f1);
    add_input_terminal (in_f2);
    add_input_terminal (in_gain);
}

SF_Peaking_Filter::~SF_Peaking_Filter ()
{
    remove_input_terminal (in_f1);
    remove_input_terminal (in_f2);
    remove_input_terminal (in_gain);
    delete in_f1;
    delete in_f2;
    delete in_gain;
    in_f1 = 0;
    in_f2 = 0;
    in_gain = 0;
}

void
SF_Peaking_Filter::initialize ()
{
    SF_Biquad_Filter::initialize ();
    s_f1 = in_f1->get_source_frame (0)->get_signal ();
    s_f2 = in_f2->get_source_frame (0)->get_signal ();
    s_gain = in_gain->get_source_frame (0)->get_signal ();
}

void
SF_Peaking_Filter::execute ()
{
    SF_Biquad_Filter::execute ();
    SF_Length i, l;
    l = out_y->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_f1[i] >= s_f2[i])
	{
	    throw SF_Exception (this, "Lower frequency must be less than the upper frequency");
	}
	if (s_f1[i] < 0)
	{
	    throw SF_Exception (this, "Frequencies must be non-negative");
	}	   
	if (s_f2[i] > (fs / 2))
	{
	    throw SF_Exception (this, "Frequencies must be less than or equal to half of the sample frequency");
	}	   
	calculate_coefficients (fs, s_f1[i], s_f2[i], s_gain[i]);
	s_y[i] = filter (s_x[i]);
    }
}

void
SF_Peaking_Filter::finish ()
{
    SF_Biquad_Filter::finish ();
}

/* EOF */
