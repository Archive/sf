/* SF_Audio_Output
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This audio is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>
#include <stdio.h> // sprintf ()

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>
#include <sf/version.h>

#include <sf/blocks/audio_output.hh>

#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)>(b))?(b):(a))

SF_Audio_Output::SF_Audio_Output (const char* name)
    : SF_Block (false, true,	// we're a consumer
		"Audio output",
		name),
      clipping_warning_printed (false),
      nonfinity_warning_printed (false)
{
    // begin with 1 output channel "ch0"
    set_num_channels (1);
}

SF_Audio_Output::~SF_Audio_Output ()
{
    // wipe out the channels array
    int i;
    for (i = 0; i < num_channels; i++)
    {
	const char* name = 0;
	SF_Input_Terminal* term;
	term = channels[i];
	remove_input_terminal (term);
	name = (char*) term->get_name ();
	delete term;
	delete[] name;
    }
    delete[] channels;
    channels = 0;
}

void
SF_Audio_Output::initialize ()
{
    clipping_warning_printed = false;
    nonfinity_warning_printed = false;
}

void
SF_Audio_Output::execute ()
{
    SF_Length i, l;
    SF_Sample* s_ch = 0;
    int ch;

    // there's always at least 1 channel
    l = channels[0]->get_source_frame (0)->get_num_rows ();

    // traverse each channel
    for (ch = 0; ch < num_channels; ch++)
    {
	// fetch frame buffer to get samples from
	s_ch = channels[ch]->get_source_frame (0)->get_signal ();

	for (i = 0; i < l; i++)
	{
	    SF_Sample samp;
	    int samp_int; // N-bit integer representation

	    // read SF_Sample from the frame buffer
	    samp = s_ch[i];
	    // check for NaN's and infinite numbers
	    if (!nonfinity_warning_printed && !finite (samp))
	    {
		samp_int = 0;
		nonfinity_warning_printed = true;
		SF_Warning (this, "Output signal infinite or NaN");
	    }
	    // convert floating-point input to 16-bit (FIXME) integer samples;
	    // check for clipping
	    else if (samp >= 1)
	    {
		samp_int = 32767;
		if (!clipping_warning_printed)
		{
		    clipping_warning_printed = true;
		    SF_Warning (this, "Output signal clipped");
		}
	    }
	    else if (samp <= -1)
	    {
		samp_int = -32768;
		if (!clipping_warning_printed)
		{
		    clipping_warning_printed = true;
		    SF_Warning (this, "Output signal clipped");
		}
	    }
	    else
	    {
		samp_int = (int)(samp * 32768);
	    }
	    // FIXME: write samp_int to audio hardware (and do buffering? what
	    // do I know)
	    // -jams
	}
    }
}

void
SF_Audio_Output::finish ()
{
}

void
SF_Audio_Output::set_num_channels (int num)
    // Set the number channels to output; adjusts the number of input terminals
    // in the block.  The terminals are named ch0, ch1, ch2, ...
    // FIXME?: redundant code in SF_Multiplexer, SF_Audio_Input and
    // SF_Audio_Output
{
    SF_Input_Terminal** new_channels = 0;
    int i;

    // alter the number of input terminals
    // FIXME: inspect audio hardware for number of channels to allow
    if (num <= 0 || num > 100)
    {
	throw SF_Exception (this, "Number of channels out of bounds");
    }

    // do nothing if asked
    if (num == num_channels)
    {
	return;
    }

    new_channels = new SF_Input_Terminal*[num];
    for (i = 0; i < min (num_channels, num); i++)
    {
	new_channels[i] = channels[i];
    }

    for (i = min (num_channels, num); i < max (num_channels, num); i++)
    {
	char* name = 0;
	SF_Input_Terminal* term;
	if (num > num_channels)
	{
	    // allocate new terminals if asked
	    // compose a name for the terminals
	    name = new char[6]; // "ch100\0"
	    sprintf (name, "ch%d", i);
	    term = new SF_Input_Terminal (name, this, false);
	    new_channels[i] = term;
	    add_input_terminal (term);
	}
	else
	{
	    // free existing terminals if asked
	    term = channels[i];
	    remove_input_terminal (term);
	    name = (char*) term->get_name ();
	    delete term;
	    delete[] name;
	}
    }

    // finally, replace old ch array with the new one
    delete[] channels;
    channels = new_channels;
    num_channels = num;
}

/* EOF */
