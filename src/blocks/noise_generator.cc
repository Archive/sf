/* SF_Noise_Generator
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/noise_generator.hh>

SF_Noise_Generator::SF_Noise_Generator (const char* name)
    : SF_Block (true,	// producer
		false,	// not a consumer
		"Noise generator",
		name),
      s_amp (0), s_y (0), fs (0),
      lcg_i (0),		//seed
      lcg_a (16807),		//constant a = 7^5 = 16807
      lcg_m (2147483647)	//constant m = 2^31 - 1 = 2147483647
{
    in_amp = new SF_Input_Terminal ("amp", this, 1, false);  //Default input = 1
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_amp);
    add_output_terminal (out_y);
}

SF_Noise_Generator::~SF_Noise_Generator ()
{
    remove_input_terminal (in_amp);
    remove_output_terminal (out_y);
    delete in_amp;
    delete out_y;
    in_amp = 0;
    out_y = 0;
}

void
SF_Noise_Generator::initialize ()
{
    // initialize the seed value from clock and process id.
    do
    {
	lcg_i = (long)time (0) ^ ((long)getpid () + ((long)getpid () << 15));
    } while (lcg_i == 0);	//zero not accepted as seed

    // initialize signal pointers
    s_amp = in_amp->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Noise_Generator::execute ()
{
    SF_Length i, l;
    l = out_y->get_frame ()->get_num_rows ();

    for (i = 0; i < l; i++)
    {
	if (s_amp[i] < 0)
	{
	    throw SF_Exception (this, "Amplitude must be non-negative");
	}
	// fill the output frame with LCG sequence
	lcg_i = (lcg_a * lcg_i) % lcg_m;
	s_y[i] = s_amp[i] * (float)lcg_i / (float)(lcg_m - 1);
    }
}

void
SF_Noise_Generator::finish ()
{
}

/* EOF */
