/* SF_Max_Min
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/max_min.hh>

SF_Max_Min::SF_Max_Min (const char* name)
    : SF_Block (false,
		false,
		"Max/min",
		name),
      s_max(0), s_min(0)
{
    in_x = new SF_Input_Terminal ("x", this, true);
    out_max = new SF_Output_Terminal ("max", this);
    out_min = new SF_Output_Terminal ("min", this);
    add_input_terminal (in_x);
    add_output_terminal (out_max);
    add_output_terminal (out_min);
}

SF_Max_Min::~SF_Max_Min ()
{
    remove_input_terminal (in_x);
    remove_output_terminal (out_max);
    remove_output_terminal (out_min);
    delete in_x;
    delete out_max;
    delete out_min;
    in_x = 0;
    out_max = 0;
    out_min = 0;
}

void
SF_Max_Min::initialize ()
{
    s_max = out_max->get_frame ()->get_signal ();
    s_min = out_min->get_frame ()->get_signal ();
}

void
SF_Max_Min::execute ()
{
    SF_Sample *s_x;
    SF_Length j, l;
    int i;
    l = out_max->get_frame ()->get_num_rows ();

    s_x = in_x->get_source_frame (0)->get_signal ();
    for (j = 0; j < l; j++)
    {
	s_max[j] = s_x[j];
	s_min[j] = s_x[j];
    }
	
    for (i = 1; i < in_x->get_num_source_frames (); i++)
    {
	s_x = in_x->get_source_frame (i)->get_signal();
	for (j = 0; j < l; j++)
	{
	    s_max[j] = max (s_max[j], s_x[j]);
	    s_min[j] = min (s_min[j], s_x[j]);
	}
    }
}

void
SF_Max_Min::finish ()
{
}

/* EOF */
