/* SF_Multiplexer
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <stdio.h> // sprintf ()
#include <math.h> // rint ()

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/multiplexer.hh>

#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)>(b))?(b):(a))

SF_Multiplexer::SF_Multiplexer (const char* name)
    : SF_Block (false,
		false,
		"Multiplexer",
		name),
      s_n (0),
      s_y (0)
{
    in_n = new SF_Input_Terminal ("n", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_n);
    add_output_terminal (out_y);
    // begin with 1 channel input "x0"
    set_num_channels (1);
}

SF_Multiplexer::~SF_Multiplexer ()
{
    // wipe out the channels array
    int i;
    for (i = 0; i < num_channels; i++)
    {
	const char* name = 0;
	SF_Input_Terminal* term;
	term = channels[i];
	remove_input_terminal (term);
	name = (char*) term->get_name ();
	delete term;
	delete[] name;
    }
    delete[] channels;
    channels = 0;

    remove_input_terminal (in_n);
    remove_output_terminal (out_y);
    delete in_n;
    delete out_y;
    in_n = 0;
    out_y = 0;
}

void
SF_Multiplexer::initialize ()
{
    s_n = in_n->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Multiplexer::execute ()
{
    SF_Length i, l;
    int ch;
    SF_Sample* s_ch;
    l = out_y->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	ch = (int) rint (s_n[i]);
	if (ch < 0 || ch > num_channels)
	{
	    throw SF_Exception (this, "N input out of bounds");
	}
	s_ch = channels[ch]->get_source_frame(0)->get_signal ();
	s_y[i] = s_ch[i];
    }
}

void
SF_Multiplexer::finish ()
{
}

void
SF_Multiplexer::set_num_channels (int num)
{
    SF_Input_Terminal** new_channels = 0;
    int i;

    // alter the number of input terminals
    if (num <= 0 || num > 100)
    {
	throw SF_Exception (this, "Number of channels out of bounds");
    }

    // do nothing if asked
    if (num == num_channels)
    {
	return;
    }

    new_channels = new SF_Input_Terminal*[num];
    for (i = 0; i < min (num_channels, num); i++)
    {
	new_channels[i] = channels[i];
    }

    for (i = min (num_channels, num); i < max (num_channels, num); i++)
    {
	char* name = 0;
	SF_Input_Terminal* term;
	if (num > num_channels)
	{
	    // allocate new terminals if asked
	    // compose a name for the terminals
	    name = new char[5]; // "x100\0"
	    sprintf (name, "x%d", i);
	    term = new SF_Input_Terminal (name, this, false);
	    new_channels[i] = term;
	    add_input_terminal (term);
	}
	else
	{
	    // free existing terminals if asked
	    term = channels[i];
	    remove_input_terminal (term);
	    name = (char*) term->get_name ();
	    delete term;
	    delete[] name;
	}
    }

    // finally, replace old ch array with the new one
    delete[] channels;
    channels = new_channels;
    num_channels = num;
}

/* EOF */
