/* SF_Negation
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/negation.hh>

SF_Negation::SF_Negation (const char* name)
    : SF_Block (false,
		false,
		"Negation",
		name),
      s_y(0),
      s_x(0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_output_terminal (out_y);
}

SF_Negation::~SF_Negation ()
{
    remove_input_terminal (in_x);
    remove_output_terminal (out_y);
    delete in_x;
    delete out_y;
    in_x = 0;
    out_y = 0;
}

void
SF_Negation::initialize ()
{
    s_y = out_y->get_frame ()->get_signal ();
    s_x = in_x->get_source_frame (0)->get_signal ();
}

void
SF_Negation::execute ()
{
    SF_Length i, l;
    l = out_y->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	s_y[i] = -s_x[i];
    }
}

void
SF_Negation::finish ()
{
}

/* EOF */
