/* SF_Impulse_Generator
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/impulse_generator.hh>

SF_Impulse_Generator::SF_Impulse_Generator (const char* name)
    : SF_Block (true,
		false,
		"Impulse generator",
		name),
      s_y(0), impulse (true)
{
    out_y = new SF_Output_Terminal ("y", this);
    add_output_terminal (out_y);
}

SF_Impulse_Generator::~SF_Impulse_Generator ()
{
    remove_output_terminal (out_y);
    delete out_y;
    out_y = 0;
}

void
SF_Impulse_Generator::initialize ()
{
    s_y = out_y->get_frame ()->get_signal ();
    impulse = true;
}

void
SF_Impulse_Generator::execute ()
{
    SF_Length l = out_y->get_frame ()->get_num_rows ();
    for (int i = 0; i < l; i++)
    {
	s_y[i] = 0;
    }
    if (impulse)
    {
	s_y[0] = 1;
	impulse = false;
    }
}

void
SF_Impulse_Generator::finish ()
{
}

/* EOF */
