/* SF_Variable
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/variable.hh>

SF_Variable::SF_Variable (const char* name)
    : SF_Block (true, false, // We're a producer
		"Variable",
		name),
      s_y(0), value (0)
{
    out_y = new SF_Output_Terminal ("y", this);
    add_output_terminal (out_y);
}

SF_Variable::~SF_Variable ()
{
    remove_output_terminal (out_y);
    delete out_y;
    out_y = 0;
}

void
SF_Variable::initialize ()
{
    SF_Length i, l;
    s_y = out_y->get_frame ()->get_signal ();
    l = out_y->get_frame ()->get_num_rows ();

    // fill the output frame with desired value
    for (i = 0; i < l; i++)
    {
	s_y[i] = value;
    }
}

void
SF_Variable::execute ()
{
    // nothing to do!
}

void
SF_Variable::finish ()
{
}

/* EOF */
