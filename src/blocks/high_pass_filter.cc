/* SF_High_Pass_Filter
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/high_pass_filter.hh>

SF_High_Pass_Filter::SF_High_Pass_Filter (const char* name)
    : SF_Biquad_Filter (false,
			false,
			"High-pass filter",
			name),
      s_freq (0), s_q (0)
{
    in_freq = new SF_Input_Terminal ("freq", this, false);
    in_q = new SF_Input_Terminal ("Q", this, false);
    add_input_terminal (in_freq);
    add_input_terminal (in_q);
}

SF_High_Pass_Filter::~SF_High_Pass_Filter ()
{
    remove_input_terminal (in_freq);
    remove_input_terminal (in_q);
    delete in_freq;
    delete in_q;
    in_freq = 0;
    in_q = 0;
}

void
SF_High_Pass_Filter::initialize ()
{
    SF_Biquad_Filter::initialize ();
    s_freq = in_freq->get_source_frame (0)->get_signal ();
    s_q = in_q->get_source_frame (0)->get_signal ();
}

void
SF_High_Pass_Filter::execute ()
{
    SF_Biquad_Filter::execute ();
    SF_Length l = out_y->get_frame ()->get_num_rows ();
    for (int i = 0; i < l; i++)
    {
	if (s_freq[i] < 0)
	{
	    throw SF_Exception (this, "Frequencies must be non-negative");
	}	   
	if (s_freq[i] > (fs / 2))
	{
	    throw SF_Exception (this, "Frequencies must be less than or equal to half of the sample frequency");
	}	   
	calculate_coefficients (fs, s_freq[i], s_q[i]);
	s_y[i] = filter (s_x[i]);
    }
}

void
SF_High_Pass_Filter::finish ()
{
    SF_Biquad_Filter::finish ();
}

/* EOF */
