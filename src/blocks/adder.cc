/* SF_Adder
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/adder.hh>

SF_Adder::SF_Adder (const char* name)
    : SF_Block (false,
		false,
		"Adder",
		name),
      s_y(0),
      initial_value (0)
{
    in_x = new SF_Input_Terminal ("x", this, true);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_output_terminal (out_y);
}

SF_Adder::~SF_Adder ()
{
    remove_input_terminal (in_x);
    remove_output_terminal (out_y);
    delete in_x;
    delete out_y;
    in_x = 0;
    out_y = 0;
}

void
SF_Adder::initialize ()
{
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Adder::execute ()
{
    // Pointer to the signal coming from a wire to the input terminal "x"
    SF_Sample* s_x;
    int i;
    SF_Length j, l;

    // Get the length of the output signal here, since it may change during
    // simulation
    l = out_y->get_frame ()->get_num_rows ();

    // Initialize the output frame to initial_value (default = 0)
    for (j = 0; j < l; j++)
    {
	s_y[j] = initial_value;
    }

    // Add all the signals from all wires connected to "x"
    for (i = 0; i < in_x->get_num_source_frames (); i++)
    {
	s_x = in_x->get_source_frame (i)->get_signal ();
	for (j = 0; j < l; j++)
	{
	    s_y[j] = s_y[j] + s_x[j];
	}
    }
}

void
SF_Adder::finish ()
{
}

/* EOF */
