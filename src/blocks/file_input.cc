/* SF_File_Input
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/file_input.hh>

extern "C"
{
#include <sndlib/sndlib.h>
}

SF_File_Input::SF_File_Input (const char* name)
    : SF_Block (true, false,	// we're a producer
		"File input",
		name),
      file_name (0),
      fd (-1),
      buffer (0),
      chan_buflen (0),
      chan_bufs (0),
      file_chans (0),
      file_fs (0),
      file_length (0),
      coef (0)
{
    out_y = new SF_Output_Terminal ("y", this);
    add_output_terminal (out_y);
}

SF_File_Input::~SF_File_Input ()
{
    remove_output_terminal (out_y);
    delete out_y;
    out_y = 0;

    // just in case
    delete[] buffer;
    delete[] chan_bufs;
    buffer = 0;
    chan_buflen = 0;
    chan_bufs = 0;
}

void
SF_File_Input::finish ()
{
    close_sound_input (fd);
    fd = -1;
    delete[] buffer;
    buffer = 0;
    chan_buflen = 0;
    delete[] chan_bufs;
    chan_bufs = 0;
}

void
SF_File_Input::initialize ()
{
    initialize_sndlib ();

    // open file
    if (file_name == 0)
    {
	throw SF_Exception (this, "No file name specified");
    }
    fd = open_sound_input ((char*)file_name);
    if (fd == -1)
    {
	throw SF_Exception (this, "Unable to open audio input file.");
    }

    // inspect file header for information
    file_fs = sound_srate ((char*)file_name);
    file_chans = sound_chans ((char*)file_name);
    file_length = sound_samples ((char*)file_name) / file_chans;
    coef = pow (2, 1 - ((SF_Sample) sound_bits_per_sample((char*)file_name)));

    // set the output sample rate to that of the file
    out_y->set_sample_rate (file_fs);

    // calculate the size and allocate the read buffers
    chan_bufs = new int*[file_chans];
    chan_buflen = (int) ceil (file_fs * SF_Frame::get_duration () * 1e-3);
    buffer = new int[file_chans * chan_buflen];
    for (int i = 0; i < file_chans; i++)
    {
	chan_bufs[i] = &buffer[i * chan_buflen];
    }
}

void
SF_File_Input::execute ()
{
    SF_Frame *y = out_y->get_frame ();
    SF_Sample *s = y->get_signal ();
    SF_Length l = y->get_num_rows ();
    SF_Length i;

    read_sound (fd, 0, (int)l-1, file_chans, chan_bufs);
    
    // convert 16-bit integer samples into floating-point
    for (i = 0; i < l; i++)
    {
	// take the first channel
	s[i] = coef * ((SF_Sample) chan_bufs[0][i]);
    }
}

const char*
SF_File_Input::get_file_name ()
{
    return file_name;
}

void
SF_File_Input::set_file_name (const char* file_name)
{
    this->file_name = file_name;
}

/* EOF */
