/* SF_Audio_Input
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This audio is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>
#include <stdio.h> // sprintf ()

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/audio_input.hh>

#define max(a,b) (((a)>(b))?(a):(b))
#define min(a,b) (((a)>(b))?(b):(a))

SF_Audio_Input::SF_Audio_Input (const char* name)
    : SF_Block (true, false,	// we're a producer
		"Audio input",
		name),
      coef (0)
{
    // begin with 1 input channel "ch0"
    set_num_channels (1);
}

SF_Audio_Input::~SF_Audio_Input ()
{
    // wipe out the channels array
    int i;
    for (i = 0; i < num_channels; i++)
    {
	const char* name = 0;
	SF_Output_Terminal* term;
	term = channels[i];
	remove_output_terminal (term);
	name = (char*) term->get_name ();
	delete term;
	delete[] name;
    }
    delete[] channels;
    channels = 0;
}

void
SF_Audio_Input::initialize ()
{
    //FIXME: shouldn't we inspect how many bits the audio hardware supports?
    int bits_per_sample = 16;
    coef = pow (2, 1 - ((SF_Sample) bits_per_sample));
}

void
SF_Audio_Input::execute ()
{
    SF_Length i, l;
    SF_Sample* s_ch = 0;
    int ch;

    // there's always at least 1 channel
    l = channels[0]->get_frame ()->get_num_rows ();

    // traverse each channel
    for (ch = 0; ch < num_channels; ch++)
    {
	// fetch frame buffer to put samples to
	s_ch = channels[ch]->get_frame ()->get_signal ();

	for (i = 0; i < l; i++)
	{
	    int samp_int; // N-bit integer representation

	    // FIXME: read sample from audio hw into samp_int (do buffering?)
	    // -jams
	    samp_int = 0;

	    // convert N-bit integer samples into floating-point
	    // write SF_Sample to the frame buffer
	    s_ch[i] = coef * ((SF_Sample) samp_int);
	}
    }
}

void
SF_Audio_Input::finish ()
{
}

void
SF_Audio_Input::set_num_channels (int num)
    // Set the number of channels to input; adjusts the number of output
    // terminals in the block.  The terminals are named ch0, ch1, ch2, ...
    // FIXME?: redundant code in SF_Multiplexer, SF_Audio_Input and
    // SF_Audio_Output
{
    SF_Output_Terminal** new_channels = 0;
    int i;

    // alter the number of input terminals
    // FIXME: inspect audio hardware for number of channels to allow
    if (num <= 0 || num > 100)
    {
	throw SF_Exception (this, "Number of channels out of bounds");
    }

    // do nothing if asked
    if (num == num_channels)
    {
	return;
    }

    new_channels = new SF_Output_Terminal*[num];
    for (i = 0; i < min (num_channels, num); i++)
    {
	new_channels[i] = channels[i];
    }

    for (i = min (num_channels, num); i < max (num_channels, num); i++)
    {
	char* name = 0;
	SF_Output_Terminal* term;
	if (num > num_channels)
	{
	    // allocate new terminals if asked
	    // compose a name for the terminals
	    name = new char[6]; // "ch100\0"
	    sprintf (name, "ch%d", i);
	    term = new SF_Output_Terminal (name, this);
	    new_channels[i] = term;
	    add_output_terminal (term);
	}
	else
	{
	    // free existing terminals if asked
	    term = channels[i];
	    remove_output_terminal (term);
	    name = (char*) term->get_name ();
	    delete term;
	    delete[] name;
	}
    }

    // finally, replace old ch array with the new one
    delete[] channels;
    channels = new_channels;
    num_channels = num;
}

/* EOF */
