/* SF_Delay_Line
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/delay_line.hh>

SF_Delay_Line::SF_Delay_Line (const char* name)
    : SF_Block (false,
		false,
		"Delay line",
		name),
      delay_memory (0),
      delay_memory_length (0),
      delay_line (0),
      fractional_delay_filter_state (0),
      current_index (0),
      fs(0),
      s_x(0),
      s_d(0),
      s_y(0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    in_d = new SF_Input_Terminal ("d", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_input_terminal (in_d);
    add_output_terminal (out_y);
}

SF_Delay_Line::~SF_Delay_Line ()
{
    remove_input_terminal (in_x);
    remove_input_terminal (in_d);
    remove_output_terminal (out_y);
    delete in_x;
    delete in_d;
    delete out_y;
    in_x = 0;
    in_d = 0;
    out_y = 0;
}

void
SF_Delay_Line::initialize ()
{
    fs = in_x->get_sample_rate ();
    s_y = out_y->get_frame ()->get_signal ();
    s_x = in_x->get_source_frame (0)->get_signal ();
    s_d = in_d->get_source_frame (0)->get_signal ();
	
    //time to sample -conversion, extra sample added for the current sample
    delay_memory_length = (unsigned int)ceil (delay_memory * fs * 0.001) + 1;
    delay_memory = (delay_memory_length - 1) / fs * 1000;

    //allocate delay line
    delay_line = new SF_Sample[delay_memory_length];
    current_index = 0;
    fractional_delay_filter_state = 0;

    // fill delay line with zeros
    for (int i = 0; i < delay_memory_length; i++)
    {
	delay_line[i] = 0;
    }
}

void
SF_Delay_Line::execute ()
{
    SF_Length i, l;

    l = out_y->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_d[i] < 0)
	{
	    throw SF_Exception (this, "Negative delay");
	}
	if (s_d[i] > delay_memory)
	{
	    throw SF_Exception (this, "Delay exceeds maximum delay");
	}
	delay_line[current_index] = s_x[i];
	s_y[i] = interpolate_delay_line (s_d[i]);
	current_index = (current_index + 1) % delay_memory_length;
    }
}

SF_Sample
SF_Delay_Line::interpolate_delay_line (SF_Time delay)
    //This function interpolates the delay line i.e. it calculates
    //the integer and the fractional parts of the delay (according
    //to Dattorro, see functional specification).

{
    unsigned int delay_i, i_n, i_n_1;
    double a, c, delay_f;
    SF_Sample v, w;

    a = delay * fs * 0.001;
    delay_i = (unsigned int)floor (a);	// integer part
    delay_f = a - delay_i;		// fractional part
    c = (1 - delay_f) / (1 + delay_f);	// warping factor
    i_n = (current_index - delay_i + delay_memory_length) % delay_memory_length;
    i_n_1 = ((current_index - delay_i - 1 + delay_memory_length)
	     % delay_memory_length);
    w = c * delay_line[i_n] + delay_line[i_n_1];
    v = w - c * fractional_delay_filter_state;
    fractional_delay_filter_state = v;
    return v;
}

void
SF_Delay_Line::finish ()
{
}

/* EOF */
