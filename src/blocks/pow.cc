/* SF_Pow
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/pow.hh>

SF_Pow::SF_Pow (const char* name)
    : SF_Block (false,
		false,
		"Pow",
		name),
      s_y (0),
      in_m (0),
      in_e (0),
      out_y (0)
{
    in_m = new SF_Input_Terminal ("m", this, false);
    in_e = new SF_Input_Terminal ("e", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_m);
    add_input_terminal (in_e);
    add_output_terminal (out_y);
}

SF_Pow::~SF_Pow ()
{
    remove_input_terminal (in_m);
    remove_input_terminal (in_e);
    remove_output_terminal (out_y);
    delete in_m;
    delete in_e;
    delete out_y;
    in_m = 0;
    in_e = 0;
    out_y = 0;
}

void
SF_Pow::initialize ()
{
    // Initialize pointers to input and output (stream) signals
    s_m = in_m->get_source_frame (0)->get_signal ();
    s_e = in_e->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Pow::execute ()
{
    SF_Length i, l;
    l = in_m->get_source_frame (0)->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	s_y[i] = pow (s_m[i], s_e[i]);
    }
}

void
SF_Pow::finish ()
{
}

/* EOF */
