/* SF_Divider
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/divider.hh>

SF_Divider::SF_Divider (const char* name)
    : SF_Block (false,
		false,
		"Divider",
		name),
      s_p (0),
      s_q (0),
      s_y (0)
{
    in_p = new SF_Input_Terminal ("p", this, false);
    in_q = new SF_Input_Terminal ("q", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_p);
    add_input_terminal (in_q);
    add_output_terminal (out_y);
}

SF_Divider::~SF_Divider ()
{
    remove_input_terminal (in_p);
    remove_input_terminal (in_q);
    remove_output_terminal (out_y);
    delete in_p;
    delete in_q;
    delete out_y;
    in_p = 0;
    in_q = 0;
    out_y = 0;
}

void
SF_Divider::initialize ()
{
    s_p = in_p->get_source_frame (0)->get_signal ();
    s_q = in_q->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Divider::execute ()
{
    SF_Length i, l;
    l = out_y->get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_q[i] == 0)
	{
	    throw SF_Math_Exception (this, "Divide by zero");
	}
	s_y[i] = s_p[i] / s_q[i];
    }
}

void
SF_Divider::finish ()
{
}

/* EOF */
