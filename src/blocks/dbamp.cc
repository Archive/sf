/* SF_Dbamp
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/dbamp.hh>

SF_Dbamp::SF_Dbamp (const char* name)
    : SF_Block (false,
		false,
		"dBamp",
		name),
      s_amp (0),
      s_db (0),
      in_amp (0),
      out_db (0)
{
    in_amp = new SF_Input_Terminal ("amp", this, false);
    out_db = new SF_Output_Terminal ("dB", this);
    add_input_terminal (in_amp);
    add_output_terminal (out_db);
}

SF_Dbamp::~SF_Dbamp ()
{
    remove_input_terminal (in_amp);
    remove_output_terminal (out_db);
    delete in_amp;
    delete out_db;
    in_amp = 0;
    out_db = 0;
}

void
SF_Dbamp::initialize ()
{
    // Initialize pointers to input and output (stream) signals
    s_amp = in_amp->get_source_frame (0)->get_signal ();
    s_db = out_db->get_frame ()->get_signal ();
}

void
SF_Dbamp::execute ()
{
    SF_Length i, l;
    l = in_amp->get_source_frame (0)->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_amp[i] <= 0)
	{
	    throw SF_Math_Exception (this, "Log of nonpositive number");
	}
	s_db[i] = 20 * log10 (s_amp[i]);
    }
}

void
SF_Dbamp::finish ()
{
}

/* EOF */
