/* SF_Sample_Hold
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/sample_hold.hh>

SF_Sample_Hold::SF_Sample_Hold (const char* name)
    : SF_Block (false,
		false,
		"Sample&Hold",
		name),
      held_sample (0),
      s_x (0),
      s_gate (0),
      s_y (0),
      in_x (0),
      in_gate (0),
      out_y (0)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    in_gate = new SF_Input_Terminal ("gate", this, false);
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_x);
    add_input_terminal (in_gate);
    add_output_terminal (out_y);
}

SF_Sample_Hold::~SF_Sample_Hold ()
{
    remove_input_terminal (in_x);
    remove_input_terminal (in_gate);
    remove_output_terminal (out_y);
    delete in_x;
    delete in_gate;
    delete out_y;
    in_x = 0;
    in_gate = 0;
    out_y = 0;
}

void
SF_Sample_Hold::initialize ()
{
    // Initialize pointers to input and output (stream) signals
    s_x = in_x->get_source_frame (0)->get_signal ();
    s_gate = in_gate->get_source_frame (0)->get_signal ();
    s_y = out_y->get_frame ()->get_signal ();
}

void
SF_Sample_Hold::execute ()
{
    SF_Length i, l;

    l = in_x->get_source_frame (0)->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_gate[i] != 0)
	{
	    // allow data to pass thru
	    s_y[i] = s_x[i];
	    held_sample = s_x[i];
	}
	else
	{
	    // hold the last sample
	    s_y[i] = held_sample;
	}
    }
}

void
SF_Sample_Hold::finish ()
{
}

/* EOF */
