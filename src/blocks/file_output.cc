/* SF_File_Output
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>
#include <sf/version.h>

#include <sf/blocks/file_output.hh>

extern "C"
{
#include <sndlib/sndlib.h>
}

SF_File_Output::SF_File_Output (const char* name)
    : SF_Block (false, true,	// we're a consumer
		"File output",
		name),
      file_name (0),
      fd (-1),
      buffer (0),
      buflen (0),
      samples_written (0),
      clipping_warning_printed (false)
{
    in_x = new SF_Input_Terminal ("x", this, false);
    add_input_terminal (in_x);
}

SF_File_Output::~SF_File_Output ()
{
    remove_input_terminal (in_x);
    delete in_x;
    in_x = 0;

    if (buffer)
    {
	delete[] buffer;
	buffer = 0;
	buflen = 0;
    }
}

void
SF_File_Output::initialize ()
{
    SF_Frequency fs;

    initialize_sndlib ();

    samples_written = 0;

    // get the sample rate of the input terminal
    fs = in_x->get_sample_rate ();

    // calculate the size and allocate the write buffer
    buflen = (int) ceil (fs * SF_Frame::get_duration () * 1e-3);
    buffer = new int[buflen];

    // open file
    if (file_name == 0)
    {
	throw SF_Exception (this, "No file name specified");
    }
    fd = open_sound_output ((char*)file_name,	// file name
			    (int)fs,		// sample rate
			    1,			// channels
			    snd_16_linear_little_endian,	// data format (AIFF: snd_16_linear)
			    RIFF_sound_file,	// header type (AIFF: AIFF_sound_file)
			    "Synthesized with Sonic Flow " SF_VERSION
			    " (sndlib v2).");
    if (fd == -1)
    {
	throw SF_Exception (this, "Unable to open audio output file.");
    }

    clipping_warning_printed = false;
}

void
SF_File_Output::execute ()
{
    SF_Frame *x = in_x->get_source_frame (0);
    SF_Sample *s = x->get_signal ();
    SF_Length l = x->get_num_rows ();
    SF_Length i;

    // convert floating-point input to 16-bit integer samples
    for (i = 0; i < l; i++)
    {
	SF_Sample samp;
	samp = s[i];
	if (samp >= 1)
	{
	    samp = 32767;
	    if (!clipping_warning_printed)
	    {
		clipping_warning_printed = true;
		SF_Warning (this, "Output signal clipped");
	    }
	}
	else if (samp <= -1)
	{
	    samp = -32768;
	    if (!clipping_warning_printed)
	    {
		clipping_warning_printed = true;
		SF_Warning (this, "Output signal clipped");
	    }
	}
	else
	{
	    samp *= 32768;
	}
	buffer[i] = (int)samp;
    }
    write_sound (fd, 0, (int)l-1, 1, &buffer);
    samples_written += (int)l;
}

void
SF_File_Output::finish ()
{
    close_sound_output (fd, samples_written * c_snd_datum_size (snd_16_linear));
    fd = -1;
    delete[] buffer;
    buffer = 0;
    buflen = 0;
}

const char*
SF_File_Output::get_file_name ()
{
    return file_name;
}

void
SF_File_Output::set_file_name (const char* file_name)
{
    this->file_name = file_name;
}

/* EOF */
