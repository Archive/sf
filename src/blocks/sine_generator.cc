/* SF_Sine_Generator
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/sine_generator.hh>

SF_Sine_Generator::SF_Sine_Generator (const char* name)
    : SF_Block (true,
		false,
		"Sine generator",
		name),
      s_y (0),
      s_freq (0),
      s_amp (0),
      fs (0),
      phi (0)
{
    in_freq = new SF_Input_Terminal ("freq", this, false);
    in_amp = new SF_Input_Terminal ("amp", this, 1, false);  //Default input = 1
    out_y = new SF_Output_Terminal ("y", this);
    add_input_terminal (in_freq);
    add_input_terminal (in_amp);
    add_output_terminal (out_y);
}

SF_Sine_Generator::~SF_Sine_Generator ()
{
    remove_input_terminal (in_freq);
    remove_input_terminal (in_amp);
    remove_output_terminal (out_y);
    delete in_freq;
    delete in_amp;
    delete out_y;
    in_freq = 0;
    in_amp = 0;
    out_y = 0;
}

void
SF_Sine_Generator::initialize ()
{
    s_y = out_y->get_frame ()->get_signal ();
    s_freq = in_freq->get_source_frame (0)->get_signal ();
    s_amp = in_amp->get_source_frame (0)->get_signal ();
    fs = in_freq->get_sample_rate ();
    phi = 0;	//phi[0] = 0
}

void
SF_Sine_Generator::execute ()
{
    SF_Length l = out_y->get_frame ()->get_num_rows ();

    for (int i = 0; i < l; i++)
    {
	if (s_freq[i] < 0)
	{
	    throw SF_Exception (this, "Frequency must be non-negative");
	}	   
	if (s_freq[i] > (fs / 2))
	{
	    throw SF_Exception (this, "Frequency must be less than or equal to half of the sample frequency");
	}	   
	if (s_amp[i] < 0)
	{
	    throw SF_Exception(this, "Amplitude must be non-negative");
	}
	phi = phi + 2 * M_PI * s_freq[i] / fs;		//M_PI defined in math.h
	s_y[i] = s_amp[i] * sin(phi);
    }
}

void
SF_Sine_Generator::finish()
{
}

/* EOF */
