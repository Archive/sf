/* SF_Ampdb
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/ampdb.hh>

SF_Ampdb::SF_Ampdb (const char* name)
    : SF_Block (false,
		false,
		"ampdB",
		name),
      s_db (0),
      s_amp (0),
      in_db (0),
      out_amp (0)
{
    in_db = new SF_Input_Terminal ("dB", this, false);
    out_amp = new SF_Output_Terminal ("amp", this);
    add_input_terminal (in_db);
    add_output_terminal (out_amp);
}

SF_Ampdb::~SF_Ampdb ()
{
    remove_input_terminal (in_db);
    remove_output_terminal (out_amp);
    delete in_db;
    delete out_amp;
    in_db = 0;
    out_amp = 0;
}

void
SF_Ampdb::initialize ()
{
    // Initialize pointers to input and output (stream) signals
    s_db = in_db->get_source_frame (0)->get_signal ();
    s_amp = out_amp->get_frame ()->get_signal ();
}

void
SF_Ampdb::execute ()
{
    SF_Length i, l;
    l = in_db->get_source_frame (0)->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	s_amp[i] = pow (10, s_db[i] / 20);
    }
}

void
SF_Ampdb::finish ()
{
}

/* EOF */
