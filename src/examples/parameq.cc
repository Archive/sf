/* 3-band parametric equalizer
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/high_shelf_filter.hh>
#include <sf/blocks/low_shelf_filter.hh>
#include <sf/blocks/peaking_filter.hh>
#include <sf/blocks/variable.hh>

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 11)
    {
	cerr << "Usage:" << endl
	     << "parameq <dur> <infile> <outfile> <lowfreq> <lowgain> <midfreq> <midbw> <midgain> <highfreq> <highgain>" << endl
	     << "<dur>:       simulation duration                   [ms]" << endl
	     << "<infile>:    audio file to read from" << endl
	     << "<outfile>:   audio file to write to (WAV)" << endl
	     << "<lowfreq>:   corner frequency for low band filter  [Hz]" << endl
	     << "<lowgain>:   gain for frequencies under <lowfreq>  [dB]" << endl
	     << "<midfreq>:   center frequency for middle filter    [Hz]" << endl
	     << "<midbw>:     bandwidth for middle filter           [Hz]" << endl
	     << "<midgain>:   gain for frequencies around <midfreq> [dB]" << endl
	     << "<highfreq>:  corner frequency for high band filter [Hz]" << endl
	     << "<highgain>:  gain for frequencies above <highfreq> [dB]" << endl << endl
	     << " === Example use ===" << endl
	     << "./parameq 5 helmi.wav peqexample.wav 300 -20 800 300 10 5000 -20" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Parametric equalizer");
	SF_File_Input in ("Input");
	SF_File_Output out ("Output");
	SF_Low_Shelf_Filter fil_low ("Low band filter");
	SF_Peaking_Filter fil_mid ("Middle band filter");
	SF_High_Shelf_Filter fil_high ("High band filter");
	SF_Variable lowfreq ("lowfreq");
	SF_Variable midf1 ("midf1");
	SF_Variable midf2 ("midf2");
	SF_Variable highfreq ("highfreq");
	SF_Variable lowgain ("lowgain");
	SF_Variable midgain ("midgain");
	SF_Variable highgain ("highgain");

	nw.add_block (in);
	nw.add_block (out);
	nw.add_block (fil_low);
	nw.add_block (fil_mid);
	nw.add_block (fil_high);
	nw.add_block (lowfreq);
	nw.add_block (midf1);
	nw.add_block (midf2);
	nw.add_block (highfreq);
	nw.add_block (lowgain);
	nw.add_block (midgain);
	nw.add_block (highgain);

	in >> fil_low.get_input ("x")
	   >> fil_mid.get_input ("x")
	   >> fil_high.get_input ("x")
	   >> out;

	lowfreq >> fil_low.get_input ("freq");
	midf1 >> fil_mid.get_input ("f1");
	midf2 >> fil_mid.get_input ("f2");
	highfreq >> fil_high.get_input ("freq");

	lowgain >> fil_low.get_input ("gain");
	midgain >> fil_mid.get_input ("gain");
	highgain >> fil_high.get_input ("gain");

	in.set_file_name (argv[2]);
	out.set_file_name (argv[3]);

	lowfreq.set_value (atof (argv[4]));
	midf1.set_value (atof (argv[6]) - atof (argv[7]) / 2);
	midf2.set_value (atof (argv[6]) + atof (argv[7]) / 2);
	highfreq.set_value (atof (argv[9]));
	lowgain.set_value (atof (argv[5]));
	midgain.set_value (atof (argv[8]));
	highgain.set_value (atof (argv[10]));

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
