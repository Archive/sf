/* Binary_Tree
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __AFM_TREE_HH__
#define __AFM_TREE_HH__

#include <sf/network.hh>
#include <sf/block.hh>
#include <sf/input_terminal.hh>

#include "binary_tree.hh"

const char SINE_SEPARATOR = ',';
const char ADDER_SEPARATOR = '+';
//const char BEGIN_SEPARATOR = '(';	//Defined in binary_tree.hh
//const char END_SEPARATOR = ')';	//Defined in binary_tree.hh

class AFM_Tree : public Binary_Tree
{
public:
    AFM_Tree ();
    virtual ~AFM_Tree ();
    virtual bool parse (char* expr);
    void generate_afm_synthesizer (SF_Network& net, SF_Input_Terminal& dest);	//throws SF_Exception
private:
    bool is_sine(Node* n);
    bool is_adder(Node* n);
    void generate_block (Node* n, SF_Input_Terminal* dest, SF_Network& net);
    SF_Block** blocklist;
};

#endif

/* EOF */




