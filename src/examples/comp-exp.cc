/* Compressor/expander program
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/ampdb.hh>
#include <sf/blocks/attack_release.hh>
#include <sf/blocks/delay_line.hh>
#include <sf/blocks/divider.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/max_min.hh>
#include <sf/blocks/multiplexer.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/negation.hh>
#include <sf/blocks/pow.hh>
#include <sf/blocks/reciprocal.hh>
#include <sf/blocks/rms_estimator.hh>
#include <sf/blocks/variable.hh>

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 8)
    {
	cerr << "Usage:" << endl
	     << "comp-exp <dur> <infile> <outfile> <ratio> <threshold> <attack> <release>" << endl
	     << "<dur>:       simulation duration           [ms]" << endl
	     << "<infile>:    audio file to read from" << endl
	     << "<outfile>:   audio file to write to (WAV)" << endl
	     << "<ratio>:     compression/expansion ratio;" << endl
	     << "             K/1 for compression, 1/K for expansion" << endl
	     << "<threshold>: compressor/expander threshold [dB]" << endl
	     << "<attack>:    attack time                   [ms]" << endl
	     << "<release>:   release time                  [ms]" << endl << endl
	     << " === Example uses ===" << endl
	     << "    Compression:" << endl
	     << "./comp-exp 5 helmi.wav cexexample.wav 5    -20 30  100" << endl
	     << "    Expansion:" << endl
	     << "./comp-exp 5 helmi.wav cexexample.wav 0.3  -30 30  100" << endl
	     << "    Limiting:" << endl
	     << "./comp-exp 5 helmi.wav cexexample.wav 20   -20 0.1 20" << endl
	     << "    Noise gating:" << endl
	     << "./comp-exp 5 helmi.wav cexexample.wav 0.03 -50 500 20" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Compressor/expander");
	SF_File_Input in ("Input");
	SF_File_Output out ("Output");
	SF_Variable ratio ("ratio");
	SF_Variable threshold ("threshold");
	SF_Variable attack ("attack");
	SF_Variable release ("release");
	SF_Rms_Estimator rms ("rms");
	SF_Ampdb ampdb ("ampdB");
	SF_Max_Min max_min ("max/min");

	SF_Network sel ("sel");
	SF_Adder sel_add ("sel_add");
	SF_Variable sel_minushalf ("sel_minushalf");
	SF_Variable sel_zero ("sel_zero");
	SF_Variable sel_one ("sel_one");
	SF_Max_Min sel_maxmin0 ("sel_maxmin0");
	SF_Max_Min sel_maxmin1 ("sel_maxmin1");
	SF_Multiplexer sel_mux ("sel_mux");

	SF_Attack_Release ar ("ar");

	SF_Network ctrl ("ctrl");
	SF_Divider ctrl_div ("ctrl_div");
	SF_Reciprocal ctrl_rec ("ctrl_rec");
	SF_Negation ctrl_neg ("ctrl_neg");
	SF_Variable ctrl_one ("ctrl_one");
	SF_Adder ctrl_add ("ctrl_add");
	SF_Pow ctrl_pow ("ctrl_pow");

	SF_Multiplier amp ("amp");

	nw.add_block (in);
	nw.add_block (out);
	nw.add_block (ratio);
	nw.add_block (threshold);
	nw.add_block (attack);
	nw.add_block (release);
	nw.add_block (rms);
	nw.add_block (ampdb);
	nw.add_block (max_min);
	nw.add_block (sel);
	nw.add_block (ar);
	nw.add_block (ctrl);
	nw.add_block (amp);
	sel.add_block (sel_add);
	sel.add_block (sel_minushalf);
	sel.add_block (sel_zero);
	sel.add_block (sel_one);
	sel.add_block (sel_maxmin0);
	sel.add_block (sel_maxmin1);
	sel.add_block (sel_mux);
	ctrl.add_block (ctrl_div);
	ctrl.add_block (ctrl_rec);
	ctrl.add_block (ctrl_neg);
	ctrl.add_block (ctrl_one);
	ctrl.add_block (ctrl_add);
	ctrl.add_block (ctrl_pow);

	// Construct the lower-level network "sel"
	sel.create_input ("max");
	sel.create_input ("min");
	sel.create_input ("ratio");
	sel.create_output ("max or min");

	sel_minushalf >> sel_add;
	sel.get_input_bubble ("ratio") >> sel_add >> sel_maxmin0;
	sel_zero >> sel_maxmin0;
	sel_maxmin0.get_output ("max") >> sel_maxmin1;
	sel_one >> sel_maxmin1;
	sel_maxmin1.get_output ("min") >> sel_mux.get_input ("n");

	sel_mux.set_num_channels (2);
	sel.get_input_bubble ("min") >> sel_mux.get_input ("x0");
	sel.get_input_bubble ("max") >> sel_mux.get_input ("x1");
	sel_mux >> sel.get_output_bubble ("max or min");

	// Construct the lower-level network "ctrl"
	ctrl.create_input ("ratio");
	ctrl.create_input ("thres_power");
	ctrl.create_input ("eff_power");
	ctrl.create_output ("gain");

	ctrl.get_input_bubble ("thres_power") >> ctrl_div.get_input ("p");
	ctrl.get_input_bubble ("eff_power") >> ctrl_div.get_input ("q");
	ctrl_div >> ctrl_pow.get_input ("m");
	ctrl.get_input_bubble ("ratio") >> ctrl_rec >> ctrl_neg >> ctrl_add;
	ctrl_one >> ctrl_add >> ctrl_pow.get_input ("e");
	ctrl_pow >> ctrl.get_output_bubble ("gain");

	// Construct the higher-lever network nw
	in >> rms >> max_min;
	threshold >> ampdb >> max_min;

	max_min.get_output ("max") >> sel.get_input ("max");
	max_min.get_output ("min") >> sel.get_input ("min");
	ratio >> sel.get_input ("ratio");

	sel >> ar.get_input ("x");
	attack >> ar.get_input ("attack");
	release >> ar.get_input ("release");

	ratio >> ctrl.get_input ("ratio");
	ampdb >> ctrl.get_input ("thres_power");
	ar >> ctrl.get_input ("eff_power");

	ctrl >> amp;
	in >> amp >> out;

	//DEBUG
//	SF_File_Output rmsout ("DEBUG:RMS out"),
//	    gainout ("DEBUG:gain out"),
//	    arout ("DEBUG:A/R out"),
//	    maxout ("DEBUG:max out"),
//	    adbout ("DEBUG:adb out");
//	nw.add_block (rmsout);
//	nw.add_block (gainout);
//	nw.add_block (arout);
//	nw.add_block (maxout);
//	nw.add_block (adbout);
//	rms >> rmsout;
//	ctrl >> gainout;
//	ar >> arout;
//	ampdb >> adbout;
//	max_min.get_output ("max") >> maxout;
//	rmsout.set_file_name ("debug-cex-rms.wav");
//	gainout.set_file_name ("debug-cex-gain.wav");
//	arout.set_file_name ("debug-cex-ar.wav");
//	maxout.set_file_name ("debug-cex-max.wav");
//	adbout.set_file_name ("debug-cex-adb.wav");
	//DEBUG

	in.set_file_name (argv[2]);
	out.set_file_name (argv[3]);

	ratio.set_value (atof (argv[4]));
	threshold.set_value (atof (argv[5]));
	attack.set_value (atof (argv[6]));
	release.set_value (atof (argv[7]));
	sel_minushalf.set_value (-0.5);
	sel_zero.set_value (0);
	sel_one.set_value (1);
	ctrl_one.set_value (1);

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
