/* Amplitude and frequency modulation synthesizer
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/quantizer.hh>
#include <sf/blocks/variable.hh>

#include "afm_tree.hh"		//Inherited from Binary_Tree (binary_tree.cc)

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 4)
    {
	cerr << "Usage:" << endl
	     << "afmsynth <dur> <outfile> <expression>" << endl
	     << "<dur>:      simulation duration            [ms]" << endl
	     << "<outfile>:  audio file to write to (WAV)" << endl
	     << "<expression>: '(A, B)' or '(A+ B)', where 'A� and �B�" << endl
	     << "              are either floats or <expression>:s recursively" << endl << endl
	     << " === Example use ===" << endl
	     << "./afmsynth 10 afmexample.wav '((1000 + ((50 + (0.15, 50)), 1000)), 1)'" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("AFM synthesizer");
	SF_File_Output out ("Output");
	SF_Quantizer quant ("Output quantizer");
	SF_Variable bits ("Quantization bits");
	AFM_Tree sine_tree;

	nw.add_block (out);
	nw.add_block (quant);
	nw.add_block (bits);

	out.set_file_name (argv[2]);
	bits.set_value (16);
	bits >> quant.get_input ("bits");
	quant >> out;

	sine_tree.parse (argv[3]);
	//DEBUG: print the parsed tree expression
	//sine_tree.print ();

	try 
	{
	    sine_tree.generate_afm_synthesizer (nw, quant.get_input ("x"));
	}
	catch (...)
	{
	    throw SF_Exception (0, "Synthesizer network generation failed");
	}

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
