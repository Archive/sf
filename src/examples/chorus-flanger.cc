/* Chorus/flanger
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/delay_line.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/negation.hh>
#include <sf/blocks/sine_generator.hh>
#include <sf/blocks/variable.hh>

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 10)
    {
	cerr << "Usage:" << endl
	     << "chorus-flanger <dur> <infile> <outfile> <delay> <modfreq> <moddepth> <feedback> <feedfwd> <blend>" << endl
	     << "<dur>:      simulation duration            [ms]" << endl
	     << "<infile>:   audio file to read from" << endl
	     << "<outfile>:  audio file to write to (WAV)" << endl
	     << "<delay>:    delay                          [ms]" << endl
	     << "<modfreq>:  delay modulation frequency     [Hz]" << endl
	     << "<moddepth>: delay modulation depth         [ms]" << endl
	     << "<feedback>: feedback amount" << endl
	     << "<feedfwd>:  feedforward amount" << endl
	     << "<blend>:    dry signal blend amount" << endl << endl
	     << " === Example uses ===" << endl
	     << "    Vibrato:" << endl
	     << "./chorus-flanger 5 helmi.wav cflexample.wav 60 0.3 60 0       1       0" << endl
	     << "    Flanger:" << endl
	     << "./chorus-flanger 5 helmi.wav cflexample.wav 2  0.3 2  -0.7071 0.7071  0.7071" << endl
	     << "    Industry standard chorus:" << endl
	     << "./chorus-flanger 5 helmi.wav cflexample.wav 40 0.3 20 0       0.7071  1" << endl
	     << "    White chorus:" << endl
	     << "./chorus-flanger 5 helmi.wav cflexample.wav 20 0.3 10 0.7071  1       0.7071" << endl
	     << "    Doubling:" << endl
	     << "./chorus-flanger 5 helmi.wav cflexample.wav 50 0.3 30 0       0.7071  0.7071" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Chorus/flanger");
	SF_File_Input in ("Input");
	SF_File_Output out ("Output");
	SF_Delay_Line dfb ("dfb");
	SF_Delay_Line dff ("dff");
	SF_Multiplier mfb ("fb mult");
	SF_Multiplier mff ("ff mult");
	SF_Multiplier mbl ("bl mult");
	SF_Sine_Generator mod_sine ("modulator sine");
	SF_Adder apre ("apre");
	SF_Adder apost ("apost");
	SF_Adder avib ("avib");
	SF_Negation neg ("neg");
	SF_Variable delayff ("delayff");
	SF_Variable delayfb ("delayfb");
	SF_Variable modfreq ("modfreq");
	SF_Variable moddepth ("moddepth");
	SF_Variable feedback ("feedback");
	SF_Variable feedfwd ("feedfwd");
	SF_Variable blend ("blend");

	nw.add_block (in);
	nw.add_block (out);
	nw.add_block (dfb);
	nw.add_block (dff);
	nw.add_block (mfb);
	nw.add_block (mff);
	nw.add_block (mbl);
	nw.add_block (mod_sine);
	nw.add_block (apre);
	nw.add_block (apost);
	nw.add_block (avib);
	nw.add_block (neg);
	nw.add_block (delayff);
	nw.add_block (delayfb);
	nw.add_block (modfreq);
	nw.add_block (moddepth);
	nw.add_block (feedback);
	nw.add_block (feedfwd);
	nw.add_block (blend);

	in >> apre;
	apre >> dff.get_input ("x");
	apre >> dfb.get_input ("x");

	modfreq >> mod_sine.get_input ("freq");
	moddepth >> mod_sine.get_input ("amp");
	mod_sine >> avib;
	delayff >> avib;
	avib >> dff.get_input ("d");
	delayfb >> dfb.get_input ("d");

	dff >> mff;
	feedfwd >> mff;
	mff >> apost;

	apre >> mbl;
	blend >> mbl;
	mbl >> apost;

	dfb >> mfb;
	feedback >> mfb;
	mfb >> neg;
	neg >> apre;

	apost >> out;

	in.set_file_name (argv[2]);
	out.set_file_name (argv[3]);

	delayfb.set_value (atof (argv[4]) - SF_Frame::get_duration ());
	delayff.set_value (atof (argv[4]));
	modfreq.set_value (atof (argv[5]));
	moddepth.set_value (atof (argv[6]));
	blend.set_value (atof (argv[9])); // amp
	feedfwd.set_value (atof (argv[8])); // amp
	feedback.set_value (atof (argv[7])); // amp
	dfb.set_length (delayfb.get_value ());
	dff.set_length (delayff.get_value () + moddepth.get_value ());

	if (delayfb.get_value () < 0)
	{
	    throw SF_Exception (0, "Delay must be greater than or equal to frame duration (1 ms by default).");
	}
	if (moddepth.get_value () > delayff.get_value ())
	{
	    throw SF_Exception (0, "Delay must be greater than or equal to modulation depth.");
	}

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
