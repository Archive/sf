/* Multitap delay program
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/delay_line.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/variable.hh>

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 12)
    {
	cerr << "Usage:" << endl
	     << "multitap <dur> <infile> <outfile> <delay 1> <gain 1> <delay 2> <gain 2> <delay 3> <gain 3> <delay 4> <gain 4>"
	     << "<dur>:       simulation duration           [ms]" << endl
	     << "<infile>:    audio file to read from" << endl
	     << "<outfile>:   audio file to write to (WAV)" << endl
	     << "<delay 1>:   First delay tap length        [ms]" << endl
	     << "<gain 1>:    Gain of first delay tap" << endl
	     << "<delay 2>:   Second delay tap length       [ms]" << endl
	     << "<gain 2>:    Gain of second delay tap" << endl
	     << "<delay 3>:   Third delay tap length        [ms]" << endl
	     << "<gain 3>:    Gain of third delay tap" << endl
	     << "<delay 4>:   Fourth delay tap length       [ms]" << endl
	     << "<gain 4>:    Gain of fourth delay tap" << endl << endl
	     << " === Example uses ===" << endl
	     << "./multitap 5 helmi.wav mulexample.wav 100 0.5 300 0.3 400 0.2 800 0.1" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Multitap delay line");
	SF_File_Input in ("Input");
	SF_File_Output out ("Output");
	SF_Adder add ("adder");
	SF_Delay_Line d1 ("delay line 1");
	SF_Delay_Line d2 ("delay line 2");
	SF_Delay_Line d3 ("delay line 3");
	SF_Delay_Line d4 ("delay line 4");
	SF_Variable g1 ("gain 1");
	SF_Variable g2 ("gain 2");
	SF_Variable g3 ("gain 3");
	SF_Variable g4 ("gain 4");
	SF_Variable l1 ("delay length 1");
	SF_Variable l2 ("delay length 2");
	SF_Variable l3 ("delay length 3");
	SF_Variable l4 ("delay length 4");
	SF_Multiplier m1 ("multiplier 1");
	SF_Multiplier m2 ("multiplier 2");
	SF_Multiplier m3 ("multiplier 3");
	SF_Multiplier m4 ("multiplier 4");

	nw.add_block (in);
	nw.add_block (out);
	nw.add_block (add);
	nw.add_block (d1);
	nw.add_block (d2);
	nw.add_block (d3);
	nw.add_block (d4);
	nw.add_block (g1);
	nw.add_block (g2);
	nw.add_block (g3);
	nw.add_block (g4);
	nw.add_block (l1);
	nw.add_block (l2);
	nw.add_block (l3);
	nw.add_block (l4);
	nw.add_block (m1);
	nw.add_block (m2);
	nw.add_block (m3);
	nw.add_block (m4);

	in >> d1.get_input ("x");
	in >> d2.get_input ("x");
	in >> d3.get_input ("x");
	in >> d4.get_input ("x");

	l1 >> d1.get_input ("d");
	l2 >> d2.get_input ("d");
	l3 >> d3.get_input ("d");
	l4 >> d4.get_input ("d");

	d1 >> m1;
	d2 >> m2;
	d3 >> m3;
	d4 >> m4;

	g1 >> m1 >> add;
	g2 >> m2 >> add;
	g3 >> m3 >> add;
	g4 >> m4 >> add;

	add >> out;

	in.set_file_name (argv[2]);
	out.set_file_name (argv[3]);

	l1.set_value (atof (argv[4]));
	g1.set_value (atof (argv[5]));
	l2.set_value (atof (argv[6]));
	g2.set_value (atof (argv[7]));
	l3.set_value (atof (argv[8]));
	g3.set_value (atof (argv[9]));
	l4.set_value (atof (argv[10]));
	g4.set_value (atof (argv[11]));
	d1.set_length (l1.get_value ());
	d2.set_length (l2.get_value ());
	d3.set_length (l3.get_value ());
	d4.set_length (l4.get_value ());

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
