/* Wah-wah program
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/band_pass_filter.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/sine_generator.hh>
#include <sf/blocks/variable.hh>

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 9)
    {
	cerr << "Usage:" << endl
	     << "wah-wah <dur> <infile> <outfile> <lower fc> <bandwidth> <band sepa> <modfreq> <moddepth>"
	     << "<dur>:       simulation duration           [ms]" << endl
	     << "<infile>:    audio file to read from" << endl
	     << "<outfile>:   audio file to write to (WAV)" << endl
	     << "<lower fc>:  lower band center frequency   [Hz]" << endl
	     << "<bandwidth>: width of lower and upper bands in octaves" << endl
	     << "<band sepa>: separation between lower and upper bands in octaves" << endl
	     << "<modfreq>:   modulation frequency          [Hz]" << endl
	     << "<moddepth>:  modulation depth              [Hz]" << endl << endl
	     << " === Example uses ===" << endl
	     << "./wah-wah 5 helmi.wav wahexample.wav 200 1 3 2 100" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Wah-wah");
	SF_File_Input in ("Input");
	SF_File_Output out ("Output");
	SF_Band_Pass_Filter fil_l1 ("Lower band filter 1");
	SF_Band_Pass_Filter fil_l2 ("Lower band filter 2");
	SF_Band_Pass_Filter fil_u1 ("Upper band filter 1");
	SF_Band_Pass_Filter fil_u2 ("Upper band filter 2");
	SF_Variable fcenter ("fcenter");
	SF_Variable bsepacoef ("bsepacoef");
	SF_Variable bwidthcoef ("bwidthcoef");
	SF_Variable modfreq ("modfreq");
	SF_Variable moddepth ("moddepth");
	SF_Multiplier mbsepa ("mbsepa");
	SF_Multiplier mbwidth_lower ("mbwidth_lower");
	SF_Multiplier mbwidth_upper ("mbwidth_upper");
	SF_Adder amod ("amod");
	SF_Adder aout ("aout");
	SF_Sine_Generator mod ("mod");

	nw.add_block (in);
	nw.add_block (out);
	nw.add_block (fil_l1);
	nw.add_block (fil_l2);
	nw.add_block (fil_u1);
	nw.add_block (fil_u2);
	nw.add_block (fcenter);
	nw.add_block (bsepacoef);
	nw.add_block (bwidthcoef);
	nw.add_block (modfreq);
	nw.add_block (moddepth);
	nw.add_block (mbsepa);
	nw.add_block (mbwidth_lower);
	nw.add_block (mbwidth_upper);
	nw.add_block (amod);
	nw.add_block (aout);
	nw.add_block (mod);

	in >> fil_l1.get_input ("x")
	   >> fil_l2.get_input ("x")
	   >> aout;
	in >> fil_u1.get_input ("x")
	   >> fil_u2.get_input ("x")
	   >> aout
	   >> out;

	modfreq >> mod.get_input ("freq");
	moddepth >> mod.get_input ("amp");
	mod >> amod;
	fcenter >> amod;

	amod >> fil_l1.get_input ("f1");
	amod >> fil_l2.get_input ("f1");
	amod >> mbsepa;
	bsepacoef >> mbsepa;
	mbsepa >> fil_u1.get_input ("f1");
	mbsepa >> fil_u2.get_input ("f1");

	amod >> mbwidth_lower;
	bwidthcoef >> mbwidth_lower;
	mbwidth_lower >> fil_l1.get_input ("f2");
	mbwidth_lower >> fil_l2.get_input ("f2");

	mbsepa >> mbwidth_upper;
	bwidthcoef >> mbwidth_upper;
	mbwidth_upper >> fil_u1.get_input ("f2");
	mbwidth_upper >> fil_u2.get_input ("f2");

	in.set_file_name (argv[2]);
	out.set_file_name (argv[3]);

	fcenter.set_value (atof (argv[4]));
	bsepacoef.set_value (pow (2, atof (argv[6])));
	bwidthcoef.set_value (pow (2, atof (argv[5])));
	modfreq.set_value (atof (argv[7]));
	moddepth.set_value (atof (argv[8]));

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
