/* Dattorro reverb algorithm
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */


// This algorithm is based on the following article:
// Dattorro, Jon,
// ``Effect Design --- Part 1: Reverberator and Other Filters.''
// Journal of the Audio Engineering Society,
// Vol. 45, No. 9, pp. 660--684, September 1997


#include <iostream.h>

#include <sf/sf.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/delay_line.hh>

#include "dattorro_reverb.hh"

Dattorro_Reverb::Dattorro_Reverb (const char* name)
    : SF_Block (1,
		2,
		false,
		false,
		"Dattorro reverb",
		name),
      s_x (0), s_yl (0), s_yr (0),
      nw ("Dattorro reverb"),
      var_excursion ("var_excursion"),
      var_decay ("var_decay"),
      var_decay_diffusion_1 ("var_decay_diffusion_1"),
      var_decay_diffusion_2 ("var_decay_diffusion_2"),
      var_input_diffusion_1 ("var_input_diffusion_1"),
      var_input_diffusion_2 ("var_input_diffusion_2"),
      var_bandwidth ("var_bandwidth"),
      var_damping ("var_damping"),
      var_predelay ("var_predelay"),
      var_mod_freq ("var_mod_freq"),
      var_attenuation ("var_attenuation"),
      pre_attenuator ("pre_attenuator"),
      pre_var_attenuation ("pre_var_attenuation"),
      pre_predelay ("pre_predelay"),
      pre_bandwidth_filter ("pre_bandwidth_filter"),
      lt_modulator ("lt_modulator"),
      lt_mod_adder ("lt_mod_adder"),
      lt_damping_filter ("lt_damping_filter"),
      lt_feedback ("lt_feedback"),
      rt_modulator ("rt_modulator"),
      rt_mod_adder ("rt_mod_adder"),
      rt_damping_filter ("rt_damping_filter"),
      rt_feedback ("rt_feedback"),
      out_left_sum ("out_left_sum"),
      out_left_attenuator ("out_left_attenuator"),
      out_right_sum ("out_right_sum"),
      out_right_attenuator ("out_right_attenuator")
{
    SF_Block::inputs = new SF_Input_Terminal[1];
    SF_Block::outputs = new SF_Output_Terminal[2];
    inputs[X].init_terminal ("x", this, false);
    outputs[YL].init_terminal ("yl", this);
    outputs[YR].init_terminal ("yr", this);

    pre_input_diffusor[0].set_instance_name ("pre_input_diffusor[0]");
    pre_input_diffusor[1].set_instance_name ("pre_input_diffusor[1]");
    pre_input_diffusor[2].set_instance_name ("pre_input_diffusor[2]");
    pre_input_diffusor[3].set_instance_name ("pre_input_diffusor[3]");
    pre_var_input_diffusor_delay[0].set_instance_name ("pre_var_input_diffusor_delay[0]");
    pre_var_input_diffusor_delay[1].set_instance_name ("pre_var_input_diffusor_delay[1]");
    pre_var_input_diffusor_delay[2].set_instance_name ("pre_var_input_diffusor_delay[2]");
    pre_var_input_diffusor_delay[3].set_instance_name ("pre_var_input_diffusor_delay[3]");
    lt_decay_diffusor[0].set_instance_name ("lt_decay_diffusor[0]");
    lt_decay_diffusor[1].set_instance_name ("lt_decay_diffusor[1]");
    lt_delay[0].set_instance_name ("lt_delay[0]");
    lt_delay[1].set_instance_name ("lt_delay[1]");
    lt_decay[0].set_instance_name ("lt_decay[0]");
    lt_decay[1].set_instance_name ("lt_decay[1]");
    lt_var_decay_diffusor_delay[0].set_instance_name ("lt_var_decay_diffusor_delay[0]");
    lt_var_decay_diffusor_delay[1].set_instance_name ("lt_var_decay_diffusor_delay[1]");
    lt_var_delay[0].set_instance_name ("lt_var_delay[0]");
    lt_var_delay[1].set_instance_name ("lt_var_delay[1]");
    rt_decay_diffusor[0].set_instance_name ("rt_decay_diffusor[0]");
    rt_decay_diffusor[1].set_instance_name ("rt_decay_diffusor[1]");
    rt_delay[0].set_instance_name ("rt_delay[0]");
    rt_delay[1].set_instance_name ("rt_delay[1]");
    rt_decay[0].set_instance_name ("rt_decay[0]");
    rt_decay[1].set_instance_name ("rt_decay[1]");
    rt_var_decay_diffusor_delay[0].set_instance_name ("rt_var_decay_diffusor_delay[0]");
    rt_var_decay_diffusor_delay[1].set_instance_name ("rt_var_decay_diffusor_delay[1]");
    rt_var_delay[0].set_instance_name ("rt_var_delay[0]");
    rt_var_delay[1].set_instance_name ("rt_var_delay[1]");
    out_left_negation[0].set_instance_name ("out_left_negation[0]");
    out_left_negation[1].set_instance_name ("out_left_negation[1]");
    out_left_negation[2].set_instance_name ("out_left_negation[2]");
    out_left_negation[3].set_instance_name ("out_left_negation[3]");
    out_right_negation[0].set_instance_name ("out_right_negation[0]");
    out_right_negation[1].set_instance_name ("out_right_negation[1]");
    out_right_negation[2].set_instance_name ("out_right_negation[2]");
    out_right_negation[3].set_instance_name ("out_right_negation[3]");

    // Set default parameter values
    var_excursion.set_value (DEFAULT_EXCURSION);
    var_decay.set_value (DEFAULT_DECAY);
    var_decay_diffusion_1.set_value (DEFAULT_DECAY_DIFFUSION_1);
    var_decay_diffusion_2.set_value (DEFAULT_DECAY_DIFFUSION_2);
    var_input_diffusion_1.set_value (DEFAULT_INPUT_DIFFUSION_1);
    var_input_diffusion_2.set_value (DEFAULT_INPUT_DIFFUSION_2);
    var_bandwidth.set_value (DEFAULT_BANDWIDTH);
    var_damping.set_value (DEFAULT_DAMPING);
    var_predelay.set_value (DEFAULT_PREDELAY);
    var_mod_freq.set_value (DEFAULT_MOD_FREQ);
    var_attenuation.set_value (DEFAULT_ATTENUATION);
}

Dattorro_Reverb::~Dattorro_Reverb ()
{
    delete[] SF_Block::inputs;
    SF_Block::inputs = 0;
    delete[] SF_Block::outputs;
    SF_Block::outputs = 0;
}

void
Dattorro_Reverb::print (ostream& stream) const
{
    nw.print (stream);
}

void
Dattorro_Reverb::initialize ()
{
    // construct and initialize the internal network which does the actual
    // reverberation business
    add_blocks ();
    init_preprocessing ();
    init_left_tank ();
    init_right_tank ();
    init_output ();

    // "merge" the input terminal of the this Dattorro_Reverb block and the
    // input terminal of the internal network.  Note that this is some degree of
    // a kludge: DON'T TRY THIS AT HOME!
    pre_attenuator.inputs[SF_Multiplier::X].add_input
	(inputs[X].get_first_source_frame ());

    // DEBUG
//     nw.add_block (debug_pre);
//     nw.add_block (debug_lt1);
//     nw.add_block (debug_lt2);
//     nw.add_block (debug_rt1);
//     nw.add_block (debug_rt2);
//     pre_input_diffusor[3].outputs[Dattorro_Allpass::Y] >> debug_pre;
//     lt_delay[0].outputs[Dattorro_Delay_Line::Y] >> debug_lt1;
//     lt_delay[1].outputs[Dattorro_Delay_Line::Y] >> debug_lt2;
//     rt_delay[0].outputs[Dattorro_Delay_Line::Y] >> debug_rt1;
//     rt_delay[1].outputs[Dattorro_Delay_Line::Y] >> debug_rt2;
//     debug_pre.set_file_name ("rvdebug_pre.wav");
//     debug_lt1.set_file_name ("rvdebug_lt1.wav");
//     debug_lt2.set_file_name ("rvdebug_lt2.wav");
//     debug_rt1.set_file_name ("rvdebug_rt1.wav");
//     debug_rt2.set_file_name ("rvdebug_rt2.wav");
//     nw.print (cout);

    nw.initialize ();

    // get signal pointers ready for execution phase
    s_yl = outputs[YL].get_frame ()->get_signal ();
    s_yr = outputs[YR].get_frame ()->get_signal ();
    nw_s_yl = out_left_attenuator.outputs[Dattorro_Consumer_Multiplier::Y]
	.get_frame ()->get_signal ();
    nw_s_yr = out_right_attenuator.outputs[Dattorro_Consumer_Multiplier::Y]
	.get_frame ()->get_signal ();
}

void
Dattorro_Reverb::execute ()
{
    // run the reverb algorithm
    nw.execute ();

    // unfortunately we have to duplicate data here due to the lack of support
    // for hierarchical networks; copy the output of the internal network to the
    // output of this Dattorro_Reverb block.
    SF_Length i, l;
    l = outputs[YL].get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	s_yl[i] = nw_s_yl[i];
	s_yr[i] = nw_s_yr[i];
    }
}

void
Dattorro_Reverb::finish ()
{
    nw.finish ();
}

void
Dattorro_Reverb::add_blocks ()
{
    // parameters
    nw.add_block (var_excursion);
    nw.add_block (var_decay);
    nw.add_block (var_decay_diffusion_1);
    nw.add_block (var_decay_diffusion_2);
    nw.add_block (var_input_diffusion_1);
    nw.add_block (var_input_diffusion_2);
    nw.add_block (var_bandwidth);
    nw.add_block (var_damping);
    nw.add_block (var_predelay);
    nw.add_block (var_mod_freq);
    nw.add_block (var_attenuation);

    // preprocessing subnetwork
    nw.add_block (pre_attenuator);
    nw.add_block (pre_var_attenuation);
    nw.add_block (pre_predelay);
    nw.add_block (pre_bandwidth_filter);
    nw.add_block (pre_input_diffusor[0]);
    nw.add_block (pre_input_diffusor[1]);
    nw.add_block (pre_input_diffusor[2]);
    nw.add_block (pre_input_diffusor[3]);
    nw.add_block (pre_var_input_diffusor_delay[0]);
    nw.add_block (pre_var_input_diffusor_delay[1]);
    nw.add_block (pre_var_input_diffusor_delay[2]);
    nw.add_block (pre_var_input_diffusor_delay[3]);

    // left tank subnetwork
    nw.add_block (lt_decay_diffusor[0]);
    nw.add_block (lt_decay_diffusor[1]);
    nw.add_block (lt_modulator);
    nw.add_block (lt_mod_adder);
    nw.add_block (lt_delay[0]);
    nw.add_block (lt_delay[1]);
    nw.add_block (lt_damping_filter);
    nw.add_block (lt_decay[0]);
    nw.add_block (lt_decay[1]);
    nw.add_block (lt_var_decay_diffusor_delay[0]);
    nw.add_block (lt_var_decay_diffusor_delay[1]);
    nw.add_block (lt_var_delay[0]);
    nw.add_block (lt_var_delay[1]);
    nw.add_block (lt_feedback);

    // right tank subnetwork
    nw.add_block (rt_decay_diffusor[0]);
    nw.add_block (rt_decay_diffusor[1]);
    nw.add_block (rt_modulator);
    nw.add_block (rt_mod_adder);
    nw.add_block (rt_delay[0]);
    nw.add_block (rt_delay[1]);
    nw.add_block (rt_damping_filter);
    nw.add_block (rt_decay[0]);
    nw.add_block (rt_decay[1]);
    nw.add_block (rt_var_decay_diffusor_delay[0]);
    nw.add_block (rt_var_decay_diffusor_delay[1]);
    nw.add_block (rt_var_delay[0]);
    nw.add_block (rt_var_delay[1]);
    nw.add_block (rt_feedback);

    // output network
    nw.add_block (out_left_negation[0]);
    nw.add_block (out_left_negation[1]);
    nw.add_block (out_left_negation[2]);
    nw.add_block (out_left_negation[3]);
    nw.add_block (out_left_sum);
    nw.add_block (out_left_attenuator);
    nw.add_block (out_right_negation[0]);
    nw.add_block (out_right_negation[1]);
    nw.add_block (out_right_negation[2]);
    nw.add_block (out_right_negation[3]);
    nw.add_block (out_right_sum);
    nw.add_block (out_right_attenuator);
}

void
Dattorro_Reverb::init_preprocessing ()
{
    // build input and preprocessing subnetwork
    //HERE'S the input to the internal network
    pre_attenuator
	>> pre_predelay.inputs[SF_Delay_Line::X]
	>> pre_bandwidth_filter.inputs[Dattorro_1pole_Lowpass::X]
	>> pre_input_diffusor[0].inputs[Dattorro_Allpass::X];
    pre_input_diffusor[0].outputs[Dattorro_Allpass::Y] >> pre_input_diffusor[1].inputs[Dattorro_Allpass::X];
    pre_input_diffusor[1].outputs[Dattorro_Allpass::Y] >> pre_input_diffusor[2].inputs[Dattorro_Allpass::X];
    pre_input_diffusor[2].outputs[Dattorro_Allpass::Y] >> pre_input_diffusor[3].inputs[Dattorro_Allpass::X];
    pre_input_diffusor[3].outputs[Dattorro_Allpass::Y] >> lt_feedback;
    pre_input_diffusor[3].outputs[Dattorro_Allpass::Y] >> rt_feedback;

    // initialize preprocessing subnetwork
    pre_var_attenuation >> pre_attenuator;
    pre_var_attenuation.set_value (0.5);

    var_predelay >> pre_predelay.inputs[SF_Delay_Line::D];
    pre_predelay.set_length (var_predelay.get_value ());

    var_bandwidth >> pre_bandwidth_filter.inputs[Dattorro_1pole_Lowpass::C];

    pre_var_input_diffusor_delay[0] >> pre_input_diffusor[0].inputs[Dattorro_Allpass::D];
    pre_var_input_diffusor_delay[1] >> pre_input_diffusor[1].inputs[Dattorro_Allpass::D];
    pre_var_input_diffusor_delay[2] >> pre_input_diffusor[2].inputs[Dattorro_Allpass::D];
    pre_var_input_diffusor_delay[3] >> pre_input_diffusor[3].inputs[Dattorro_Allpass::D];
    pre_var_input_diffusor_delay[0].set_value(PRE_DELAYS[0]);
    pre_var_input_diffusor_delay[1].set_value(PRE_DELAYS[1]);
    pre_var_input_diffusor_delay[2].set_value(PRE_DELAYS[2]);
    pre_var_input_diffusor_delay[3].set_value(PRE_DELAYS[3]);
    pre_input_diffusor[0].set_length (PRE_DELAYS[0]);
    pre_input_diffusor[1].set_length (PRE_DELAYS[1]);
    pre_input_diffusor[2].set_length (PRE_DELAYS[2]);
    pre_input_diffusor[3].set_length (PRE_DELAYS[3]);

    var_input_diffusion_1 >> pre_input_diffusor[0].inputs[Dattorro_Allpass::C];
    var_input_diffusion_1 >> pre_input_diffusor[1].inputs[Dattorro_Allpass::C];
    var_input_diffusion_2 >> pre_input_diffusor[2].inputs[Dattorro_Allpass::C];
    var_input_diffusion_2 >> pre_input_diffusor[3].inputs[Dattorro_Allpass::C];
}

void
Dattorro_Reverb::init_left_tank ()
{
    // build left tank subnetwork
    rt_feedback >> lt_decay_diffusor[0].inputs[Dattorro_Allpass::X];
    lt_decay_diffusor[0].outputs[Dattorro_Allpass::Y]
	>> lt_delay[0].inputs[Dattorro_Delay_Line::X];
    lt_delay[0].outputs[Dattorro_Delay_Line::Y]
	>> lt_damping_filter.inputs[Dattorro_1pole_Lowpass::X]
	>> lt_decay[0]
	>> lt_decay_diffusor[1].inputs[Dattorro_Allpass::X];
    lt_decay_diffusor[1].outputs[Dattorro_Allpass::Y]
	>> lt_delay[1].inputs[Dattorro_Delay_Line::X];
    lt_delay[1].outputs[Dattorro_Delay_Line::Y]
	>> lt_decay[1]
	>> lt_feedback;

    // initialize left tank subnetwork
    var_decay_diffusion_1 >> lt_decay_diffusor[0].inputs[Dattorro_Allpass::C];
    var_excursion >> lt_modulator.inputs[SF_Sine_Generator::A];
    var_mod_freq >> lt_modulator.inputs[SF_Sine_Generator::F];
    lt_modulator >> lt_mod_adder;
    lt_var_decay_diffusor_delay[0] >> lt_mod_adder
				   >> lt_decay_diffusor[0].inputs[Dattorro_Allpass::D];
    lt_var_decay_diffusor_delay[0].set_value(LT_DELAYS[0]);
    // take the modulation into account
    lt_decay_diffusor[0].set_length (LT_DELAYS[0] + var_excursion.get_value ());

    lt_var_delay[0] >> lt_delay[0].inputs[Dattorro_Delay_Line::D];
    lt_var_delay[0].set_value (LT_DELAYS[1]);
    lt_delay[0].set_length (LT_DELAYS[1]);

    var_damping >> lt_damping_filter.inputs[Dattorro_1pole_Lowpass::C];

    var_decay >> lt_decay[0];

    var_decay_diffusion_2 >> lt_decay_diffusor[1].inputs[Dattorro_Allpass::C];
    lt_var_decay_diffusor_delay[1] >> lt_decay_diffusor[1].inputs[Dattorro_Allpass::D];
    lt_var_decay_diffusor_delay[1].set_value(LT_DELAYS[2]);
    lt_decay_diffusor[1].set_length (LT_DELAYS[2]);

    lt_var_delay[1] >> lt_delay[1].inputs[Dattorro_Delay_Line::D];
    // take implicit delays into account in the feedback loops
    lt_var_delay[1].set_value (LT_DELAYS[3] - SF_Frame::get_duration ());
    lt_delay[1].set_length (LT_DELAYS[3] - SF_Frame::get_duration ());

    var_decay >> lt_decay[1];
}

void
Dattorro_Reverb::init_right_tank ()
{
    // build right tank subnetwork
    lt_feedback >> rt_decay_diffusor[0].inputs[Dattorro_Allpass::X];
    rt_decay_diffusor[0].outputs[Dattorro_Allpass::Y]
	>> rt_delay[0].inputs[Dattorro_Delay_Line::X];
    rt_delay[0].outputs[Dattorro_Delay_Line::Y]
	>> rt_damping_filter.inputs[Dattorro_1pole_Lowpass::X]
	>> rt_decay[0]
	>> rt_decay_diffusor[1].inputs[Dattorro_Allpass::X];
    rt_decay_diffusor[1].outputs[Dattorro_Allpass::Y]
	>> rt_delay[1].inputs[Dattorro_Delay_Line::X];
    rt_delay[1].outputs[Dattorro_Delay_Line::Y]
	>> rt_decay[1]
	>> rt_feedback;

    // initialize right tank subnetwork
    var_decay_diffusion_1 >> rt_decay_diffusor[0].inputs[Dattorro_Allpass::C];
    var_excursion >> rt_modulator.inputs[SF_Sine_Generator::A];
    var_mod_freq >> rt_modulator.inputs[SF_Sine_Generator::F];
    rt_modulator >> rt_mod_adder;
    rt_var_decay_diffusor_delay[0] >> rt_mod_adder
				   >> rt_decay_diffusor[0].inputs[Dattorro_Allpass::D];
    rt_var_decay_diffusor_delay[0].set_value(RT_DELAYS[0]);
    // take the modulation into account
    rt_decay_diffusor[0].set_length (RT_DELAYS[0] + var_excursion.get_value ());

    rt_var_delay[0] >> rt_delay[0].inputs[Dattorro_Delay_Line::D];
    rt_var_delay[0].set_value (RT_DELAYS[1]);
    rt_delay[0].set_length (RT_DELAYS[1]);

    var_damping >> rt_damping_filter.inputs[Dattorro_1pole_Lowpass::C];

    var_decay >> rt_decay[0];

    var_decay_diffusion_2 >> rt_decay_diffusor[1].inputs[Dattorro_Allpass::C];
    rt_var_decay_diffusor_delay[1] >> rt_decay_diffusor[1].inputs[Dattorro_Allpass::D];
    rt_var_decay_diffusor_delay[1].set_value(RT_DELAYS[2]);
    rt_decay_diffusor[1].set_length (RT_DELAYS[2]);

    rt_var_delay[1] >> rt_delay[1].inputs[Dattorro_Delay_Line::D];
    // take implicit delays into account in the feedback loops
    rt_var_delay[1].set_value (RT_DELAYS[3] - SF_Frame::get_duration ());
    rt_delay[1].set_length (RT_DELAYS[3] - SF_Frame::get_duration ());

    var_decay >> rt_decay[1];
}

void
Dattorro_Reverb::init_output ()
{
    // build output network
    lt_delay[0].outputs[Dattorro_Delay_Line::U1] >> out_left_negation[0] >> out_left_sum;
    lt_delay[1].outputs[Dattorro_Delay_Line::U1] >> out_left_negation[1] >> out_left_sum;
    lt_decay_diffusor[1].outputs[Dattorro_Allpass::U1] >> out_left_negation[2] >> out_left_sum;
    rt_decay_diffusor[1].outputs[Dattorro_Allpass::U1] >> out_left_negation[3] >> out_left_sum;
    rt_delay[0].outputs[Dattorro_Delay_Line::U1] >> out_left_sum;
    rt_delay[0].outputs[Dattorro_Delay_Line::U2] >> out_left_sum;
    rt_delay[1].outputs[Dattorro_Delay_Line::U1] >> out_left_sum;
    out_left_sum >> out_left_attenuator;
    //HERE'S the left output of the internal network

    rt_delay[0].outputs[Dattorro_Delay_Line::U3] >> out_right_negation[0] >> out_right_sum;
    rt_delay[1].outputs[Dattorro_Delay_Line::U2] >> out_right_negation[1] >> out_right_sum;
    rt_decay_diffusor[1].outputs[Dattorro_Allpass::U2] >> out_right_negation[2] >> out_right_sum;
    lt_decay_diffusor[1].outputs[Dattorro_Allpass::U2] >> out_right_negation[3] >> out_right_sum;
    lt_delay[0].outputs[Dattorro_Delay_Line::U2] >> out_right_sum;
    lt_delay[0].outputs[Dattorro_Delay_Line::U3] >> out_right_sum;
    lt_delay[1].outputs[Dattorro_Delay_Line::U2] >> out_right_sum;
    out_right_sum >> out_right_attenuator;
    //HERE'S the right output of the internal network

    // initialize output network
    rt_delay[0]		.set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP1));
    rt_delay[0]		.set_u2_delay ((unsigned int) rint(YL_OUTPUT_TAP2));
    rt_decay_diffusor[1].set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP3));
    rt_delay[1]		.set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP4));
    lt_delay[0]		.set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP5));
    lt_decay_diffusor[1].set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP6));
    lt_delay[1]		.set_u1_delay ((unsigned int) rint(YL_OUTPUT_TAP7));

    lt_delay[0]		.set_u2_delay ((unsigned int) rint(YR_OUTPUT_TAP1));
    lt_delay[0]		.set_u3_delay ((unsigned int) rint(YR_OUTPUT_TAP2));
    lt_decay_diffusor[1].set_u2_delay ((unsigned int) rint(YR_OUTPUT_TAP3));
    lt_delay[1]		.set_u2_delay ((unsigned int) rint(YR_OUTPUT_TAP4));
    rt_delay[0]		.set_u3_delay ((unsigned int) rint(YR_OUTPUT_TAP5));
    rt_decay_diffusor[1].set_u2_delay ((unsigned int) rint(YR_OUTPUT_TAP6));
    rt_delay[1]		.set_u2_delay ((unsigned int) rint(YR_OUTPUT_TAP7));

    var_attenuation >> out_left_attenuator;
    var_attenuation >> out_right_attenuator;
}

Dattorro_1pole_Lowpass::Dattorro_1pole_Lowpass (const char* name)
    : SF_Block (2,
		1,
		false,
		false,
		"1-pole lowpass filter",
		name),
      s_x (0), s_c (0), s_y (0), past_y (0)
{
    SF_Block::inputs = new SF_Input_Terminal[2];
    SF_Block::outputs = new SF_Output_Terminal[1];
    inputs[X].init_terminal ("x", this, false);
    inputs[C].init_terminal ("c", this, false);
    outputs[Y].init_terminal ("y", this);
}

Dattorro_1pole_Lowpass::~Dattorro_1pole_Lowpass ()
{
    delete[] SF_Block::inputs;
    SF_Block::inputs = 0;
    delete[] SF_Block::outputs;
    SF_Block::outputs = 0;
}

void
Dattorro_1pole_Lowpass::initialize ()
{
    s_x = inputs[X].get_first_source_frame ()->get_signal ();
    s_c = inputs[C].get_first_source_frame ()->get_signal ();
    s_y = outputs[Y].get_frame ()->get_signal ();
    past_y = 0;
}

void
Dattorro_1pole_Lowpass::execute ()
{
    SF_Length i, l;

    l = outputs[Y].get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	past_y = s_c[i] * s_x[i] + (1 - s_c[i]) * past_y;
	s_y[i] = past_y;
    }
}

void
Dattorro_1pole_Lowpass::finish ()
{
}

Dattorro_Delay_Line::Dattorro_Delay_Line (const char* name)
    : SF_Delay_Line (name),
      s_u1 (0), s_u2 (0), s_u3 (0),
      tap_u1 (0), tap_u2 (0), tap_u3 (0)
{
    SF_Delay_Line::delay_memory = MAX_DELAY;	// initialize the delay line length
    SF_Block::num_output_terminals = 4;		// change the number of output terminals
    SF_Block::class_name = "Modified delay line";	// and change the name also
    // reallocate the outputs array since we're increasing the number of output
    // terminals compared to the SF_Delay_Line
    delete[] SF_Block::outputs;
    SF_Block::outputs = new SF_Output_Terminal[4];
    outputs[Y].init_terminal ("y", this);
    outputs[U1].init_terminal ("u1", this);
    outputs[U2].init_terminal ("u2", this);
    outputs[U3].init_terminal ("u3", this);
}

Dattorro_Delay_Line::~Dattorro_Delay_Line ()
{
}

void
Dattorro_Delay_Line::initialize ()
{
    SF_Delay_Line::initialize ();
    s_u1 = outputs[U1].get_frame ()->get_signal ();
    s_u2 = outputs[U2].get_frame ()->get_signal ();
    s_u3 = outputs[U3].get_frame ()->get_signal ();
}

void
Dattorro_Delay_Line::execute ()
{
    SF_Length i, l;

    l = outputs[Y].get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_d[i] < 0)
	{
	    throw SF_Exception (this, "Negative delay");
	}
	if (s_d[i] > delay_memory)
	{
	    throw SF_Exception (this, "Delay exceeds maximum delay");
	}
	if (tap_u1 >= delay_memory_length
	    || tap_u2 >= delay_memory_length
	    || tap_u3 >= delay_memory_length)
	{
	    throw SF_Exception (this, "A non-interpolating tap exceeds delay memory length");
	}
	delay_line[current_index] = s_x[i];
	s_y[i] = interpolate_delay_line (s_d[i]);
	// fetch the 3 additional taps (non-interpolating):
	s_u1[i] = delay_line[((current_index - tap_u1 + delay_memory_length)
			      % delay_memory_length)];
	s_u2[i] = delay_line[((current_index - tap_u2 + delay_memory_length)
			      % delay_memory_length)];
	s_u2[i] = delay_line[((current_index - tap_u2 + delay_memory_length)
			      % delay_memory_length)];
	current_index = (current_index + 1) % delay_memory_length;
    }
}

void
Dattorro_Delay_Line::finish ()
{
    SF_Delay_Line::finish ();
}

Dattorro_Allpass::Dattorro_Allpass (const char* name)
    : Dattorro_Delay_Line (name),
      s_c (0)
{
    // reallocate the inputs array since we're increasing the number of input
    // terminals compared to Dattorro_Delay_Line
    delete[] SF_Block::inputs;
    SF_Block::inputs = new SF_Input_Terminal[3];
    inputs[X].init_terminal ("x", this, false);
    inputs[D].init_terminal ("d", this, false);
    inputs[C].init_terminal ("c", this, false);
    SF_Block::class_name = "Allpass filter";	// change the name
}

Dattorro_Allpass::~Dattorro_Allpass ()
{
}

void
Dattorro_Allpass::initialize ()
{
    Dattorro_Delay_Line::initialize ();
    s_c = inputs[C].get_first_source_frame ()->get_signal ();
}

void
Dattorro_Allpass::execute ()
{
    SF_Length i, l;
    SF_Sample t;

    l = outputs[Y].get_frame ()->get_num_rows ();
    for (i = 0; i < l; i++)
    {
	if (s_d[i] < 0)
	{
	    throw SF_Exception (this, "Negative delay");
	}
	if (s_d[i] > delay_memory)
	{
	    throw SF_Exception (this, "Delay exceeds maximum delay");
	}
	t = interpolate_delay_line (s_d[i]);
	delay_line[current_index] = s_x[i] - s_c[i] * t;
	s_y[i] = s_c[i] * s_x[i] + t;
	// fetch the 3 additional taps (non-interpolating):
	s_u1[i] = delay_line[((current_index - tap_u1 + delay_memory_length)
			      % delay_memory_length)];
	s_u2[i] = delay_line[((current_index - tap_u2 + delay_memory_length)
			      % delay_memory_length)];
	s_u2[i] = delay_line[((current_index - tap_u2 + delay_memory_length)
			      % delay_memory_length)];
	current_index = (current_index + 1) % delay_memory_length;
    }
}

void
Dattorro_Allpass::finish ()
{
    Dattorro_Delay_Line::finish ();
}

// This is a HACK: a multiplier which is a consumer.  We use these in the
// outputs of the internal network to prevent BFS from arguing about not having
// any sinks.
Dattorro_Consumer_Multiplier::Dattorro_Consumer_Multiplier (const char* name)
    : SF_Multiplier (name)
{
    SF_Block::consumer = true;
}

/* EOF */
