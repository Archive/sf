/* Reverb program
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <stdlib.h>

#include <sf/sf.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>

#include "dattorro_reverb.hh"

int
main (int argc, char* argv[])
{
    SF_initialize ();
    atexit (SF_finish);

    if (argc != 10)
    {
	cerr << "Usage:" << endl
	     << "reverb <dur> <infile> <loutfile> <routfile>" << endl
	     << "<dur>:      simulation duration            [ms]" << endl
	     << "<infile>:   audio file to read from" << endl
	     << "<loutfile>: audio file to write left channel to (WAV)" << endl
	     << "<routfile>: audio file to write right channel to (WAV)" << endl << endl
	     << " === Example use ===" << endl
	     << "./reverb 5 helmi.wav revexample_left.wav revexample_right.wav" << endl;
	exit (EXIT_FAILURE);
    }
    try
    {
	int i, num_rounds;

	SF_Network nw ("Reverb");
	SF_File_Input in ("Input");
	SF_File_Output left_out ("Left output");
	SF_File_Output right_out ("Right output");
	Dattorro_Reverb r ("Dattorro reverb algorithm");

	nw.add_block (in);
	nw.add_block (left_out);
	nw.add_block (right_out);
	nw.add_block (r);

	in >> r;
	r.get_output ("yl") >> left_out;
	r.get_output ("yr") >> right_out;

	in.set_file_name (argv[2]);
	left_out.set_file_name (argv[3]);
	right_out.set_file_name (argv[4]);

	nw.print (cout);

	nw.initialize ();
	num_rounds = (int)rint (atof (argv[1])
				* 1e3 / SF_Frame::get_duration ());
	for (i = 0; i < num_rounds; i++)
	{
	    nw.execute ();
	}
	nw.finish ();
    }
    catch (SF_Exception e)
    {
	cerr << "Exception";
	if (e.origin != 0)
	{
	    cerr << " in block \"" << e.origin->get_name () << "\"";
	}
	cerr << ": " << e.description << endl;
	exit (EXIT_FAILURE);
    }

    exit (EXIT_SUCCESS);
}

/* EOF */
