/* Binary_Tree
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>	//cerr
#include <stdlib.h>	//atof()

#include <sf/network.hh>
#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/exception.hh>

#include <sf/blocks/sine_generator.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/variable.hh>

#include "afm_tree.hh"

AFM_Tree::AFM_Tree () : Binary_Tree ()
{
}

AFM_Tree::~AFM_Tree ()
{
}

bool
AFM_Tree::parse (char* expr)
    //Function for parsing a binary tree from a text string.
    //Legal binary tree expression 
    // - (A, B)
    //
    //Node content 'A' and 'B' may be any string
    //which doesn't contain a '(', a ')' nor a ',' OR
    //another expression '(C, D)' recursively. Paretheses
    //must be well found.
    //
    //For example the string '(((A, B), C), (D, E))' is
    //a legal binary tree expression where as 'A, B' and
    //'((A, B) C)' are not (missing parentheses and a missing
    //comma respectively).
{
    int i;
    Node * current;

    initialize_tree ();
    i = 0;
    current = 0;
    try {
	while (expr[i] != '\0')
	{
	    //Remove preceeding 'empty' characters
	    while (expr[i] == ' ' ||
		   expr[i] == '\t' ||
		   expr[i] == '\n')
	    {
		i++;
	    }
	    switch (expr[i])
	    {
		case '\0':
		    break;
		case BEGIN_SEPARATOR:
		    if (current == 0)
		    {
			initialize_tree ();
			add_left_child (root);
			current = root->left;
		    }
		    else
		    {
			add_left_child (current);
			current = current->left;
		    }
		    i++;
		    break;
		case END_SEPARATOR:
		    if (current == 0)
		    {
			throw Bin_Tree_Exception("Missing 'BEGIN_SEPARATOR'");
		    }
		    if (current->parent == 0)
		    {
			throw Bin_Tree_Exception("Illegally placed 'END_SEPARATOR'");
		    }
		    else
		    {
			current = current->parent;
		    }
		    i++;
		    break;
		case SINE_SEPARATOR:
		    if (current == 0)
		    {
			throw Bin_Tree_Exception("Missing �BEGIN_SEPARATOR'");
		    }
		    if (current->parent == 0)
		    {
			throw Bin_Tree_Exception("Illegally placed 'SINE_SEPARATOR'");
		    }
		    add_right_child (current->parent);
		    current->parent->content = new char[2];
		    current->parent->content[0] = SINE_SEPARATOR;
		    current->parent->content[1] = '\0';
		    current = current->parent->right;
		    i++;
		    break;
		case ADDER_SEPARATOR:
		    if (current == 0)
		    {
			throw Bin_Tree_Exception("Missing �BEGIN_SEPARATOR'");
		    }
		    if (current->parent == 0)
		    {
			throw Bin_Tree_Exception("Illegally placed 'ADDER_SEPARATOR'");
		    }
		    add_right_child (current->parent);
		    current->parent->content = new char[2];
		    current->parent->content[0] = ADDER_SEPARATOR;
		    current->parent->content[1] = '\0';
		    current = current->parent->right;
		    i++;
		    break;
		default:
		    if (current == 0)
		    {
			throw Bin_Tree_Exception("Missing 'BEGIN_SEPARATOR'");
		    }
		    if (!is_leaf (current))
		    {
			throw Bin_Tree_Exception("Node not a leaf");
		    }
		    if (current->content != 0)
		    {
			throw Bin_Tree_Exception("Node already has a value");
		    }
		    int j;
		    j = 0;
		    while (expr[i + j] != SINE_SEPARATOR &&
			   expr[i + j] != ADDER_SEPARATOR &&
			   expr[i + j] != END_SEPARATOR &&
			   expr[i + j] != BEGIN_SEPARATOR)
		    {
			if (expr[i + j] == '\0')
			{
			    throw Bin_Tree_Exception("Possibly missing 'END_SEPARATOR'");
			}
			j++;
		    }
		    j--;
		    //Remove succeeding 'empty' characters
		    while (expr[i + j] == ' ' ||
			   expr[i + j] == '\t' ||
			   expr[i + j] == '\n')
		    {
			j--;
		    }
		    j++;
		    current->content = new char[j+1];
		    for (int k = 0; k < j; k++)
		    {
			current->content[k] = expr[i + k];
		    }
		    current->content[j] = '\0';
		    i = i + j;
	    }
	}
	if (current != root)
	{
	    throw Bin_Tree_Exception ("Missing 'END_SEPARATOR'");
	}
    }
    catch (Bin_Tree_Exception e)
    {
       	cerr << "Parse error: " << e.description << endl;
	clear ();
	return false;
    }
    if (sanity_check (root) == false)
    {
	cerr << "Sanity error: possibly missing nodes!" << endl;
	clear ();
	return false;
    }
    return true;
}


bool
AFM_Tree::is_sine(Node* n)
{
    if (n->content[0] == SINE_SEPARATOR)
    {
	return true;
    }
    return false;
}

bool
AFM_Tree::is_adder(Node* n)
{
    if (n->content[0] == ADDER_SEPARATOR)
    {
	return true;
    }
    return false;
}

void
AFM_Tree::generate_afm_synthesizer (SF_Network& net, SF_Input_Terminal& dest)
{
    if (num_nodes == 0)
    {
	throw SF_Exception (0, "Cannot create a network from empty tree");
    }
    //Allocate list of SF_Block pointers
    blocklist = new SF_Block*[num_nodes];
    //Generate, add and connect blocks recursively beginning from the root
    generate_block(root, 0, net);
    //Connect the root (index 0) to fileout
    blocklist[0]->get_output (0).connect(dest);
}

void
AFM_Tree::generate_block (Node* n, SF_Input_Terminal* dest, SF_Network& net)
{
    static int i = 0;
    i++;
    if (i >= num_nodes)
    {
	//This function (generate_block) should be called exactly 'num_blocks' times
	throw SF_Exception (0, "AFM Synthesizer internal error");	
    }
    if (is_leaf(n))	//A node is a variable if and only if it is a leaf.
    {
	SF_Sample s = atof(n->content);
	blocklist[i] = new SF_Variable("AFM Variable");
	((SF_Variable*)blocklist[i])->set_value(s);
	net.add_block(*blocklist[i]);
	if (dest != 0)
	    blocklist[i]->get_output (0).connect(*dest);
    }
    else
    {
	if (n == root)
	{
	    i = 0;
	}
	if (is_sine(n))
	{
	    SF_Sine_Generator* temp = new SF_Sine_Generator("AFM Sine");
	    blocklist[i] = temp;
	    net.add_block(*blocklist[i]);
	    if (dest != 0)
		blocklist[i]->get_output (0).connect(*dest);
	    //Connect left child to FREQ
	    generate_block (n->left, &(temp->get_input (0)), net);
	    //Connect right child to AMP
	    generate_block (n->right, &(temp->get_input (1)), net);
	    return;
	}
	if (is_adder(n))
	{
	    SF_Adder* temp = new SF_Adder("AFM Adder");
	    blocklist[i] = temp;
	    net.add_block(*blocklist[i]);
	    if (dest != 0)
		blocklist[i]->get_output (0).connect(*dest);
	    //Connect both children to X
	    generate_block (n->left, &(temp->get_input (0)), net);
	    generate_block (n->right, &(temp->get_input (0)), net);
	    return;
	}
	//All non-leaf nodes ought to be either sines or adders
	throw SF_Exception (0, "AFM Synthesizer internal error");	
    }
}

