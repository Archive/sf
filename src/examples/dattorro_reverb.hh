/* Dattorro reverb header file
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */



// This algorithm is based on the following article:
// Dattorro, Jon,
// ``Effect Design --- Part 1: Reverberator and Other Filters.''
// Journal of the Audio Engineering Society,
// Vol. 45, No. 9, pp. 660--684, September 1997


#ifndef __DATTORRO_REVERB_HH__
#define __DATTORRO_REVERB_HH__

#include <math.h>

#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/adder.hh>
#include <sf/blocks/delay_line.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/negation.hh>
#include <sf/blocks/sine_generator.hh>
#include <sf/blocks/variable.hh>

// DEBUG
#include <sf/blocks/file_output.hh>
// Transform the sample indices presented in the paper to milliseconds.  The
// algorithm on the paper uses a sample rate of 29761 Hz.
const double PAPER_FS_SCALING = 1e3 / 29761;

// The default values for the parameters according to the paper.
const double DEFAULT_EXCURSION =	PAPER_FS_SCALING * 16; //ms
const double DEFAULT_DECAY =		0.50;
const double DEFAULT_DECAY_DIFFUSION_1 = 0.70;
const double DEFAULT_DECAY_DIFFUSION_2 = 0.50;
const double DEFAULT_INPUT_DIFFUSION_1 = 0.750;
const double DEFAULT_INPUT_DIFFUSION_2 = 0.625;
const double DEFAULT_BANDWIDTH =	0.9995;
const double DEFAULT_DAMPING =		0.0005;
const double DEFAULT_PREDELAY =		10; //ms
const double DEFAULT_MOD_FREQ =		1; //Hz
const double DEFAULT_ATTENUATION =	0.6; //Hz
					
const double PRE_DELAYS[4] =		{PAPER_FS_SCALING * 142,
					 PAPER_FS_SCALING * 107,
					 PAPER_FS_SCALING * 379,
					 PAPER_FS_SCALING * 277};
// LT_DELAYS[3] and RT_DELAYS[3] need be greater than SF_DEFAULT_FRAME_DURATION
// (1 ms) due to implicit delays in the feedback loops.
const double LT_DELAYS[4] =		{PAPER_FS_SCALING * 672,
					 PAPER_FS_SCALING * 4453, //max
					 PAPER_FS_SCALING * 1800,
					 PAPER_FS_SCALING * 3720}; // >1 ms
const double RT_DELAYS[4] =		{PAPER_FS_SCALING * 908,
					 PAPER_FS_SCALING * 4217,
					 PAPER_FS_SCALING * 2656,
					 PAPER_FS_SCALING * 3163}; // >1 ms
const double MAX_DELAY =		LT_DELAYS[1]; //max of all _DELAYS

// The output tap structure (for the output network):
const double YL_OUTPUT_TAP1 =		PAPER_FS_SCALING * 266;  //rt_delay[0]
const double YL_OUTPUT_TAP2 =		PAPER_FS_SCALING * 2974; //rt_delay[0]
const double YL_OUTPUT_TAP3 =		PAPER_FS_SCALING * 1913; //rt_decay_diffusor[1]
const double YL_OUTPUT_TAP4 =		PAPER_FS_SCALING * 1996; //rt_delay[1]
const double YL_OUTPUT_TAP5 =		PAPER_FS_SCALING * 1990; //lt_delay[0]
const double YL_OUTPUT_TAP6 =		PAPER_FS_SCALING * 187;  //lt_decay_diffusor[1]
const double YL_OUTPUT_TAP7 =		PAPER_FS_SCALING * 1066; //lt_delay[1]
const double YR_OUTPUT_TAP1 =		PAPER_FS_SCALING * 353;  //lt_delay[0]
const double YR_OUTPUT_TAP2 =		PAPER_FS_SCALING * 3627; //lt_delay[0]
const double YR_OUTPUT_TAP3 =		PAPER_FS_SCALING * 1228; //lt_decay_diffusor[1]
const double YR_OUTPUT_TAP4 =		PAPER_FS_SCALING * 2673; //lt_delay[1]
const double YR_OUTPUT_TAP5 =		PAPER_FS_SCALING * 2111; //rt_delay[0]
const double YR_OUTPUT_TAP6 =		PAPER_FS_SCALING * 335;  //rt_decay_diffusor[1]
const double YR_OUTPUT_TAP7 =		PAPER_FS_SCALING * 121;  //rt_delay[1]

class Dattorro_1pole_Lowpass : public SF_Block
{  
public:  
    Dattorro_1pole_Lowpass (const char* name = 0);
    virtual ~Dattorro_1pole_Lowpass ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

    enum Input_Indices {X, C};
    enum Output_Indices {Y};

private:
    SF_Sample*		s_x;
    SF_Sample*		s_c;
    SF_Sample*		s_y;
    SF_Sample		past_y;
};

class Dattorro_Delay_Line : public SF_Delay_Line
// A modified version of the standard delay line; here we add a second and a
// third fixed tap output.
{
public:  
    Dattorro_Delay_Line (const char* name = 0);
    virtual ~Dattorro_Delay_Line ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

    enum Input_Indices {X, D};
    enum Output_Indices {Y, U1, U2, U3};

    inline void		set_u1_delay (unsigned int tap);
    inline void		set_u2_delay (unsigned int tap);
    inline void		set_u3_delay (unsigned int tap);

protected:
    // the other terminals' pointers are in the SF_Delay_Line base class
    SF_Sample*		s_u1;
    SF_Sample*		s_u2;
    SF_Sample*		s_u3;
    unsigned int	tap_u1;
    unsigned int	tap_u2;
    unsigned int	tap_u3;
};

class Dattorro_Allpass : public Dattorro_Delay_Line
{  
public:  
    Dattorro_Allpass (const char* name = 0);
    virtual ~Dattorro_Allpass ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

    enum Input_Indices {X, D, C};
    enum Output_Indices {Y, U1, U2, U3};

protected:
    SF_Sample*		s_c;
};

class Dattorro_Consumer_Multiplier : public SF_Multiplier
// This is a HACK: a multiplier which is a consumer.  We use these in the
// outputs of the internal network to prevent BFS from arguing about not having
// any sinks.
{
public:  
    Dattorro_Consumer_Multiplier (const char* name = 0);
};

class Dattorro_Reverb : public SF_Block
{  
public:  
    Dattorro_Reverb (const char* name = 0);
    virtual ~Dattorro_Reverb ();

    virtual void	print (ostream& stream) const;

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

    enum Input_Indices {X};
    enum Output_Indices {YL, YR};

    inline void		set_excursion (SF_Sample excursion);
    inline void		set_decay (SF_Sample decay);
    inline void		set_decay_diffusion_1 (SF_Sample decay_diffusion_1);
    inline void		set_decay_diffusion_2 (SF_Sample decay_diffusion_2);
    inline void		set_input_diffusion_1 (SF_Sample input_diffusion_1);
    inline void		set_input_diffusion_2 (SF_Sample input_diffusion_2);
    inline void		set_bandwidth (SF_Sample bandwidth);
    inline void		set_damping (SF_Sample damping);
    inline void		set_predelay (SF_Sample predelay);
    inline void		set_mod_freq (SF_Sample mod_freq);
    inline void		set_attenuation (SF_Sample attenuation);

private:
    void		add_blocks ();
    void		init_preprocessing ();
    void		init_left_tank ();
    void		init_right_tank ();
    void		init_output ();

    SF_Sample*		s_x;
    SF_Sample*		s_yl;
    SF_Sample*		s_yr;
    SF_Sample*		nw_s_yl;
    SF_Sample*		nw_s_yr;

    SF_Network		nw;	// there is an internal network here

    // DEBUG
    SF_File_Output	debug_pre, debug_lt1, debug_lt2, debug_rt1, debug_rt2;

    // network variables
    SF_Variable			var_excursion;
    SF_Variable			var_decay;
    SF_Variable			var_decay_diffusion_1;
    SF_Variable			var_decay_diffusion_2;
    SF_Variable			var_input_diffusion_1;
    SF_Variable			var_input_diffusion_2;
    SF_Variable			var_bandwidth;
    SF_Variable			var_damping;
    SF_Variable			var_predelay;
    SF_Variable			var_mod_freq;
    SF_Variable			var_attenuation;

    // the preprocessing subnetwork
    SF_Multiplier		pre_attenuator;
    SF_Variable			pre_var_attenuation;
    SF_Delay_Line		pre_predelay;
    Dattorro_1pole_Lowpass	pre_bandwidth_filter;
    Dattorro_Allpass		pre_input_diffusor[4];
    SF_Variable			pre_var_input_diffusor_delay[4];

    // the left tank subnetwork
    Dattorro_Allpass		lt_decay_diffusor[2];
    SF_Sine_Generator		lt_modulator;
    SF_Adder			lt_mod_adder;
    Dattorro_Delay_Line	lt_delay[2];
    Dattorro_1pole_Lowpass	lt_damping_filter;
    SF_Multiplier		lt_decay[2];
    SF_Variable			lt_var_decay_diffusor_delay[2];
    SF_Variable			lt_var_delay[2];
    SF_Adder			lt_feedback;

    // the right tank subnetwork
    Dattorro_Allpass		rt_decay_diffusor[2];
    SF_Sine_Generator		rt_modulator;
    SF_Adder			rt_mod_adder;
    Dattorro_Delay_Line	rt_delay[2];
    Dattorro_1pole_Lowpass	rt_damping_filter;
    SF_Multiplier		rt_decay[2];
    SF_Variable			rt_var_decay_diffusor_delay[2];
    SF_Variable			rt_var_delay[2];
    SF_Adder			rt_feedback;

    // output network
    SF_Negation			out_left_negation[4];
    SF_Adder			out_left_sum;
    Dattorro_Consumer_Multiplier out_left_attenuator;
    SF_Negation			out_right_negation[4];
    SF_Adder			out_right_sum;
    Dattorro_Consumer_Multiplier out_right_attenuator;
};

inline void
Dattorro_Reverb::set_excursion (SF_Sample excursion)
{
    var_excursion.set_value (excursion);
}

inline void
Dattorro_Reverb::set_decay (SF_Sample decay)
{
    var_decay.set_value (decay);
}

inline void
Dattorro_Reverb::set_decay_diffusion_1 (SF_Sample decay_diffusion_1)
{
    var_decay_diffusion_1.set_value (-decay_diffusion_1); //NOTE sign
}

inline void
Dattorro_Reverb::set_decay_diffusion_2 (SF_Sample decay_diffusion_2)
{
    var_decay_diffusion_2.set_value (decay_diffusion_2);
}

inline void
Dattorro_Reverb::set_input_diffusion_1 (SF_Sample input_diffusion_1)
{
    var_input_diffusion_1.set_value (input_diffusion_1);
}

inline void
Dattorro_Reverb::set_input_diffusion_2 (SF_Sample input_diffusion_2)
{
    var_input_diffusion_2.set_value (input_diffusion_2);
}

inline void
Dattorro_Reverb::set_bandwidth (SF_Sample bandwidth)
{
    var_bandwidth.set_value (bandwidth);
}

inline void
Dattorro_Reverb::set_damping (SF_Sample damping)
{
    var_damping.set_value (1 - damping); //NOTICE
}

inline void
Dattorro_Reverb::set_predelay (SF_Sample predelay)
{
    var_predelay.set_value (predelay);
}

inline void
Dattorro_Reverb::set_mod_freq (SF_Sample mod_freq)
{
    var_mod_freq.set_value (mod_freq);
}

inline void
Dattorro_Reverb::set_attenuation (SF_Sample attenuation)
{
    var_attenuation.set_value (attenuation);
}

inline void
Dattorro_Delay_Line::set_u1_delay (unsigned int tap)
{
    tap_u1 = tap;
}

inline void
Dattorro_Delay_Line::set_u2_delay (unsigned int tap)
{
    tap_u2 = tap;
}

inline void
Dattorro_Delay_Line::set_u3_delay (unsigned int tap)
{
    tap_u3 = tap;
}

#endif
/* EOF */
