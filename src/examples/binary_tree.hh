/* Binary_Tree
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __BINARY_TREE_HH__
#define __BINARY_TREE_HH__

const char NODE_SEPARATOR = ',';
const char BEGIN_SEPARATOR = '(';
const char END_SEPARATOR = ')';

class Binary_Tree
{
public:
    Binary_Tree ();
    virtual ~Binary_Tree ();
    virtual bool parse (char* expr);
    void clear ();
    void print ();
    inline int get_num_nodes ();
protected:
    struct Node
    {
	char* content;
	Node* parent;
	Node* left;
	Node* right;
    };
    Node* root;
    int num_nodes;
    void initialize_tree ();
    bool is_leaf (Node* n);
    bool is_single (Node* n);
    bool sanity_check (Node* n);
    bool add_left_child (Node* n);
    bool add_right_child (Node* n);
    void remove_node (Node* n);
    void print_tree (Node* n);
};

inline int
Binary_Tree::get_num_nodes ()
{
    return num_nodes;
}

class Bin_Tree_Exception
{
public:
    Bin_Tree_Exception (char* desc);
    ~Bin_Tree_Exception ();
    char* description;
};

#endif

/* EOF */

