/* SF_List
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/exception.hh>
#include <sf/list.hh>

SF_List::SF_List () throw (SF_Memory_Exception)
    // Create an empty list.
{
    nil = new SF_List_Node;
    nil->next = nil;
    nil->prev = nil;
    nil->content = 0;
    current = nil;
    current_index = -1;
    num_nodes = 0;
}

SF_List::~SF_List () throw ()
    // Destructor
{
    SF_List_Node* temp;
    SF_List_Node* curr;

    if (nil == 0)
    {
	return;
    }
    curr = nil->next;
    while (curr != nil)
    {
	temp = curr;
	curr = curr->next;
	delete temp;
	temp = 0;
    }
    delete nil;
    nil = 0; 
}


void
SF_List::add (const void* content) throw (SF_Memory_Exception)
    // Add a new node to the end of the list.  Argument "content" is the content
    // (a pointer) to be placed to the added node.
{
    SF_List_Node* a;
    a = new SF_List_Node;
    a->prev = nil->prev;
    nil->prev->next = a;
    nil->prev = a;
    a->next = nil;
    a->content = content;
    num_nodes++;
}

void
SF_List::remove (const void* content) throw (SF_Exception)
    // Remove a node from the list.  Argument "content" is the content of the
    // node to be removed.
{
    SF_List_Node* a;
    a = search (content);
    if (a == 0)
    {
	throw SF_Exception (0, "Node not found");
    }
    a->prev->next = a->next;
    a->next->prev = a->prev;
    delete a;
    num_nodes--;
    current = nil;
    current_index = -1;
}

bool
SF_List::find (const void* content) throw ()
    // Find out if a certain node is on the list.  Argument "content" is the
    // content of the node to be looked for.  Returns true if there is a node
    // with the specified content.

    // Doesn't throw exceptions.
{
    SF_List_Node* a;
    a = search (content);
    return a != 0;
}

SF_List::SF_List_Node* 
SF_List::search (const void* content) throw ()
    // Search for a certain node in the list and return the node data
    // structure.

    // Doesn't throw exceptions.
{
    SF_List_Node* a;
    if (nil == 0)
    {
	return 0;
    }
    a = nil->next;
    while ((a != nil) && (a->content != content))
    {
	a = a->next;
    }
    if (a == nil)
    {
	return 0;
    }
    return a;
}

void*
SF_List::get_node (int index) throw (SF_Indexing_Exception)
    // Return the content pointer on the index'th node on the list.  Index 0
    // refers to the first element on the list.  If the index is out of bounds,
    // throws an exception.
{
    if (index < 0 || index >= num_nodes)
    {
	throw SF_Indexing_Exception (0, "Index out of bounds");
    }
    while (current_index != index)
    {
	if (current_index < index)
	{
	    current = current->next;
	    current_index++;
	}
	if (current_index > index)
	{
	    current = current->prev;
	    current_index--;
	}
    }
    return (void*) (current->content);
}

void
SF_List::sort (int (*comparison_function) (const void* a, const void* b)) throw (SF_Exception)
    // Sort the list in ascending order according to the user supplied
    // comparison function.  The comparison function must return an integer less
    // than, equal to, or greater than zero to indicate if the content "a" is to
    // be considered less than, equal to, or greater than the content "b",
    // respectively.  Uses the insertion sort algorithm.
{
    SF_List_Node* ready = nil->next;
    SF_List_Node* n1 = ready->next;
    SF_List_Node* n2 = 0;
    if (comparison_function == 0)
    {
	throw SF_Exception (0, "Comparison function must be specified (pointer == 0)");
    }

    // reset the "official" current pointers
    current = nil;
    current_index = -1;

    while (n1 != nil)
    {
	//if n1 is already in the correct position
	if (comparison_function (ready->content, n1->content) <= 0)
	{
	    ready = n1;
	}
	else
	{
	    //remove n1 from the list
	    n1->prev->next = n1->next;
	    n1->next->prev = n1->prev;
	    n2 = ready->prev;
	    //short circuit evaluation if n2 == nil
	    while (n2 != nil
		   && comparison_function (n2->content, n1->content) > 0)
	    {
		//search backwards for the correct position
		n2 = n2->prev;
	    }
	    //insert n1 back to the list
	    n1->next = n2->next;
	    n1->prev = n2->next->prev;
	    n2->next->prev = n1;
	    n2->next = n1;
	}
	n1 = ready->next;
    }
}


/* EOF */
