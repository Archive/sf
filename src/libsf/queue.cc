/* SF_Queue
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/queue.hh>
#include <sf/exception.hh>

SF_Queue::SF_Queue () throw ()
{
    head = 0;
    tail = 0;
}

SF_Queue::~SF_Queue () throw ()
{
    SF_Queue_Node* temp;
    while (head != 0)
    {
	temp = head;
	head = head->next;
	delete temp;
	temp = 0;
    }
}

void
SF_Queue::enqueue (const void* content) throw (SF_Memory_Exception)
    //Add a new node to the end of the queue.
{
    SF_Queue_Node* a = new SF_Queue_Node;
    a->content = content;
    a->next = 0;
    if (head == 0)
    {
	head = a;
    }
    else
    {
	tail->next = a;
    }
    tail = a;
}

void*
SF_Queue::dequeue () throw (SF_Exception)
    //Remove the first node of the queue.
{
    if (is_empty ())
    {
	throw SF_Exception (0, "Queue underflow");
    }
    else
    {
	const void* a = head->content;
	SF_Queue_Node* t = head;
	head = head->next;
	delete t;
	return (void*)a;
    }
}

/* EOF */
