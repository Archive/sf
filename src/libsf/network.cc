/* SF_Network
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>

#include <sf/block.hh>
#include <sf/bubble_terminal.hh>
#include <sf/exception.hh>
#include <sf/global.hh>
#include <sf/list.hh>
#include <sf/network.hh>
#include <sf/typedefs.h>

SF_Network::SF_Network (const char* name) throw (SF_Exception)
    : SF_Block (false,
		false,
		0,
		name),
      block_list (0)
    // Construct an empty network.
{
    block_list = new SF_List;
}

SF_Network::~SF_Network () throw ()
    // Destruct a network
{
    int i;
    // FIXME SIGSEGV: Remove all remaining blocks from the network
//    for (i = 0; i < get_num_blocks (); i++)
//    {
//	remove_block (*(get_block_node (i)->block));
//    }
    // Destroy (i.e. free) all inputs and outputs
    for (i = 0; i < get_num_inputs (); i++)
    {
	destroy_input (i);
    }
    for (i = 0; i < get_num_outputs (); i++)
    {
	destroy_output (i);
    }
    delete block_list;
    block_list = 0;
}

void
SF_Network::initialize () throw (SF_Exception, SF_Network_Sanity_Exception)
    // Initialize the network prior to simulation.  This function must be called
    // at the beginning of a simulation run in order to be able to simulate the
    // network.  Every block is initialized.  Note that the sample rates, input
    // and output frame pointers have all been initialized and the frames have
    // all been cleared at the time of calling the initialize () function of an
    // individual block.

    // Throws exceptions: SF_Exception, SF_Network_Sanity_Exception.
{
    int i;

    // Sanity check: make sure every block belongs to this network
    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->block->get_host_network () != this)
	{
	    throw SF_Exception (get_block_node (i)->block,
				"Block doesn't belong to this network");
	}
    }
    // partition the network
    partition_network ();

    // clear the frames within every output terminal of every block and
    // initialize every block; however, ignore blocks with rank == 0
    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->rank != 0)
	{
	    clear_frames (get_block_node (i)->block);
	    get_block_node (i)->block->initialize ();
	}
    }

    // Make sure every block used in the simulation (rank != 0) is functional;
    // this checks the sample rates also.
    check_functionality ();

    // Check that the input and output bubbles are wired appropriately.
    check_bubble_wirings ();
}

void
SF_Network::execute () throw (SF_Exception)
    // Execute one round of simulation.  In the course of one simulation round
    // the amount of data which passes around is equal to the duration of one
    // frame.  This means that each call to execute () produces one frame's
    // duration of output signal(s).  Note that the network must have been
    // initialized via a call to initialize () prior to execution.
{
    int i;

    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->rank != 0)
	{
	    get_block_node (i)->block->execute ();
	}
    }
}

void
SF_Network::finish () throw (SF_Exception)
    // Clean up the network after simulation.  This function must be called at
    // the end of a simulation run in order to ensure that all resources
    // allocated for the simulation get freed.
{
    int i;

    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->rank != 0)
	{
	    get_block_node (i)->block->finish ();
	}
    }
}

void
SF_Network::print (ostream& stream) const throw ()
    // Print a textual description of the contents of the network.  The printout
    // contains general information on the network as well as a detailed
    // printout of each block.

    // Doesn't throw exceptions.
{
    int num_wires, i, j;

    // print the network in order
    //partition_network ();

    // count the number of wires
    num_wires = 0;
    for (i = 0; i < get_num_blocks (); i++)
    {
	for (j = 0;
	     j < get_block_node (i)->block->get_num_inputs ();
	     j++)
	{
	    num_wires += get_block_node (i)->block->get_input (j).get_num_wires ();
	}
    }

    stream << "Network: \"" << name << "\" ("
	   << get_num_blocks () << " block(s), "
	   << num_wires << " wire(s))" << endl;

    for (i = 0; i < get_num_blocks (); i++)
    {
	get_block_node (i)->block->print (stream);
	//debug
//	stream << "DEBUG: funct=" << node->block->is_functional ()
//	       << ", rank=" << node->rank
//	       << ", prod=" << node->block->is_producer ()
//	       << ", cons=" << node->block->is_consumer ()
//	       << endl;
    }
}

bool
SF_Network::is_functional () const throw ()
    // Inspect whether the network is functional as whole and return true if it
    // is.
{
    try
    {
	check_functionality ();
    }
    catch (SF_Exception e)
    {
	(void)e; // I guess the MipsPro CC requires this
	return false;
    }
    return true;
}

void
SF_Network::add_block (SF_Block& block) throw (SF_Exception)
    // Add an existing block to the network.
{
    SF_Network_Block_List_Content* new_node = 0;
    new_node = new SF_Network_Block_List_Content;
    new_node->rank = 0;
    new_node->is_visited = false;
    new_node->block = &block;
    block_list->add ((const void*)new_node);
    block.set_host_network (this);
}

void
SF_Network::remove_block (SF_Block& block) throw (SF_Exception)
    // Remove an existing block from the network.  Throws an exception if the
    // block is not in the network.
{
    SF_Network_Block_List_Content* node = 0;
    // search the block
    node = search_block_list (&block);
    if (node == 0)
    {
	throw SF_Exception (&block, "Block to remove is not in the network");
    }
    // disconnect every connection of the block
    unwire_block (&block);
    block.set_host_network (0);
    // remove the block from the list
    block_list->remove ((const void*)node);
    delete node;
    node = 0;
}

void
SF_Network::check () throw (SF_Exception, SF_Network_Sanity_Exception)
    // Check the sanity of the connections and sample rates in the network and
    // throw an exception on insane cases.  This function call partitions the
    // network, initializes sample rates and reallocates frames as a side
    // effect.
{
    partition_network ();
    // FIXME???
}

void
SF_Network::create_input (const char* name) throw (SF_Exception)
    // Create both an input terminal and an associated input bubble.  The input
    // terminal is used from outside the network and the input bubble is used
    // from inside the network.  Technically this function creates a single
    // SF_Bubble_Terminal, which functions both as an input and and output
    // terminal.
{
    SF_Bubble_Terminal* t = 0;
    t = new SF_Bubble_Terminal (name, this, true);
    add_input_terminal (t);
}

void
SF_Network::create_output (const char* name) throw (SF_Exception)
    // Create both an output terminal and an associated output bubble.  The
    // output terminal is used from outside the network and the output bubble is
    // used from inside the network.  Technically this function creates a single
    // SF_Bubble_Terminal, which functions both as an input and and output
    // terminal.
{
    SF_Bubble_Terminal* t = 0;
    t = new SF_Bubble_Terminal (name, this, false);
    add_output_terminal (t);
}

void
SF_Network::destroy_input (const char* name) throw (SF_Exception)
    // Destroy a named input terminal and the associated and similarly named
    // input bubble.  This may cause the numbering of the remaining inputs to
    // change.
{
    SF_Bubble_Terminal* t = 0;
    t = (SF_Bubble_Terminal*)&get_input (name);
    try
    {
	remove_input_terminal (t);
    }
    catch (SF_Exception)
    {
	// make sure the terminal gets destructed even in case of an error
	delete t;
	throw;
    }
    delete t;
}

void
SF_Network::destroy_input (int index) const throw (SF_Indexing_Exception)
    // Destroy an input terminal and the associated input bubble by index.  This
    // may cause the numbering of the remaining inputs to change.
{
    SF_Bubble_Terminal* t = 0;
    t = (SF_Bubble_Terminal*)&get_input (index);
    try
    {
//	remove_input_terminal (t);
	// Uglily cast away the constness to satisfy GCC 2.95 (FIXME!)
	((SF_Network*)this)->remove_input_terminal (t);
    }
    catch (SF_Exception)
    {
	// make sure the terminal gets destructed even in case of an error
	delete t;
	throw;
    }
    delete t;
}

void
SF_Network::destroy_output (const char* name) throw (SF_Exception)
    // Destroy a named output terminal and the associated and similarly named
    // output bubble.  This may cause the numbering of the remaining outputs to
    // change.
{
    SF_Bubble_Terminal* t = 0;
    t = (SF_Bubble_Terminal*)&get_output (name);
    try
    {
	remove_output_terminal (t);
    }
    catch (SF_Exception)
    {
	// make sure the terminal gets destructed even in case of an error
	delete t;
	throw;
    }
    delete t;
}

void
SF_Network::destroy_output (int index) const throw (SF_Indexing_Exception)
    // Destroy an output terminal and the associated output bubble by index.
    // This may cause the numbering of the remaining outputs to change.
{
    SF_Bubble_Terminal* t = 0;
    t = (SF_Bubble_Terminal*)&get_output (index);
    try
    {
//	remove_output_terminal (t);
	// Uglily cast away the constness to satisfy GCC 2.95 (FIXME!)
	((SF_Network*)this)->remove_output_terminal (t);
    }
    catch (SF_Exception)
    {
	// make sure the terminal gets destructed even in case of an error
	delete t;
	throw;
    }
    delete t;
}

SF_Output_Terminal&
SF_Network::get_input_bubble (int index) const throw (SF_Indexing_Exception)
    // Function to access the bubbles (related to network's terminals) from
    // inside the network.
{
    // MUST cast via SF_Bubble_Terminal!
    return (SF_Bubble_Terminal&)get_input (index);
}

SF_Output_Terminal&
SF_Network::get_input_bubble (const char* name) const throw (SF_Exception)
    // Function to access the bubbles (related to network's terminals) from
    // inside the network.
{
    // MUST cast via SF_Bubble_Terminal!
    return (SF_Bubble_Terminal&)get_input (name);
}

SF_Input_Terminal&
SF_Network::get_output_bubble (int index) const throw (SF_Indexing_Exception)
    // Function to access the bubbles (related to network's terminals) from
    // inside the network.
{
    // MUST cast via SF_Bubble_Terminal!
    return (SF_Bubble_Terminal&)get_output (index);
}

SF_Input_Terminal&
SF_Network::get_output_bubble (const char* name) const throw (SF_Exception)
    // Function to access the bubbles (related to network's terminals) from
    // inside the network.
{
    // MUST cast via SF_Bubble_Terminal!
    return (SF_Bubble_Terminal&)get_output (name);
}

bool
SF_Network::is_producer () const throw ()
    // Inspect whether there are producer blocks in the network and return true
    // if there are.  Ispects only the blocks which are wired into the
    // functional network and not just floating around.
{
    int i;
    // make sure ranks are up to date
//    partition_network ();
    // Uglily cast away the constness to satisfy GCC 2.95 (FIXME!)
    ((SF_Network*)this)->partition_network ();
    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->rank != 0
	    && get_block_node (i)->block->is_producer ())
	{
	    return true;
	}
    }
    return false;
}

bool
SF_Network::is_consumer () const throw ()
    // Inspect whether there are consumer blocks in the network and return true
    // if there are.  Ispects only the blocks which are wired into the
    // functional network and not just floating around.
{
    int i;
    // make sure ranks are up to date
//    partition_network ();
    // Uglily cast away the constness to satisfy GCC 2.95 (FIXME!)
    ((SF_Network*)this)->partition_network ();
    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->rank != 0
	    && get_block_node (i)->block->is_consumer ())
	{
	    return true;
	}
    }
    return false;
}

SF_Network::SF_Network_Block_List_Content*
SF_Network::search_block_list (const SF_Block* block) const throw ()
    // Find a certain block from this network (from block_list).  Returns 0 if
    // not found.
{
    int i;
    if (block == 0)
    {
	// 'short circuit'
	return 0;
    }
    for (i = 0; i < get_num_blocks (); i++)
    {
	if (get_block_node (i)->block == block)
	{
	    return get_block_node (i);
	}
    }
    // not found
    return 0;
}

void
SF_Network::unwire_block (SF_Block* block) throw (SF_Assertion_Exception)
    // Remove all connections going to and coming from the block.

    // Doesn't throw exceptions.
{
    int i, j;
    __SF_ASSERT (block != 0);

    // Step 1: disconnect all wires to all input terminals
    for (i = 0; i < block->get_num_inputs (); i++)
    {
	SF_Input_Terminal* input;
	input = &(block->get_input (i));
	for (j = 0; j < input->get_num_wires (); j++)
	{
	    input->get_source_terminal (j)->disconnect (*input);
	}
    }
    // Step 2: disconnect all wires from all output terminals
    for (i = 0; i < block->get_num_outputs (); i++)
    {
	SF_Output_Terminal* output;
	output = &(block->get_output (i));
	for (j = 0; j < output->get_num_wires (); j++)
	{
	    output->disconnect (*(output->get_destination_terminal (j)));
	}
    }
}

void
SF_Network::partition_network ()
    throw (SF_Exception, SF_Network_Sanity_Exception)
    // Order the block list so that the following holds: for any given block B,
    // there are no upstream blocks in the list after B.  This implies that all
    // the source blocks come first in the block list.  When the list is ordered
    // in this way, it is said to be partitioned.  Adding and removing
    // connections may cause the network to become unpartitioned.
{
    int i, j;

    // initialize the ranks of all blocks to zero ("illegal rank")
    for (i = 0; i < get_num_blocks (); i++)
    {
	get_block_node (i)->rank = 0;
    }

    // Traverse all the input bubbles of this network and call
    // assign_ranks_downstream () for the blocks they are wired to.  The ranks
    // of the blocks are initialized to `2', i.e. there is supposed to be a
    // "pseudo source block" upstream from the blocks.  assign_ranks_downstream
    // will essentially initialize ranks traversing downstream from the input
    // bubbles.
    for (i = 0; i < get_num_inputs (); i++)
    {
	SF_Output_Terminal* wire_beg = 0;
	SF_Input_Terminal* wire_end = 0;
	SF_Block* wire_end_block = 0;

        wire_beg = &get_input_bubble (i);
	for (j = 0; j < wire_beg->get_num_wires (); j++)
	{
	    wire_end = wire_beg->get_destination_terminal (j);
	    wire_end_block = wire_end->get_host_block ();
	    if (wire_end_block != 0)
	    {
		// clear all the `is_visited' flags
		int k;
		for (k = 0; k < get_num_blocks (); k++)
		{
		    get_block_node (k)->is_visited = false;
		}
		// and run DFS
		assign_ranks_downstream (search_block_list (wire_end_block),
					 2);
	    }
	}
    }

    // Find the potential source blocks within the network and initialize ranks
    // traversing downstream from them using depth-first search.  After this the
    // real source blocks are known, as they are the blocks whose rank is 1.
    for (i = 0; i < get_num_blocks (); i++)
    {
        // Inspect whether the block in question is potentially a source block.
        // A potential source block is candidate for a source block.  A real
        // source block is a block which requires no other blocks to be run
        // prior to it in the course of one round of execution.  Note that there
        // can be incoming wires to a (real) source block, e.g. from itself.
        // Another example is the traditional R-S flip-flop topology.
	if (get_block_node (i)->rank == 0
	    && get_block_node (i)->block->is_producer ()
	    && get_block_node (i)->block->is_functional ())
	{
	    // Found a potential source block:
	    // clear all the `is_visited' flags
	    for (j = 0; j < get_num_blocks (); j++)
	    {
		get_block_node (j)->is_visited = false;
	    }
	    // and run DFS
	    assign_ranks_downstream (get_block_node (i), 1);
	}
    }

    // partition the network (order blocks according to their rank)
    block_list->sort ((int (*) (const void*, const void*))compare_ranks);
}

void
SF_Network::assign_ranks_downstream (SF_Network_Block_List_Content* start, int rank)
    throw (SF_Assertion_Exception, SF_Network_Sanity_Exception)
    // Starting from the specified block, traverse all possible downstream paths
    // and assign an increasing rank to every block visited during the
    // traversal.  The rank of the specified starting block is as given in the
    // function call.  The rank of a so called source block is defined to be 1
    // and the rank of a downstream block could be defined as "the distance to
    // the farthest source block plus one".  Thus, the rank argument must be >=
    // 1.  This routine also searches for consumer blocks in the downstream and,
    // if it doesn't find any, gives a warning.  Algorithm: depth-first search
    // (DFS).  Subroutine of partition_network ().
{
    static int recursion_depth = 0;
    static bool consumer_found = false;
    SF_Network_Block_List_Content* v = 0;
    SF_Block* start_block = 0;
    SF_Output_Terminal* output = 0;
    SF_Input_Terminal* destination = 0;
    int num_outputs, output_index;
    int num_destinations, destination_index;

    __SF_ASSERT (start != 0);

    if (recursion_depth == 0)
    {
	consumer_found = false;
    }
    recursion_depth++;
    start->is_visited = true;

    // Initialize rank
    if (start->rank == 0)
    {
	start->rank = rank;
    }
    start_block = start->block;

    num_outputs = start_block->get_num_outputs ();

    // Traverse all output terminals in current block
    for (output_index = 0; output_index < num_outputs; output_index++)
    {
	output = &(start_block->get_output (output_index));
	num_destinations = output->get_num_wires ();

	// Traverse all wires in current output terminal
	for (destination_index = 0; destination_index < num_destinations; destination_index++)
	{
	    destination = output->get_destination_terminal (destination_index);
	    if (destination->get_host_block () != 0)
	    {
		// The destination terminal isn't a bubble.

		// Ensure that destination block is part of this network.
		v = search_block_list (destination->get_host_block ());
		if (v == 0)
		{
		    // FIXME: repair the static variables!
		    throw SF_Network_Sanity_Exception (destination->get_host_block (),
						       "Illegal topology: Wire leads out of network");
		}

		if (v->is_visited == false)
		{
		    if (start->rank >= v->rank)
		    {
			v->rank = start->rank + 1;
		    }
		    // It would be more efficient to put this into the previous if
		    // block, but in that case some consumers wouldn't be detected
		    // and a false warning message would be printed.
		    assign_ranks_downstream (v, v->rank);
		}
		consumer_found = consumer_found || v->block->is_consumer ();
	    }
	    else
	    {
		// The destination terminal is a bubble.
		consumer_found = true;
	    }
	}
    }
    start->is_visited = false;
    recursion_depth--;
    if (recursion_depth == 0)
    {
	if (consumer_found == false)
	{
	    SF_Warning (start_block, "No sinks downstream from this source");
	}
    }
}

int
SF_Network::compare_ranks (const void* a, const void* b)
    throw (SF_Assertion_Exception)
    // A comparison function for sorting the block_list according to the ranks
    // of the blocks; used by SF_List::sort () via SF_Network::assign_ranks_downstream ().
{
    __SF_ASSERT (a != 0 && b != 0);
    return (((const SF_Network_Block_List_Content*)a)->rank
	    - ((const SF_Network_Block_List_Content*)b)->rank);
}

void
SF_Network::clear_frames (SF_Block* block) throw (SF_Assertion_Exception)
    // Clear the frames in all output terminals in the block; preparing for
    // simulation.  The contents of the frames are in effect the contents of the
    // `implicit delays' in feedback loops in the network.

    // Doesn't throw exceptions.
{
    int i;
    SF_Frame* f = 0;
    __SF_ASSERT (block != 0);

    for (i = 0; i < block->get_num_outputs (); i++)
    {
	f = block->get_output (i).get_frame ();
	if (f != 0)
	{
	    f->clear ();
	}
    }
}

void
SF_Network::check_functionality () const throw (SF_Exception)
    // Make sure every block used in the simulation is functional.  These blocks
    // consist of every block with nonzero rank and and every source of such an
    // block.  
{
    int i, j, k;
    for (i = 0; i < get_num_blocks (); i++)
    {
	SF_Network_Block_List_Content* node;
	node = get_block_node (i);
	if (node->rank != 0 && !node->block->is_functional ())
	{
	    throw SF_Exception (node->block, "Block is not functional");
	}
	if (node->rank == 0)
	{
	    // check that a zero-rank block is not connected to a nonzero-rank
	    // block
	    for (j = 0; j < node->block->get_num_outputs (); j++)
	    {
		for (k = 0; k < node->block->get_output (j).get_num_wires (); k++)
		{
		    SF_Block* dest = 0;
		    dest = node->block->get_output (j)
			.get_destination_terminal (k)->get_host_block ();
		    if (dest != 0)
		    {
			SF_Network_Block_List_Content* dest_node = 0;
			dest_node = search_block_list (dest);
			__SF_ASSERT (dest_node != 0);
			if (dest_node->rank != 0)
			{
			    throw SF_Exception (node->block,
						"Block is not functional");
			}
		    }
		}
	    }
	}
    }
}

void
SF_Network::check_bubble_wirings () const throw (SF_Exception)
    // Check bubble wirings. Input bubbles must be wired and output blocks
    // should be wired to blocks, whose ranks are nonzero.  Throws exceptions if
    // input bubbles aren't wired appropriately and displays warnings if output
    // bubbles aren't.
{
    int i, j;
    // Check input bubbles
    for (i = 0; i < get_num_inputs (); i++)
    {
	bool found_proper_wires = false;
	SF_Output_Terminal* wire_beg = 0;
	wire_beg = &get_input_bubble (i);
	for (j = 0; j < wire_beg->get_num_wires (); j++)
	{
	    SF_Input_Terminal* wire_end = 0;
	    SF_Block* wire_end_block = 0;
	    wire_end = wire_beg->get_destination_terminal (j);
	    wire_end_block = wire_end->get_host_block ();
	    if (wire_end_block != 0)
	    {
		// wire leads to a block
		SF_Network_Block_List_Content* wire_end_node = 0;
		wire_end_node = search_block_list (wire_end_block);
		__SF_ASSERT (wire_end_node != 0);
		if (wire_end_node->rank != 0)
		{
		    // found a wire to a proper block
		    found_proper_wires = true;
		    break;
		}
	    }
	    else
	    {
		// wire leads to an output bubble
		found_proper_wires = true;
		break;
	    }
	}
	if (!found_proper_wires)
	{
	    throw SF_Terminal_Exception (wire_beg,
					 "Input bubble must be wired to a block in use or an output bubble");
	}
    }
    // Check output bubbles
    for (i = 0; i < get_num_outputs (); i++)
    {
	bool found_proper_wires = false;
	SF_Input_Terminal* wire_end = 0;
	wire_end = &get_output_bubble (i);
	for (j = 0; j < wire_end->get_num_wires (); j++)
	{
	    SF_Output_Terminal* wire_beg = 0;
	    SF_Block* wire_beg_block = 0;
	    wire_beg = wire_end->get_source_terminal (j);
	    wire_beg_block = wire_beg->get_host_block ();
	    if (wire_beg_block != 0)
	    {
		// wire comes from a block
		SF_Network_Block_List_Content* wire_beg_node = 0;
		wire_beg_node = search_block_list (wire_beg_block);
		__SF_ASSERT (wire_beg_node != 0);
		if (wire_beg_node->rank != 0)
		{
		    // found a wire from a proper block
		    found_proper_wires = true;
		    break;
		}
	    }
	    else
	    {
		// wire comes from an input bubble
		found_proper_wires = true;
		break;
	    }
	}
	if (!found_proper_wires)
	{
	    SF_Warning (0, "An output bubble is not wired to a block in use or an input bubble");
	}
    }
}

/* EOF */
