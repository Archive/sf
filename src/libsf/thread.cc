/* SF_Thread
   Copyright (C) 1999 Jarno Sepp�nen and Andrew Clausen
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/global.hh>
#include <sf/list.hh>
#include <sf/network.hh>
#include <sf/thread.hh>
#include <sf/typedefs.h>

SF_Thread::SF_Thread (SF_Network* network)
    running (false),
    stop (false)
    // Construct a real-time thread and make it wait for start_simulation().
    // This would need root privileges to raise thread priority, lock memory
    // pages etc?  The network parameter specifies the network this thread is
    // created to simulate.
{
    // Create a thread and have it wait for a mutex; the mutex would then be
    // operated from the start_simulation() and stop_simulation() functions,
    // effectively starting and stopping the simulation.
}

SF_Thread::~SF_Thread ()
    // Destruct a real-time thread.
{
}

// The primary functions to start and stop a simulation thread.  The
// simulation is carried out asynchronously, i.e. both of these functions
// return regardless of the simulation.  real_time is used to initiate either
// a real-time or a "full-speed" simulation run.  The duration parameter can
// be used to set a definite length (in milliseconds) for the simulation.  A
// duration value of 0 specifies indefinite length, i.e. the simulation is run
// until there is a call to stop_simulation ().
void
SF_Thread::start_simulation (bool real_time, SF_Time duration)
{
}

void
SF_Thread::stop_simulation ()
{
}

// This function can be called to query if there is a simulation running
// currently.
bool SF_Thread::is_simulation_running ();

// This function returns a value between [0, 1] according to the phase of the
// simulation.  The progress is calculated based on the duration specified in
// the start_simulation () call.
float SF_Thread::get_progress_meter ();

// The following handler will be called always when the simulation stops for
// any reason.  Currently there are three reasons: (1) the simulation duration
// is met, (2) stop_simulation () is called or (3) there is an error in
// simulation and an exception is thrown.  The exceptions thrown in the
// simulation thread will be caught and the stop handler will be called with a
// pointer to the exception.  If the simulation has stopped normally the
// pointer will be 0.  The handler will be called from the simulation thread.
typedef void (*SF_Exception_Handler)(const SF_Exception* exception);
void SF_Thread::set_simulation_stop_handler (SF_Exception_Handler handler);
SF_Exception_Handler SF_Thread::get_simulation_stop_handler ();

// Get the current (system etc.) time in microseconds.
unsigned int SF_Timer::get_time ();

// Wait until the (system etc.) time is equal to or greater than usec_time in
// microseconds.  Suspends the calling thread.
void SF_Timer::wait (unsigned int usec_time);

void
SF_Thread::start_simulation (bool real_time, SF_Time duration)
{
    atomic
	{
	    if (running)
	    {
		throw SF_Exception (0, "Simulation already running");
	    }
	    running = true;
	}
    start_thread (simulation_thread (real_time, duration / frame_duration));
}

void
SF_Thread::stop_simulation ()
{
    atomic
	{
	    if (stop || !running)
	    {
		throw SF_Exception (0, "Simulation not running");
	    }
	    stop = true;
	}
}

// This is the code which is executed in a separate thread.  Parameters are:
// real_time - whether to try to run in real time
// rounds - number of rounds to simulate (each round is frame_duration
// milliseconds)
// handler - the function to call when the simulation stops for any reason.
void
SF_Thread::simulation_thread (bool real_time, int rounds, SF_Exception_Handler handler)
{
    unsigned int interval_time;
    SF_Exception* sim_exception = 0;

    try
    {
	// initialize the network prior to execution
	initialize ();
	interval_time = SF_Timer::get_time ();
	while (atomic {!stop})
	{
	    if (real_time)
	    {
		// compute the interval by adding the allowed processing time to the
		// previous interval time
		interval_time += frame_duration * 1000;
		if (SF_Timer::get_time () > interval_time)
		{
		    throw SF_Exception (0, "Not enough CPU");
		}
		SF_Timer::wait (interval_time);
	    }
	    // execute one round of simulation; this processes frame_duration
	    // milliseconds of data
	    execute ();

	    // if not running indefinitely, decrement the simulation round counter
	    // and bail out if necessary
	    if (rounds != 0)
	    {
		rounds--;
		if (rounds == 0)
		{
		    break;
		}
	    }
	} // while
    } // try
    catch (SF_Exception e)
    {
	sim_exception = new SF_Exception;
	copy e into sim_exception;
    }
    // call the stop handler if one specified
    if (handler != 0)
    {
	handler (sim_exception);
    }
    free sim_exception;
    atomic
	{
	    running = false;
	    stop = false;
	}
}



//////////////// Andrew, this is stuff I removed from network.cc.  Note that
//////////////// this is seriously obsolete stuff:



// processes and signals
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>


void
SF_Network::start_simulation ()
    // Start an asynchronous simulation run of the whole network: the duration
    // of this run is set with set_simulation_duration ().  This call returns
    // after starting the simulation process.  NOTE that there is currently no
    // way of getting data (nor error information) back from the asynchronous
    // simulation process.

    // Throws exceptions: SF_Exception.
{
    pid_t p;
    if (simulation_running)
    {
	throw SF_Exception (this, "Network already running");
    }
    p = fork ();
    if (p == (pid_t)(-1))
    {
	throw SF_Exception (this, "Unable to fork");
    }
    if (p == 0)
    {
	// child process
	simulation_child ();
    }
    else
    {
	// parent process
	simulation_running = true;
	child_pid = p;
    }
}

void
SF_Network::stop_simulation ()
    // Stop the asynchronous simulation process.

    // Throws exceptions: SF_Exception.
{
    parent_set_signal (SIGINT);
}


void
SF_Network::simulation_child ()
    // The asynchronous child process used to run the simulation for a preset
    // duration.  This process is started in the start_simulation () function.

    // Doesn't throw exceptions.
{
    long epoch_counter = 0;
    SF_Time epoch_duration;

    try
    {
	child_init_signal (SIGINT);
	epoch_duration = SF_Frame::get_duration ();
	initialize ();
	while ((epoch_counter * epoch_duration < simulation_duration)
	       && (!child_get_signal (SIGINT)))
	{
	    execute ();
	    epoch_counter++;
	}
	finish ();
	child_finish_signal (SIGINT);
    }
    catch (...)
    {
	// we will catch all exceptions because otherwise it might result in
	// difficulties

	// FIXME: what to do now?
    }
    // the child must exit
    _exit (0);
}

void
SF_Network::child_init_signal (int signal)
    // Initialize the child's signal facilities for the specified signal
    // (e.g. SIGHUP).

    // Throws exceptions: SF_Exception.
{
    sigset_t signal_mask;
    if (sigemptyset (&signal_mask)
	|| sigaddset (&signal_mask, signal)
	|| sigprocmask (SIG_BLOCK, &signal_mask, 0))
    {
	throw SF_Exception (0, "Simulation termination signal initialization failed");
    }
}

void
SF_Network::child_finish_signal (int signal)
    // Clean up the child's signal facilities for the specified signal
    // (e.g. SIGHUP).

    // Doesn't throw exceptions.
{
    // NOP
}

bool
SF_Network::child_get_signal (int signal)
    // Poll for the occurrance of the specified signal on the child's side.

    // Throws exceptions: SF_Exception.
{
    sigset_t signal_mask;
    if (sigpending (&signal_mask))
    {
	throw SF_Exception (0, "Unable to poll simulation termination signal");
    }
    return (bool) (sigismember (&signal_mask, signal));
}

void
SF_Network::parent_set_signal (int signal)
    // Initiate the occurrance of the specified signal from the parent to the
    // child.

    // Throws exceptions: SF_Exception.
{
    if (kill (child_pid, signal))
    {
	throw SF_Exception (this, "Unable to send simulation termination signal");
    }
}

/* EOF */
