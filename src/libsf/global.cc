/* Global functions
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <new.h>

#include <sf/exception.hh>
#include <sf/global.hh>
#include <sf/block.hh>

static void	default_warning_handler (const SF_Warning& w);
static void	default_new_handler ();

void
SF_initialize () throw ()
    // Initialize the Sonic Flow library before operation.  This function needs
    // to be called before any other function is called in the Sonic Flow
    // library (libsf).  This function will set the C++ new_handler, i.e. the
    // function which is called on out-of-memory situations.
{
    SF_Warning::set_suppression (false);
    SF_Warning::set_handler (default_warning_handler);
    set_new_handler (&default_new_handler);
}

void
SF_finish () throw ()
    // Finish the Sonic Flow library after operation.  This function needs to be
    // called as the last function of the library, prior to exiting the host
    // program.  This function will clear the C++ new_handler, i.e. the function
    // which is called on out-of-memory situations.
{
    set_new_handler (0);
}

static void
default_warning_handler (const SF_Warning& w)
    // This is a default warning handler, which outputs the warning message to
    // the standard output stream.

    // Doesn't throw exceptions.
{
    if (w.message != 0)
    {
	cerr << "Warning";
    }
    else
    {
	cerr << "An unknown warning occurred";
    }
    if (w.origin != 0)
    {
	cerr << " in block \"" << w.origin->get_name () << "\"";
    }
    if (w.message != 0)
    {
	cerr << ": " << w.message;
    }
    cerr << "." << endl;
}

static void
default_new_handler ()
    // The function which gets called every time we run out of memory.  An
    // exception is thrown in such situations.

    // Throws exceptions: SF_Exception.
{
    throw SF_Memory_Exception ("Out of memory");
}

/* EOF */
