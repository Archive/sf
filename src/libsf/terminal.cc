/* SF_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <stdlib.h>

#include <sf/block.hh>
#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/queue.hh>
#include <sf/terminal.hh> 
#include <sf/typedefs.h>

SF_Terminal::SF_Terminal (const char* name,
			  const SF_Block* host_block,
			  SF_Frequency sample_rate,
			  double relative_sample_rate)
    throw (SF_Memory_Exception, SF_Terminal_Exception)
    : name (name),
      host_block (host_block),
      sample_rate (sample_rate),
      relative_sample_rate (relative_sample_rate),
      sample_rate_imbalance (false),
      wires (0),
      propagation_visited (false)
    // Construct a terminal.  Note that every terminal must have a name.
{
    // Sanity checks
    // Allow host_block to be 0 since the bubbles in a network don't have a host
    // block
    if (name == 0)
    {
	throw SF_Terminal_Exception (this, "Every terminal must have a name");
    }
    if (sample_rate <= 0 || relative_sample_rate <= 0)
    {
	throw SF_Terminal_Exception (this, "Sample rates must be positive");
    }
    wires = new SF_List;
}

SF_Terminal::~SF_Terminal () throw ()
{
    delete wires;
    wires = 0;
}

void
SF_Terminal::set_relative_sample_rate (double relative_sample_rate) throw (SF_Exception)
    // Set the relative sample rate figure of this terminal with respect to the
    // other terminals on the block.  This will both initialize the relative
    // sample rate and also propagate sample rates from this terminal to all
    // terminals and blocks connected to this, recursively.  The sample rates of
    // the terminals are calculated based on the actual sample rate of this
    // terminal and the relative sample rates of the terminals.  After this
    // function is finished, some blocks may have sample rate imbalances between
    // their terminals.  The imbalance flags in all the visited terminals are
    // updated.
{
    // set the relative sample rate of this terminal
    only_set_relative_sample_rate (relative_sample_rate);

    // If there are more than one terminals in host_block, recalculate their
    // sample rates and propagate the changes along wires.  Assume input
    // terminals have correct (i.e. reference) sample rate(s).
    if (host_block != 0 && host_block->get_num_terminals () != 1)
    {
	int i;
	SF_Terminal* ref = 0;
	for (i = 0; i < host_block->get_num_inputs (); i++)
	{
	    if (&(host_block->get_input (i)) == this)
	    {
		// (a) If there are input terminals and this terminal is an
		//     input, pick it as the reference, or
		ref = this;
		break;
	    }
	}
	if (ref == 0)
	{
	    if (host_block->get_num_inputs () != 0)
	    {
		// (b) If there are input terminals and this terminal is not in
		//     them, pick the first input as the reference, or
		ref = &(host_block->get_input (0));
	    }
	    else
	    {
		// (c) If there are no input terminals, set the rates of the
		//     other output terminals according to this terminal.
		ref = this;
	    }
	}
	set_sample_rate (relative_sample_rate / ref->relative_sample_rate
			 * ref->sample_rate);
    }
}

void
SF_Terminal::set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception)
    // Set the sample rate of this terminal and propagate the rate to all input
    // and output terminals and blocks connected to this block, recursively,
    // taking relative sample rates into account.  After this function is
    // finished, some blocks may have sample rate imbalances between their
    // terminals.  The imbalance flags in all the visited terminals are updated.
{
    int i;
    // the queue used in the block-level BFS algorithm
    SF_Queue traversal_q;
    // a queue used to clear all the visited flags after processing
    SF_Queue cleanup_q;

    if (sample_rate <= 0)
    {
	throw SF_Terminal_Exception (this, "Sample rates must be positive");
    }

    // Prepare for exceptions during processing so that we can clear all the
    // touched visited flags before falling out from this routine.  Initially
    // all the visited flags are in cleared state.
    try
    {
	// The algorithm is a hybrid of breadth-first-search (BFS) and
	// depth-first-search (DFS), i.e. it's a death-first search.  The BFS
	// operates on block level and the DFS on wires connected to each other
	// directly via terminals.  This function works mostly on the BFS level and
	// the subroutine, propagate_wire_sample_rate, on the DFS level.

	// first of all, propagate the sample rate to all wires connected to this
	// terminal recursively, and enqueue this terminal into traversal_q.
	propagate_wire_sample_rate (sample_rate, traversal_q, cleanup_q);

	// loop until there are no more blocks to process
	while (!traversal_q.is_empty ())
	{
	    // reference terminal used in calculating the sample rate of a terminal
	    // across a block using relative sample rates
	    SF_Terminal* ref_term = 0;
	    // the block we are processing
	    const SF_Block* ref_block = 0;

	    // get the reference terminal and find out its host block
	    ref_term = (SF_Terminal*) traversal_q.dequeue ();
	    ref_block = ref_term->get_host_block ();

	    if (ref_block != 0)
	    {
		// process all the terminals in the reference block 
		for (i = 0; i < ref_block->get_num_terminals (); i++)
		{
		    SF_Terminal* wire_beg = 0;
		    SF_Frequency wire_sr;
		    wire_beg = &(ref_block->get_terminal (i));
		    wire_sr = (wire_beg->get_relative_sample_rate ()
			       / ref_term->get_relative_sample_rate ()
			       * ref_term->get_sample_rate ());

		    // Propagate the rates further from every terminal in them IFF
		    // (a) the terminals are not visited yet AND
		    // (b) the sample rate is not already correct.
		    if (!wire_beg->propagation_visited
			&& wire_sr != wire_beg->get_sample_rate ())
		    {
			wire_beg->propagate_wire_sample_rate (wire_sr,
							      traversal_q,
							      cleanup_q);
		    }
		}
	    }
	}
    } catch (SF_Exception e)
    {
	// MAKE SURE we clear all the visited flags in all terminals we touched,
	// even in the case of an exception
	while (!cleanup_q.is_empty ())
	{
	    SF_Terminal* t = 0;
	    t = (SF_Terminal*) cleanup_q.dequeue ();
	    t->propagation_visited = false;
	}
	throw e;
    }
    // clear all the visited flags in all terminals we touched and update sample
    // rate imbalance flags in every block we visited

    while (!cleanup_q.is_empty ())
    {
	SF_Terminal* t = 0;
	t = (SF_Terminal*) cleanup_q.dequeue ();
	t->propagation_visited = false;
	if (t->host_block != 0)
	{
	    update_imbalance_flags (*(t->host_block));
	}
	// Here we could have a callback
    }
}

SF_Terminal*
SF_Terminal::get_wired_terminal (int index) throw (SF_Indexing_Exception)
    // Fetch the index'th source/destination terminal from the list of
    // connections.  If the index is out of bounds (must be 0 <= index <
    // degree), throws an exception.
{
    return (SF_Terminal*) (wires->get_node (index));
}

void
SF_Terminal::propagate_wire_sample_rate (SF_Frequency sample_rate,
					 SF_Queue& traversal_q,
					 SF_Queue& cleanup_q)
    throw ()
    // Set the sample rate of this terminal and propagate the rate to all the
    // terminals connected to this terminal, recursively.  This is a subroutine
    // of set_sample_rate ().  All the terminals whose rates are set are
    // enqueued into traversal_q for subsequent processing in set_sample_rate.
{
    int i;

    // We use the depth-first search (DFS) algorithm to prepare for wire loops.
    // Therefore we need the "propagation_visited" flags.  The flags are
    // initially all cleared.

    if (propagation_visited == false)
    {
	// Set the visited flag to indicate that this terminal has been
	// processed.  Add the terminal to the cleanup queue in order to clear
	// all the visited flags after set_sample_rate().
	propagation_visited = true;
	cleanup_q.enqueue (this);

	// Update the sample rate and mark the block (by enqueueing this
	// terminal) for further propagation if the sample rate is incorrect.
	if (get_sample_rate () != sample_rate)
	{
	    // set the actual sample rate value
	    only_set_sample_rate (sample_rate);
	    traversal_q.enqueue (this);
	}
	// Finally recursively propagate the sample rates along all the wires
	// connected to this terminal.
	for (i = 0; i < get_num_wires (); i++)
	{
	    get_wired_terminal (i)->propagate_wire_sample_rate (sample_rate,
								traversal_q,
								cleanup_q);
	}
    }
}

void
SF_Terminal::update_imbalance_flags (const SF_Block& b) throw ()
    // Update the imbalance flags on all terminals in the specified block.
{
    int i;
    SF_Terminal* t = 0;
    SF_Frequency ref_f;

    // ALL blocks this function is run to have at least one terminal.
    t = &(b.get_terminal (0));
    ref_f = t->get_sample_rate () / t->get_relative_sample_rate ();
    t->set_sample_rate_imbalance (false);
    for (i = 1; i < b.get_num_terminals (); i++)
    {
	SF_Frequency g;
	t = &(b.get_terminal (i));
	g = t->get_sample_rate () / t->get_relative_sample_rate ();
	t->set_sample_rate_imbalance (g != ref_f);
    }
}

void
SF_Terminal::only_set_sample_rate (SF_Frequency sample_rate)
    throw (SF_Exception)
    // Set the sample rate of this terminal without any propagation and without
    // changing any imbalance flags.
{
    if (sample_rate <= 0)
    {
	throw SF_Terminal_Exception (this, "Sample rates must be positive");
    }
    this->sample_rate = sample_rate;
}

void
SF_Terminal::only_set_relative_sample_rate (double relative_sample_rate)
    throw (SF_Exception)
    // Set the relative sample rate of this terminal without any propagation and
    // without changing any imbalance flags.
{
    if (relative_sample_rate <= 0)
    {
	throw SF_Terminal_Exception (this, "Sample rates must be positive");
    }
    this->relative_sample_rate = relative_sample_rate;
}

/* EOF */
