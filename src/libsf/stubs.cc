/* This file is totally obsolete; Joel Dinolt <joel@fived.com> should have got
   an updated version. */

#if 0

/* Wrapper for the C language API
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <stdlib.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/input_terminal.hh>
#include <sf/network.hh>
#include <sf/output_terminal.hh>
#include <sf/terminal.hh>

typedef void (*SF_Exception_Handler)(const SF_Block* origin,
				     const char* description);

extern "C"
{

static void (*exception_handler)(const SF_Block*, const char*);

static void
SF_default_exception_handler (const SF_Block* origin, const char* description)
{
    cerr << "*** Sonic Flow exception";
    if (origin != 0)
    {
	cerr << " in block \"" << origin->get_instance_name () << "\" (" <<
	    origin->get_class_name () << ")";
    }
    cerr << endl << description << endl;
    exit (EXIT_FAILURE);
}

/*	Generic */

void
SF_set_exception_handler (SF_Exception_Handler handler)
{
    exception_handler = handler;
}

SF_Exception_Handler
SF_get_exception_handler (void)
{
    return exception_handler;
}

void
SF_initialize (void)
{
    SF_set_exception_handler (SF_default_exception_handler);
}

void
SF_finish (void)
{
}

/*	SF_Block: */

const char*
SF_Block__get_class_name (SF_Block* block)
{
    try
    {
	return block->get_class_name ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

const char*
SF_Block__get_instance_name (SF_Block* block)
{
    try
    {
	return block->get_instance_name ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Block__set_instance_name (SF_Block* block, const char* instance_name)
{
    try
    {
	block->set_instance_name (instance_name);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Network*
SF_Block__get_host_network (SF_Block* block)
{
    try
    {
	return block->get_host_network ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Block__set_host_network (SF_Block* block, SF_Network* host)
{
    try
    {
	block->set_host_network (host);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

bool
SF_Block__is_functional (SF_Block* block)
{
    try
    {
	return block->is_functional ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

bool
SF_Block__is_producer (SF_Block* block)
{
    try
    {
	return block->is_producer ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

bool
SF_Block__is_consumer (SF_Block* block)
{
    try
    {
	return block->is_consumer ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

unsigned int
SF_Block__get_num_input_terminals (SF_Block* block)
{
    try
    {
	return block->get_num_input_terminals ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

unsigned int
SF_Block__get_num_output_terminals (SF_Block* block)
{
    try
    {
	return block->get_num_output_terminals ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Input_Terminal* const
SF_Block__get_inputs (SF_Block* block)
{
    try
    {
	return block->inputs;
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Output_Terminal* const
SF_Block__get_outputs (SF_Block* block)
{
    try
    {
	return block->outputs;
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

/*	SF_Output_Terminal */

void
SF_Output_Terminal__connect (SF_Output_Terminal *output_terminal,
			     SF_Input_Terminal *destination)
{
    try
    {
	output_terminal->connect (*destination);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Output_Terminal__disconnect (SF_Output_Terminal *output_terminal,
				SF_Input_Terminal *destination)
{
    try
    {
	output_terminal->disconnect (*destination);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

/*	SF_Network: */

SF_Network*
SF_Network__new (void)
{
    try
    {
	return new SF_Network ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Network__delete (SF_Network* network)
{
    try
    {
	delete network;
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}


void
SF_Network__add_block (SF_Network* network, SF_Block* block)
{
    try
    {
	network->add_block (*block);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Network__remove_block (SF_Network* network, SF_Block* block)
{
    try
    {
	network->remove_block (*block);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

                    
void
SF_Network__check (SF_Network* network)
{
    try
    {
	network->check ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Time
SF_Network__get_simulation_duration (SF_Network* network)
{
    try
    {
	return network->get_simulation_duration ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Network__set_simulation_duration (SF_Network* network, SF_Time dur_ms)
{
    try
    {
	network->set_simulation_duration (dur_ms);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}


void
SF_Network__start_simulation (SF_Network* network)
{
    try
    {
	network->start_simulation ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Network__stop_simulation (SF_Network* network)
{
    try
    {
	network->stop_simulation ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

bool
SF_Network__is_running (SF_Network* network)
{
    try
    {
	return network->is_running ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

bool
SF_Network__is_error (SF_Network* network)
{
    try
    {
	return network->is_error ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

/*  SF_Terminal: */

const char*
SF_Terminal__get_name (SF_Terminal* terminal)
{
    try
    {
	return terminal->get_name ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Block*
SF_Terminal__get_host_block (SF_Terminal* terminal)
{
    try
    {
	return terminal->get_host_block ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

SF_Frequency
SF_Terminal__get_sample_rate (SF_Terminal* terminal)
{
    try
    {
	return terminal->get_sample_rate ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Terminal__set_sample_rate (SF_Terminal* terminal, SF_Frequency sample_rate)
{
    try
    {
	terminal->set_sample_rate (sample_rate);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

double
SF_Terminal__get_relative_sample_rate (SF_Terminal* terminal)
{
    try
    {
	return terminal->get_relative_sample_rate ();
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}

void
SF_Terminal__set_relative_sample_rate (SF_Terminal* terminal,
				       double relative_sample_rate)
{
    try
    {
	terminal->set_relative_sample_rate (relative_sample_rate);
    }
    catch (SF_Exception e)
    {
	exception_handler (e.origin, e.description);
    }
}



} /* Matches extern "C"...{ */

/* EOF */

#endif
