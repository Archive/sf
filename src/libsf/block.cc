/* SF_Block
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <iostream.h>
#include <math.h>
#include <string.h>

#include <sf/block.hh>
#include <sf/exception.hh>
#include <sf/defaults.hh>
#include <sf/global.hh>
#include <sf/input_terminal.hh>
#include <sf/list.hh>
#include <sf/network.hh>
#include <sf/output_terminal.hh>

SF_Block::SF_Block (bool producer,
		    bool consumer,
		    const char* class_name,
		    const char* name)
    throw (SF_Memory_Exception)
    : class_name (class_name),
      name (name),
      host_network (0),
      producer (producer),
      consumer (consumer)
    // Construct a block
{
    input_terminals = new SF_List;
    output_terminals = new SF_List;
}

SF_Block::~SF_Block () throw ()
{
    delete input_terminals;
    input_terminals = 0;
    delete output_terminals;
    output_terminals = 0;
}

void
SF_Block::print (ostream& stream) const throw ()
    // Make a printout of the block to the specified output stream.  The
    // printout contains the class and instance names of the block, information
    // on its functionality, number of input and output terminals and
    // connectivity.
{
    int i;

    // print general information of the block
    stream << " "
	   << (class_name != 0 ? class_name : "Unknown type")
	   << ": \""
	   << (name != 0 ? name : "Unnamed block")
	   << "\" ("
	   << (!is_functional () ? "non-functional; " : "")
	   << get_num_inputs () << " input, "
	   << get_num_outputs () << " output terminal(s))" << endl;

    // print information of each input terminal
    for (i = 0; i < get_num_inputs (); i++)
    {
	SF_Input_Terminal* it = 0;
	it = &get_input (i);
	stream << "  Input \"" << it->get_name ()
	       << "\" (" << (it->is_sample_rate_imbalance () ? "imb " : "")
	       << it->get_sample_rate () << " Hz): ";

	if (it->get_num_wires () == 0)
	{
	    if (it->has_default ())
	    {
		stream << "Default value (no connections)" << endl;
	    }
	    else
	    {
		stream << "No connections" << endl;
	    }
	}
	else
	{
	    stream << it->get_num_wires () << " connection(s)" << endl;
	}
    }

    // print information of each output terminal
    for (i = 0; i < get_num_outputs (); i++)
    {
	SF_Output_Terminal* ot = 0;
	ot = &get_output (i);
	stream << "  Output \"" << ot->get_name ()
	       << "\" (" << (ot->is_sample_rate_imbalance () ? "imb " : "")
	       << ot->get_sample_rate () << " Hz): ";

	if (ot->get_num_wires () == 0)
	{
	    stream << "No connections" << endl;
	}
	else
	{
	    // print information on all the connections one at a time
	    SF_Input_Terminal* dest;
	    int j;

	    // print the first connection a bit different from the rest
	    dest = ot->get_destination_terminal (0);
	    stream << "\"" << dest->get_name ();
	    if (dest->get_host_block () != 0)
	    {
		stream << "@" << dest->get_host_block ()->get_name ();
	    }
	    stream << "\"";
	    for (j = 1; j < ot->get_num_wires (); j++)
	    {
		dest = ot->get_destination_terminal (j);
		stream << ", \"" << dest->get_name ();
		if (dest->get_host_block () != 0)
		{
		    stream << "@" << dest->get_host_block ()->get_name ();
		}
		stream << "\"";
	    }
	    stream << endl;
	}
    }
}

bool
SF_Block::is_functional () const throw ()
    // With this function you can query whether the block is functional or not.
    // When a block is functional it means that it's ready and able to be
    // executed (simulated).  The preconditions are that the block has input
    // wires to all needed input terminals and that the sample rates of the
    // terminals of the block match.
{
    int i;
    // check for necessary input wires
    for (i = 0; i < get_num_inputs (); i++)
    {
	if (get_input (i).get_num_wires () == 0
	    && get_input (i).has_default () == false)
	{
	    return false;
	}
    }
    // check for sample rate imbalances in any terminal
    for (i = 0; i < get_num_terminals (); i++)
    {
	if (get_terminal (i).is_sample_rate_imbalance ())
	{
	    return false;
	}
    }
    return true;
}

SF_Block&
SF_Block::operator>> (SF_Input_Terminal& dest_terminal) throw (SF_Exception)
    // The `>>' operators are provided for comfortable access to the
    // SF_Output_Terminal::connect () function.  With this function you can
    // connect the only output of this block to the specified terminal.

    // Throws exceptions: SF_Exception.
{
    if (get_num_outputs () != 1)
    {
	throw SF_Exception (this, "SF_Block::operator>> is for blocks with exactly one output terminal");
    }
    return get_output (0).operator>> (dest_terminal);
}

SF_Block&
SF_Block::operator>> (SF_Input_Terminal* dest_terminal) throw (SF_Exception)
    // The `>>' operators are provided for comfortable access to the
    // SF_Output_Terminal::connect () function.  With this function you can
    // connect the only output of this block to the specified terminal.

    // Throws exceptions: SF_Exception.
{
    __SF_ASSERT (dest_terminal != 0);
    return operator>> (*dest_terminal);
}

SF_Block&
SF_Block::operator>> (SF_Block& dest_block) throw (SF_Exception)
    // The `>>' operators are provided for comfortable access to the
    // SF_Output_Terminal::connect () function.  With this function you can
    // connect the only output of this block to the only input of the specified
    // block.

    // Throws exceptions: SF_Exception.
{
    if (get_num_outputs () != 1)
    {
	throw SF_Exception (this, "SF_Block::operator>> is for blocks with exactly one output terminal");
    }
    return get_output (0).operator>> (dest_block);
}


SF_Input_Terminal&
SF_Block::get_input (int index) const throw (SF_Indexing_Exception)
    // Returns a pointer to the input terminal with the specified index.  If the
    // index is out of bounds, an exception is thrown.

    // Throws exceptions: SF_Indexing_Exception.
{
    return *((SF_Input_Terminal*)input_terminals->get_node (index));
}

SF_Input_Terminal&
SF_Block::get_input (const char* name) const throw (SF_Exception)
    // Returns a pointer to the input terminal with the specified name.  If
    // there is no block with such name (case-insensitively), an exception is
    // thrown.

    // Throws exceptions: SF_Exception.
{
    int i;
    __SF_ASSERT (name != 0);

    for (i = 0; i < get_num_inputs (); i++)
    {
	if (strcasecmp (name, get_input (i).get_name ()) == 0)
	    return get_input (i);
    }
    throw SF_Exception (this, "No such input terminal");
}

SF_Output_Terminal&
SF_Block::get_output (int index) const throw (SF_Indexing_Exception)
    // Returns a pointer to the output terminal with the specified index.  If
    // the index is out of bounds, an exception is thrown.

    // Throws exceptions: SF_Indexing_Exception.
{
    return *((SF_Output_Terminal*)output_terminals->get_node (index));
}

SF_Output_Terminal&
SF_Block::get_output (const char* name) const throw (SF_Exception)
    // Returns a pointer to the output terminal with the specified name.  If
    // there is no block with such name (case-insensitively), an exception is
    // thrown.

    // Throws exceptions: SF_Exception.
{
    int i;
    __SF_ASSERT (name != 0);

    for (i = 0; i < get_num_outputs (); i++)
    {
	if (strcasecmp (name, get_output (i).get_name()) == 0)
	    return get_output (i);
    }
    throw SF_Exception (this, "No such output terminal");
}


SF_Terminal&
SF_Block::get_terminal (int index) const throw (SF_Indexing_Exception)
    // Returns one of all the (input and output) terminals, indexed with a
    // single number.

    // Throws exceptions: SF_Indexing_Exception
{
    if (index < 0 || index >= get_num_terminals ())
    {
	throw SF_Indexing_Exception (this, "Terminal index out of bounds");
    }
    if (index < get_num_inputs ())
    {
	return get_input (index);
    }
    else
    {
	return get_output (index - get_num_inputs ());
    }
}

void
SF_Block::add_input_terminal (SF_Input_Terminal* input) throw (SF_Exception)
    // Add the specified input terminal to the block.  This may cause the
    // indices of other terminals to change.  The name of the terminal must be
    // (case-insensitively) unique or an exception is thrown.

    // Throws exceptions: SF_Exception.
{
    // check for name uniqueness
    bool unique_name;
    __SF_ASSERT (input != 0);

    try
    {
	unique_name = false;
	get_input (name);
    }
    catch (SF_Exception)
    {
	unique_name = true;
    }
    // Throw an exception if the name isn't unique
    if (!unique_name)
    {
	throw SF_Exception (this, "Every input terminal within a block must have a unique name");
    }

    input_terminals->add (input);
}

void
SF_Block::add_output_terminal (SF_Output_Terminal* output) throw (SF_Exception)
    // Add the specified output terminal to the block.  This may cause the
    // indices of other terminals to change.  The name of the terminal must be
    // (case-insensitively) unique or an exception is thrown.

    // Throws exceptions: SF_Exception.
{
    // check for name uniqueness
    bool unique_name;
    __SF_ASSERT (output != 0);

    try
    {
	unique_name = false;
	get_output (name);
    }
    catch (SF_Exception)
    {
	unique_name = true;
    }
    // Throw an exception if the name isn't unique
    if (!unique_name)
    {
	throw SF_Exception (this, "Every output terminal within a block must have a unique name");
    }

    output_terminals->add (output);
}

void
SF_Block::remove_input_terminal (SF_Input_Terminal* input) throw (SF_Exception)
    // Remove the specified input terminal from the block.  This may cause the
    // indices of other terminals to change.
    // FIXME: check for connections to avoid DANGLING POINTER BUG
{
    __SF_ASSERT (input != 0);
    input_terminals->remove (input);
}

void
SF_Block::remove_output_terminal (SF_Output_Terminal* output) throw (SF_Exception)
    // Remove the specified output terminal from the block.  This may cause the
    // indices of other terminals to change.
    // FIXME: check for connections to avoid DANGLING POINTER BUG
{
    __SF_ASSERT (output != 0);
    output_terminals->remove (output);
}


/* EOF */
