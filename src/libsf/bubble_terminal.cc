/* SF_Bubble_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/bubble_terminal.hh>
#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/network.hh>
#include <sf/typedefs.h>

SF_Bubble_Terminal::SF_Bubble_Terminal (const char* name,
					const SF_Network* host_network,
					bool input,
					SF_Frequency sample_rate,
					double relative_sample_rate)
    throw (SF_Exception)
    : SF_Input_Terminal (name,
			 (input == true) ? host_network : 0,
			 false,
			 sample_rate,
			 relative_sample_rate),
      SF_Output_Terminal (name,
			  (input == false) ? host_network : 0,
			  sample_rate,
			  relative_sample_rate),
      host_network (host_network)
    // Construct an input or output bubble terminal without default value.  The
    // host_block of the bubble part is 0, i.e. input and output bubble
    // terminals differ in this from the other terminals.
{
}

SF_Bubble_Terminal::SF_Bubble_Terminal (const char* name,
					const SF_Network* host_network,
					bool input,
					SF_Sample default_value,
					SF_Frequency sample_rate,
					double relative_sample_rate)
    throw (SF_Exception)
    : SF_Input_Terminal (name,
			 (input == true) ? host_network : 0,
			 default_value,
			 false,
			 sample_rate,
			 relative_sample_rate),
      SF_Output_Terminal (name,
			  (input == false) ? host_network : 0,
			  sample_rate,
			  relative_sample_rate),
      host_network (host_network)
    // Construct an input or output bubble terminal with default value.  The
    // host_block of the bubble part is 0, i.e. input and output bubble
    // terminals differ in this from the other terminals.
{
}

SF_Bubble_Terminal::~SF_Bubble_Terminal () throw ()
    // Destruct the bubble terminal.
{
}

void
SF_Bubble_Terminal::set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception)
{
    //SF_Input_Terminal::set_sample_rate (sample_rate);
    //SF_Output_Terminal::set_sample_rate (sample_rate);
}

void
SF_Bubble_Terminal::set_relative_sample_rate (double relative_sample_rate) throw (SF_Exception)
{
    // Disable set_relative_sample_rate for bubble terminals.  This method is
    // not available in bubble terminals since the relative sample rates are
    // completely determined by the network topology.
    throw SF_Terminal_Exception (0, //this,
				 "SF_Bubble_Terminal::set_relative_sample_rate () is not for use (bad design?)");
}

double
SF_Bubble_Terminal::get_relative_sample_rate () const throw ()
{
    //FIXME: this must check and compute the relative sample rates of all bubble
    //terminals in the network
    return 0;
}

SF_Frame*
SF_Bubble_Terminal::get_frame () const throw ()
{
    // Uglily cast away the constness to satisfy GCC 2.95 (FIXME!)
    return ((SF_Bubble_Terminal*)this)->get_source_frame (0);
}

/* EOF */
