/* SF_Frame
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <math.h>

#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/typedefs.h>

// Class variable to hold the "global" frame duration parameter.
SF_Time SF_Frame::duration = SF_DEFAULT_FRAME_DURATION;

SF_Frame::SF_Frame (SF_Frequency sample_rate) throw (SF_Exception)
    : signal (0),
      num_rows (0),
      num_columns (0),
      num_channels (0),
      max_num_rows (0),
      max_num_columns (0),
      max_num_channels (0)
    // Instantiate a new frame and allocate it to an empty signal.
{
    reallocate (sample_rate);
}

SF_Frame::~SF_Frame () throw ()
    // destructor of a frame
{
    // Free the existing signal if there is one
    delete[] signal;
    signal = 0;
    num_rows = 0;	num_columns = 0;	num_channels = 0;
    max_num_rows = 0;	max_num_columns = 0;	max_num_channels = 0;
}

void
SF_Frame::reallocate (SF_Frequency sample_rate) throw (SF_Exception)
    // Allocate memory for the signal to be held in the frame.  The size is
    // calculated according to the sample_rate parameter, since the millisecond
    // duration is fixed.  The max_num_rows and num_rows variables are
    // initialized accordingly.  The freshly created signal store is initialized
    // to all zeros.
{
    // Free the existing signal if there is one
    delete[] signal;
    signal = 0;
    num_rows = 0;	num_columns = 0;	num_channels = 0;
    max_num_rows = 0;	max_num_columns = 0;	max_num_channels = 0;

    SF_Length memory_length;
    // sanity check
    if (sample_rate < 0)
    {
	throw SF_Exception (0, "Sample rate must not be negative");
    }
    // Compute the memory size needed
    max_num_rows = (unsigned int)ceil (sample_rate * duration * 1e-3);
    max_num_columns = 1;
    max_num_channels = 1;
    memory_length = max_num_rows * max_num_columns * max_num_channels;
    // Allocate the memory if nonzero size
    if (memory_length != 0)
    {
	signal = new SF_Sample[memory_length];
    }
    // Clear the newly allocated signal to all zeros and initialize the frame
    // signal to fill the allocated memory
    clear ();
}

void
SF_Frame::clear () throw ()
    // Clear the signal contents of the frame.
{
    if (signal != 0)
    {
	for (SF_Length i = 0; i < max_num_rows * max_num_columns * max_num_channels; i++)
	{
	    signal[i] = 0;
	}
    }
    num_rows = max_num_rows;
    num_columns = max_num_columns;
    num_channels = max_num_channels;
}


SF_Time
SF_Frame::get_duration () throw ()
    // Return the duration (in milliseconds) of every frame in the system.
    // Since the frame duration is a class variable of the SF_Frame class, the
    // frame duration is fixed for all networks and between them, i.e. the the
    // duration is the same inter-network as well as intra-network.

    // Doesn't throw exceptions.
{
    return duration;
}

void
SF_Frame::set_duration (SF_Time dur) throw (SF_Exception)
    // Set the pseudo-global frame duration variable (in milliseconds).  This
    // function is only meant for the global initialization of the libsf library
    // and should not be called during the operation on networks and blocks.
{
    if (dur <= 0)
    {
	throw SF_Exception (0, "Duration must be greater than zero");
    }
    duration = dur;
}
 
/* EOF */
