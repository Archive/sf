/* Exceptions and warnings
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
#include <stdio.h> // sprintf ()

#include <sf/block.hh>
#include <sf/terminal.hh>
#include <sf/exception.hh>

// Class variable to hold the pointer to the current "global" warning handler.
SF_Warning_Handler	SF_Warning::warning_handler = 0;

// Boolean class variable to indicate whether warnings are suppressed or not.
bool			SF_Warning::suppress_warnings = false;

SF_Exception::SF_Exception (const SF_Block* origin,
			    const char* description)
    : origin (origin), description (description)
    // The constructor of a SF_Exception.  Used in throwing exceptions as
    // follows: throw SF_Exception (block_ptr, "An error has happened"); The
    // origin arbument indicates the block which is associated to the error and
    // the description argument provides a textual description of the error for
    // the user.

    // Doesn't throw exceptions.
{
}

SF_Exception::~SF_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Warning::SF_Warning (const SF_Block* origin,
			const char* message)
    : origin (origin), message (message)
    // The constructor of a SF_Warning.  Warnings are used with the constructor
    // as follows: SF_Warning (block_ptr, "Something suspicious has happened");

    // Doesn't throw exceptions.
{
    print ();
}

SF_Warning::~SF_Warning ()
    // Doesn't throw exceptions.
{
}

void
SF_Warning::print ()
    // The member function used to actually "print" the warning message via the
    // warning handler.  Note that this may actually not result in printing to
    // the standard output stream.  For example, the warnings may be disabled or
    // there may be a GUI application which has provided a warning handler of
    // its own.

    // Doesn't throw exceptions.
{
    if (!suppress_warnings && warning_handler != 0)
    {
	warning_handler (*this);
    }
}

bool
SF_Warning::get_suppression ()
    // Used to query whether warning messages are suppressed or not.

    // Doesn't throw exceptions.
{
    return suppress_warnings;
}

void
SF_Warning::set_suppression (bool suppress)
    // Used to install or remove suppression of warnings.  The call
    // set_suppression (true) suppresses (disables) warning messages and
    // set_suppression (false) enables warning messages.

    // Doesn't throw exceptions.
{
    suppress_warnings = suppress;
}

SF_Warning_Handler
SF_Warning::get_handler ()
    // You may get the pointer to the warning handler function with this
    // function call.

    // Doesn't throw exceptions.
{
    return warning_handler;
}

void
SF_Warning::set_handler (SF_Warning_Handler handler)
    // You may set the warning handler function with this function call.

    // Doesn't throw exceptions.
{
    warning_handler = handler;
}

SF_Network_Sanity_Exception::SF_Network_Sanity_Exception (const SF_Block* origin,
							  const char* description)
    : SF_Exception (origin, description)
    // Used in throwing exceptions.

    // Doesn't throw exceptions.
{
}

SF_Network_Sanity_Exception::~SF_Network_Sanity_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Math_Exception::SF_Math_Exception (const SF_Block* origin,
				      const char* description)
    : SF_Exception (origin, description)
    // Used in throwing exceptions.

    // Doesn't throw exceptions.
{
}

SF_Math_Exception::~SF_Math_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Indexing_Exception::SF_Indexing_Exception (const SF_Block* origin,
					      const char* description)
    : SF_Exception (origin, description)
    // Used in throwing exceptions.

    // Doesn't throw exceptions.
{
}

SF_Indexing_Exception::~SF_Indexing_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Memory_Exception::SF_Memory_Exception (const char* description)
    : SF_Exception (0, description)
    // Used in throwing exceptions.

    // Doesn't throw exceptions.
{
}

SF_Memory_Exception::~SF_Memory_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Terminal_Exception::SF_Terminal_Exception (const SF_Terminal* terminal,
					      const char* description)
    : SF_Exception (0, description),
      terminal (terminal)
    // Used in throwing exceptions.

    // Doesn't throw exceptions.
{
    if (terminal != 0)
    {
	origin = terminal->get_host_block ();
    }
}

SF_Terminal_Exception::~SF_Terminal_Exception ()
    // Doesn't throw exceptions.
{
}

SF_Assertion_Exception::SF_Assertion_Exception (const char* expression,
						const char* source_file,
						const int source_line)

    : SF_Exception (0, 0),
      expression (expression),
      source_file (source_file),
      source_line (source_line)
{
    description = new char[1000]; // kludge
    sprintf ((char*)description, "Assertion failed: %s, file %s, line %d",
	     expression, source_file, source_line);
}
SF_Assertion_Exception::~SF_Assertion_Exception ()
{
    delete[] description;
    description = 0;
}

/* EOF */
