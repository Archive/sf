/* SF_Input_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/typedefs.h>

SF_Input_Terminal::SF_Input_Terminal (const char* name,
				      const SF_Block* host,
				      bool multiple_wires,
				      SF_Frequency sample_rate,
				      double relative_sample_rate)
    throw (SF_Exception)
    : SF_Terminal (name,
		   host,
		   sample_rate,
		   relative_sample_rate),
      multiple_wires (multiple_wires),
      enable_default (false),
      default_value (0),
      default_frame (0)
    // Construct an input terminal.  This constructor doesn't include a default
    // value, i.e. the input terminal constructed will not have a default value.
{
}

SF_Input_Terminal::SF_Input_Terminal (const char* name,
				      const SF_Block* host,
				      SF_Sample default_value,
				      bool multiple_wires,
				      SF_Frequency sample_rate,
				      double relative_sample_rate)
    throw (SF_Exception)
    : SF_Terminal (name,
		   host,
		   sample_rate,
		   relative_sample_rate),
      multiple_wires (multiple_wires),
      enable_default (true),
      default_value (default_value),
      default_frame (0)
    // Construct an input terminal.  This constructor contains a default value
    // argument, i.e. the input terminal constructed will have a default value.
{
    this->default_frame = new SF_Frame (sample_rate);
    fill_default_frame ();
}

SF_Input_Terminal::~SF_Input_Terminal () throw ()
    // Destruct an input terminal.
{
}

void
SF_Input_Terminal::only_set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception)
    // Set the sample rate (in hertz) of the incoming data to the input
    // terminal.  This will both initialize the sample rate and also set the
    // sample rates of all output terminals on the block.  The sample rates of
    // the output terminals are calculated based on the relative sample rates of
    // the terminals.  After this function is finished, the block may have
    // nonmatching sample rates in its input terminals.
{
    // first do the base class stuff
    SF_Terminal::only_set_sample_rate (sample_rate);
    // then reallocate and reinitialize the default frame if needed
    if (enable_default)
    {
	default_frame->reallocate (sample_rate);
	fill_default_frame ();
    }
}

void
SF_Input_Terminal::add_input (const SF_Output_Terminal* in)
    throw (SF_Memory_Exception, SF_Terminal_Exception)
    // This function is for the creation of wires.  This function is called by
    // SF_Output_Terminal::connect(), i.e. it is not part of the "public API".
{
    if (!is_full ())
    {
	wires->add (in);
    }
    else
    {
	throw SF_Terminal_Exception (this,
				     "No more connections allowed to input terminal");
    }
}

void
SF_Input_Terminal::remove_input (const SF_Output_Terminal* in) throw (SF_Exception)
    // This function is for the destruction of wires.  This function is called
    // by SF_Output_Terminal::disconnect(), i.e. it is not part of the "public
    // API".
{
    try
    {
	wires->remove (in);
    }
    catch (SF_Exception exc)
    {
	(void)exc; // I guess the MipsPro CC requires this
	throw SF_Terminal_Exception (this, "Internal error: asymmetrical wire");
    }
}

SF_Sample
SF_Input_Terminal::get_default () const throw (SF_Terminal_Exception)
    // Returns the default value of the input terminal, if there is any.  Throws
    // an exception if there isn't.
{
    if (enable_default == false)
    {
	throw SF_Terminal_Exception (this,
				     "There is no default value in input terminal");
    }
    return default_value;
}

void
SF_Input_Terminal::fill_default_frame () throw (SF_Terminal_Exception)
    // fill the default frame with the default value
{
    SF_Length i;
    SF_Sample* s = 0;
    if (default_frame == 0)
    {
	throw SF_Terminal_Exception (this,
				     "No default frame in input terminal");
    }
    s = default_frame->get_signal ();
    for (i = 0; i < default_frame->get_num_rows (); i++)
    {
	s[i] = default_value;
    }
}

SF_Frame*
SF_Input_Terminal::get_source_frame (int index) throw (SF_Exception)
    // Returns the frame pointer from the specified (index) connection to this
    // input terminal.  There is hidden logic related to default values in
    // here.  An exception is thrown iff index is out of bounds.

    // Throws exceptions: SF_Exception.
{
    if (enable_default && get_num_wires () == 0 && index == 0)
    {
	return default_frame;
    }
    return get_source_terminal (index)->get_frame ();
}

int
SF_Input_Terminal::get_num_source_frames () throw ()
    // Returns the number of frames for reading for this input terminal.  There
    // is hidden logic related to default values in here.

    // Doesn't throw exceptions.
{
    if (enable_default && get_num_wires () == 0)
    {
	return 1;
    }
    return get_num_wires ();
}

/* EOF */
