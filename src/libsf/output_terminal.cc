/* SF_Output_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#include <sf/block.hh>
#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/input_terminal.hh>
#include <sf/network.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

SF_Output_Terminal::SF_Output_Terminal (const char* name,
					const SF_Block* host,
					SF_Frequency sample_rate,
					double relative_sample_rate)
    throw (SF_Memory_Exception)
    : SF_Terminal (name,
		   host,
		   sample_rate,
		   relative_sample_rate)
    // Construct an output terminal.
{
    output_frame = new SF_Frame (sample_rate);
}

SF_Output_Terminal::~SF_Output_Terminal () throw ()
    // Destruct an output terminal.
{
    delete output_frame;
    output_frame = 0;
}

void
SF_Output_Terminal::only_set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception)
    // Set the sample rate (in hertz) of the data going out from the output
    // terminal.  This will both initialize the sample rate and also reallocate
    // the frame located at the terminal.

    // Throws exceptions: SF_Exception.
{
    // first do the base class stuff
    SF_Terminal::only_set_sample_rate (sample_rate);
    // and then reallocate the output frame
    output_frame->reallocate (sample_rate);
}

void
SF_Output_Terminal::connect (SF_Input_Terminal& destination) throw (SF_Exception)
    // Make a wire (connection) between an output terminal and an input
    // terminal.  The direction of the wire is from this output terminal to the
    // specified destination input terminal.  The connection function is
    // supposed to be called by the user.  Connections can be made also with the
    // `>>' operator in flexible ways; check SF_Output_Terminal::operator>> ()
    // as well as SF_Block::operator>> ().
{
    SF_Network* beg_host_net = 0;
    SF_Network* end_host_net = 0;
    
    // Sanity check: check that the terminals belong to the same network
    if (host_block != 0)
    {
	beg_host_net = host_block->get_host_network ();
    }
    else
    {
	// find out the bubble's host network
	beg_host_net = ((SF_Bubble_Terminal*)this)->get_host_network ();
    }
    if (destination.get_host_block () != 0)
    {
	end_host_net = destination.get_host_block ()->get_host_network ();
    }
    else
    {
	// find out the bubble's host network
	end_host_net = ((SF_Bubble_Terminal*)&destination)->get_host_network ();
    }
    if (beg_host_net == 0
	|| end_host_net == 0
	|| (beg_host_net != end_host_net))
    {
	throw SF_Exception (host_block, "Blocks must belong to the same network");
    }
    // throws exceptions
    destination.add_input (this);
    wires->add (&destination);

    // Propagate sample rates along the new connection; we would like to prevent
    // propagation the other way, and knowing how SF_Terminal::set_sample_rate()
    // works, it is done like this:
    this->propagation_visited = true;
    try
    {
	destination.set_sample_rate (sample_rate);
    } catch (SF_Exception e)
    {
	// MAKE SURE that the visited flag we set gets cleared even in case of
	// an exception.
	this->propagation_visited = false;
	throw e;
    }
    this->propagation_visited = false;
}

void
SF_Output_Terminal::disconnect (SF_Input_Terminal& destination) throw (SF_Exception)
    // Remove a wire (connection) between an output terminal and an input
    // terminal. The disconnection function is supposed to be called by the
    // user.

    // Throws exceptions: SF_Exception.
{
    if (wires->find (&destination))
    {
	destination.remove_input (this);
	wires->remove (&destination);
    }
    else
    {
	throw SF_Exception (host_block, "No such connection");
    }

    // Propagate sample rates from the output terminal in the case that the
    // disconnection would fix some sample rate imbalance problems.
    set_sample_rate (sample_rate);
}

SF_Block& 
SF_Output_Terminal::operator>> (SF_Input_Terminal& dest_terminal) throw (SF_Exception)
    // The `>>' operator duplicates the functionality of connect (), i.e. you
    // can write something like
    //	    sine.get_output (Y) >> adder >> fileout.get_input (X);
    // and have a connection between the blocks.  In this example the blocks
    // would be connected in cascade.

    // Throws exceptions: SF_Exception.
{
    connect (dest_terminal);
//    if (dest_terminal.get_host_block () == 0)
//    {
//	throw SF_Exception (0, "FIXME error in using operator>>");
//    }
    return *(dest_terminal.get_host_block ());
}

SF_Block& 
SF_Output_Terminal::operator>> (SF_Input_Terminal* dest_terminal) throw (SF_Exception)
    // The `>>' operator duplicates the functionality of connect (), i.e. you
    // can write something like
    //	    sine.get_output (Y) >> adder >> fileout.get_input (X);
    // and have a connection between the blocks.  In this example the blocks
    // would be connected in cascade.

    // Throws exceptions: SF_Exception.
{
    return operator>> (*dest_terminal);
}

SF_Block&
SF_Output_Terminal::operator>> (SF_Block& dest_block) throw (SF_Exception)
    // Connect this terminal to the only input of the specified block.

    // Throws exceptions: SF_Exception
{
    if (dest_block.get_num_inputs () != 1)
    {
	throw SF_Exception (&dest_block, "SF_Output_Terminal::operator>> destination block must have exactly one input terminal");
    }
    return operator>> (dest_block.get_input (0));
}

/* EOF */
