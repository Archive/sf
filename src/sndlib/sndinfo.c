/* sndinfo describes sounds */

#if defined(HAVE_CONFIG_H)
  #include "config.h"
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#if (defined(NEXT) || (defined(HAVE_LIBC_H) && (!defined(HAVE_UNISTD_H))))
  #include <libc.h>
#else
  #include <unistd.h>
  #include <string.h>
#endif
#include <errno.h>

#include "sndlib.h"

  #include <time.h>

#if MACOS
  #include <console.h>
#endif

int main(int argc, char *argv[])
{
  int fd,chans,srate,samples;
  float length;
  time_t date;
  char *comment;
  char timestr[64];
#if MACOS
  argc = ccommand(&argv);
#endif
  if (argc == 1) {printf("usage: sndinfo file\n"); exit(0);}
  initialize_sndlib();
  fd = clm_open_read(argv[1]); /* see if it exists */
  if (fd != -1)
    {
      close(fd);
      date = sound_write_date(argv[1]);
      srate = sound_srate(argv[1]);
      chans = sound_chans(argv[1]);
      samples = sound_samples(argv[1]);
      comment = sound_comment(argv[1]); 
      length = (float)samples / (float)(chans * srate);
#if (!defined(HAVE_CONFIG_H)) || defined(HAVE_STRFTIME)
      strftime(timestr,64,"%a %d-%b-%y %H:%M %Z",localtime(&date));
#else
      sprintf(timestr,"dunno");
#endif
      fprintf(stdout,"%s:\n  srate: %d\n  chans: %d\n  length: %f\n",
	      argv[1],srate,chans,length);
      fprintf(stdout,"  type: %s\n  format: %s\n  written: %s\n  comment: %s\n",
	      sound_type_name(sound_header_type(argv[1])),
	      sound_format_name(sound_data_format(argv[1])),
	      timestr,(comment) ? comment : "");
    }
  else
    fprintf(stderr,"%s: %s\n",argv[1],strerror(errno));
  return(0);
}
