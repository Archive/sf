/* audinfo decribes the current audio hardware state */

#if defined(HAVE_CONFIG_H)
  #include "config.h"
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#if (defined(NEXT) || (defined(HAVE_LIBC_H) && (!defined(HAVE_UNISTD_H))))
  #include <libc.h>
#else
  #include <unistd.h>
  #include <string.h>
#endif

#include "sndlib.h"

int main(int argc, char *argv[])
{
  initialize_sndlib();
  describe_audio_state();
  return(0);
}
