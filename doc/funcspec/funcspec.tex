% funcspec.tex - Sonic Flow functional specification
%
% $Id$
% Copyright (C) 1998 Jarno Sepp��nen, Sami Kananoja

\documentclass[a4paper,10pt]{report}
\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[dvips]{graphicx}

\pagestyle{headings}
\title{\textbf{Sonic Flow}\\
	{\large A Program for the Design and Simulation of\\
	Audio Signal Processing Networks}\\
	Functional Specification\\
	{\large $Revision$}}

\date{\today}
\author{Jarno Sepp�nen \and Sami Kananoja}

\begin{document}
\maketitle

{\small\begin{verbatim}
$Log$
Revision 1.1  1999/01/04 12:45:00  jams
Initial revision

% Revision 2.3  1998/10/25  18:57:13  jams
% Added \pagestyle{headings}, revision to title page,
% reference to Dattorro, removed d_{max} input signal from delay line.
%
% Revision 2.2  1998/10/21  11:37:04  jams
% Added specification of network printing to section 4.1.2.
%
% Revision 2.1  1998/10/21  11:20:39  jams
% Revised delay line block specification.
%
% Revision 1.3  1998/10/05  06:06:00  jams
% Final functional specification as of meeting on 5.10.1998.
%
% Revision 1.2  1998/10/03  19:54:37  jams
% Otherwise ready, ch. 3, "data and data base" and "overview" in ch. 2 are missing.
% Some FIXME's are still waiting though.
%
% Revision 1.1  1998/09/20  12:59:27  jams
% Initial revision
%
\end{verbatim}}

\tableofcontents
\newpage

\chapter{INTRODUCTION}
\section{Purpose}
This document is based on a modified, Finnish version of
ANSI/IEEE~830--1984~\cite{ansi830}.

The purpose of this document is to describe the functionality of a program for
designing and simulating audio signal processing networks.

All registered trademarks are property of their respective owners.

\section{Scope}
The name of the program is ``Sonic Flow''.  In this document the word
\emph{program} is used to designate the whole piece of software specified in
this document.

The purpose of the program is to enable the user to design and simulate signal processing
\emph{networks} via a C++ \emph{application program interface}~(API).

The program is distributed under the GNU Library General Public
License~(LGPL)~\cite{lgpl}.

\section{Definitions, acronyms and abbreviations}
\begin{tabular}{lp{10cm}}
\emph{AIFF} &	Audio Interchange File Format, a~format for saving audio
files. \\
\emph{API} &	Application Program Interface. \\
\emph{ASCII} &	American Standard Code for Information Interchange.
8-bit character tables according to ISO~8859-1 are used. \\
\emph{Block} &	Independent signal processor. \\
\emph{Frame} &	A~short segment of a signal. \\
$f_s$ &	Sample frequency in Hertz. \\
\emph{IIR} &	A~digital linear filter whose impulse response length is
infinite. \\
\emph{LCG} &	Linear Congruential Generator, an algorithm for
generating sequences of pseudo-random numbers. \\
\emph{Load} &	System load is measured with the average number of jobs
in the run queue.  This can be reviewed with the system command
\textsf{uptime}. \\
\emph{Network} &	A~system of \emph{blocks} interconnected by {\em wires}.
There may be zero or more blocks and wires in a network. \\
\emph{OS} &	Operating System. \\
\emph{PCM} &	Pulse Code Modulation.  A~method for describing (audio)
signals in digital domain. \\
\emph{RIFF WAV} &	Microsoft Resource Interface File Format, a~format for
saving audio files. \\
\emph{SP} &	Signal Processing. \\
\emph{SPL} &	Signal Processing Library.  The routines implementing the functions
described in the SPL API. \\
\emph{Wire} &	Unidirectional signal connection between \emph{blocks}. \\
\end{tabular}

\section{References}
See the bibliography at the end of the document at page~\pageref{cha:bibliography}.

\section{Overview}
The second chapter contains an overview of the whole program and its use.  The
chapter is a walk-through of the remaining chapters of this document.

The third chapter contains a definition of all the data contained, consumed and
produced within the program.  The relations of pieces of data are defined with
respect to each other.

The fourth chapter describes all the functions of the program.

The fifth chapter describes all the external interfaces of the program.

The sixth chapter deals with the remaining features of the program,
eg.~performance and maintainability.

The seventh chapter lists the constrains related to the technical specification
of the program.

The eighth chapter contains ideas for the further development of the program.

The bibliography is located at the end of the document.

\chapter{GENERAL DESCRIPTION}

\section{Product perspective}
The program is run independent, provided that the OS and API requirements are
met (chapter~\ref{cha:design constraints}).

\section{Product functions}
The program has a single interface: the \emph{signal processing library}~(SPL)
\emph{application program interface}~(API).  The SPL API provides means for
simulating and designing signal processing networks at the C++ programming
language~\cite{stroustrup} level.

\section{User characteristics}
The program is designed to be installed and operated by literate people adept at
computer programming.  There are no restrictions on the frequency of the use of
the program.  The SP~library, when installed, can be used by several
user-programmed application programs at a time.

\section{General constraints}
The program is used on variants of the UNIX operating system and requires some
external software packages to operate.

The user must configure his or her software development environment in order to
operate the program.

See section~\ref{sec:sw constraints}.

\section{Assumptions and dependencies}
The program can be installed and operated on certain kinds of UNIX machines.
The installation can be configured to build a 32-bit or 64-bit
version\footnote{Assuming 32-bit~\texttt{floats} and
64-bit~\texttt{doubles}.} of the program.

\chapter{DATA AND DATABASE}
\section{Content} %tietosis��lt��

\subsubsection{Block}
\begin{itemize}
\item unique name [text]
\item distance to the farthest source [integer]
\item input terminals [list]
\item output terminals [list]
\end{itemize}

\subsubsection{Frame}\label{sec:frame content}
\begin{itemize}
\item length (samples) [integer]
\item data [samples]
\end{itemize}

\subsubsection{Input terminal}
\begin{itemize}
\item frames of data [unidirectionally linked list]
\end{itemize}

\subsubsection{Network}
\begin{itemize}
\item name [text]
\item contained blocks [bidirectionally linked list]
\item sample frequency (Hz) [integer]
\end{itemize}

\subsubsection{Output terminal}
\begin{itemize}
\item pointers to destination input terminals [unidirectionally linked list]
\end{itemize}

Figure~\ref{fig:er diagram} illustrates the relations of the above data entities
with each other.
\begin{figure}[t]
\begin{center}
\includegraphics[width=10cm]{er-chart.eps}
\caption{ER diagram.}
\end{center}
\label{fig:er diagram} \end{figure}

\section{Intensity of use} %k��ytt��intensiteetti
The SP library interactively by one or more application programs at a time.

\section{Capacity requirements} %kapasiteettivaatimukset
The amount of networks, blocks and wires is restricted by the amount of
available memory.

\chapter{FUNCTIONS}

\section{Signal processing library}

\subsection{General}
The signal processing library is an independent part of the system.  The library
has several functions related to eg. the creation and modification of networks.
All the library functionality can be used only with the C++ programming
language.

\subsection{Description and purpose}
Simulations can be run on different signal processing networks independently
from each other.  When initiating a simulation run, the user must specify the
length of the simulation.  The simulation length is specified by means of the
output signal duration, in milliseconds, seconds, minutes or some other unit.

\subsubsection{Networks}
A~network consists of zero or more blocks and zero or more wires.

All networks have a function for printing a textual description of the network
topology.  This function will examine the structure of the network and formulate
a printout of the structure with the names of blocks and input/output
terminals.  The printout is output to an output stream such as the
\emph{standard output} stream of a process.

\subsubsection{Wires} % and frame relaying
Wires are unidirectional connections between a single output terminal of a block
and a single input terminal of a block.  There may be one or more wires
connected to a single output terminal.  Theoretically there may be multiple
independent connections to block input terminals as well, but it is possible to
restrict the number of connections a certain input terminal can handle.  For
example, the adder block (see section~\ref{sec:adder}) has a single input
terminal and a single output terminal while it can handle multiple input signals
and can produce multiple (identical) output signals.

An input and an output terminal can be connected and disconnected by using the
services provided by the SP library API.

\subsubsection{Frames}
All the signals flowing through the SP networks are segmentated to short
frames.  These frames are very short, typically a few milliseconds each.  The
frame length can be set between simulation runs.

The frame objects do not have a fixed duration but all can have a different
length, see section~\ref{sec:frame content}.

\subsubsection{Input terminals}
There is exactly one input terminal per one signal input in a block.  The input
terminals may take multiple connections, provided that the block can handle it.
If an input terminal takes multiple connections, the number of connections can
be dynamically changed.

An input terminal does not know the source of the incoming signals.  The signals
are delivered to input terminals through wires from output terminals.

\subsubsection{Output terminals}
There is exactly one output terminal per one signal output in a block.  A~single
output terminal can be connected to multiple input terminals, dynamically.
An~output terminal knows the destination input terminal if there is a wire
between them.

\subsection{Processing}
Network design is accomplished by creating blocks and by connecting the blocks
together with wires.  There are functions for creating and destroying networks
and blocks and making and removing the connections between blocks.

Network simulation is another function of the SP library.  The simulation is
carried out to a certain network.  The network may necessarily not be suitable
for simulation, and the simulation function will inspect the network before
simulation.  In the case of errors the application program using the library
will be notified.

The simulation is carried out for a preset duration.  The specification of this
duration is done via the SP library~API.

\subsection{Error handling}
If there is an error which stops the network simulation, all the opened files
will be closed.
\subsubsection{Memory allocation}
The library will detect out of memory situations and will notify the application
program in such cases.
\subsubsection{Block errors}
If an error occurs during processing of any block, the block orders the library
to stop the simulation of the network.  The application program using the
library will identify the block that caused the error and give a textual
description of the error.

\section{Blocks}

\subsection{General to all blocks}
\subsubsection{Description and purpose}
Blocks are independent signal processing algorithms with well-defined inputs and
outputs.  In general a block is the lowest-level implementation that is not
divided into subparts.  This implies that there may be some redundancy in the
functionalities between different blocks.
\subsubsection{Inputs and outputs}
There are no inputs and outputs general to all blocks.
\subsubsection{Processing}
There are five functional features implemented in every block:
\begin{enumerate}
\item constructor
\item pre-simulation initialization
\item simulation
\item post-simulation cleanup
\item destructor
\end{enumerate}
\subsubsection{Error handling}
There are two kinds of general error situations detected.
\paragraph{Mathematics errors.}  The base class will catch the math exceptions
and stop the simulation of the network in these cases.  Some examples of math
exceptions are over- and underflow and division by zero.
\paragraph{Memory allocation errors.}  The individual blocks will detect out of
memory situations and respond by stopping the simulation of the network.

\subsection{Adder}\label{sec:adder}
\subsubsection{Description and purpose}
Sum an arbitrary number of input signals together and output the sum.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x_1$ &	signal \\
$\vdots$ & $\vdots$ \\
$N.$ &	$x_N$ \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	sum signal \\
\end{tabular}
\subsubsection{Processing}
The output is calculated as the pointwise sum of the inputs:
\begin{equation}
y[n] = \sum_{i=1}^N x_i[n]
\end{equation}
\subsubsection{Error handling}
No specific error situations.

\begin{table}
\begin{center}
\caption{Calculation of the biquad IIR filter coefficients~$a_i$ and~$b_i$ as
described in~\protect\cite{bristow-johnson}.}
\label{tab:biquad}
\begin{tabular}{cc}
	\begin{tabular}{|l|l|l|l|l|}
	\hline
	 & LPF & HPF & BPF & BSF \\
	\hline
	$a_0$ &	$1+\alpha$ &			$1+\alpha$ &			$1+\alpha$ &		$1+\alpha$	\\
	$a_1$ &	$-2\cos\omega$ &		$-2\cos\omega$ &		$-2\cos\omega$ &	$-2\cos\omega$	\\
	$a_2$ &	$1-\alpha$ &			$1-\alpha$ &			$1-\alpha$ &		$1-\alpha$	\\
	$b_0$ &	$\frac{1-\cos\omega}{2}$ &	$\frac{1+\cos\omega}{2}$ &	$\alpha$ &		$1$		\\
	$b_1$ &	$1-\cos\omega$ &		$-1-\cos\omega$ &		$0$ &			$-2\cos\omega$	\\
	$b_2$ &	$\frac{1-\cos\omega}{2}$ &	$\frac{1+\cos\omega}{2}$ &	$-\alpha$ &		$1$		\\
	\hline
	\end{tabular}
&
	\begin{tabular}{|l|}
	\hline

	$\begin{array}{rcl}
	\omega & = & \pi \frac{f_1 + f_2}{f_s}			\\
	Q & = & \frac{\sin \omega}{\ln 2 (f_2 - f_1) \omega}	\\
	\alpha & = & \sinh \frac{1}{2 Q} \sin \omega
	\end{array}$ \\

	\hline
	\end{tabular}
\end{tabular}
\end{center}
\end{table}

\subsection{Biquad band-pass filter} \label{sec:band-pass}
\subsubsection{Description and purpose}
Perform band-pass filtering operations on a signal.
The filter is a $2^{\textrm{nd}}$-order biquad infinite impulse response (IIR)
filter~\cite{bristow-johnson}.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$f_1$ &	lower band frequency [Hz] \\
3. &	$f_2$ &	upper band frequency [Hz] \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	filtered signal \\
\end{tabular}
\subsubsection{Processing}
The biquad filter difference equation is as follows~\cite[Eq.~2.80]{oppenheim}:
\begin{equation}
y[n] = \frac{1}{a_0} \left( \sum_{i=0}^2 b_i x[n-i] - \sum_{i=1}^2 a_i y[n-i] \right)
\end{equation}
with the coefficients~$a_i$ and~$b_i$ as calculated in table~\ref{tab:biquad} under~BPF.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$f_1 \geq f_2$ & ``Lower frequency must be less than the upper
frequency.'' \\
2. &	$(f_1 < 0) \lor (f_2 < 0)$ & ``Frequencies must be positive or zero.''
\\
3. &	$(f_1 > \frac{f_s}{2}) \lor (f_2 > \frac{f_s}{2})$ & ``Frequencies must
be less than or equal to half of sample frequency.'' \\
\end{tabular}

\subsection{Biquad band-stop filter}
\subsubsection{Description and purpose}
Perform band-stop filtering operations on a signal.
The filter is a $2^{\textrm{nd}}$-order biquad infinite impulse response (IIR)
filter~\cite{bristow-johnson}.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$f_1$ &	lower band frequency [Hz] \\
3. &	$f_2$ &	upper band frequency [Hz] \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	filtered signal \\
\end{tabular}
\subsubsection{Processing}
The processing is done as described in section~\ref{sec:band-pass}, with the
coefficients~$a_i$ and~$b_i$ as calculated in table~\ref{tab:biquad} under~BSF.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$f_1 \geq f_2$ & ``Lower frequency must be less than the upper
frequency.'' \\
2. &	$(f_1 < 0) \lor (f_2 < 0)$ & ``Frequencies must be positive or zero.''
\\
3. &	$(f_1 > \frac{f_s}{2}) \lor (f_2 > \frac{f_s}{2})$ & ``Frequencies must
be less than or equal to half of sample frequency.'' \\
\end{tabular}

\subsection{Biquad high-pass filter}
\subsubsection{Description and purpose}
Perform high-pass filtering operations on a signal.
The filter is a $2^{\textrm{nd}}$-order biquad infinite impulse response (IIR)
filter~\cite{bristow-johnson}.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$f$ &	cutoff frequency [Hz] \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	filtered signal \\
\end{tabular}
\subsubsection{Processing}
The processing is done as described in section~\ref{sec:band-pass}, with the
coefficients~$a_i$ and~$b_i$ as calculated in table~\ref{tab:biquad} under~HPF.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$f < 0$ & ``Frequencies must be positive or zero.'' \\
2. &	$f > \frac{f_s}{2}$ & ``Frequencies must be less than or equal to half
of sample frequency.'' \\
\end{tabular}

\subsection{Biquad low-pass filter}
\subsubsection{Description and purpose}
Perform low-pass filtering operations on a signal.
The filter is a $2^{\textrm{nd}}$-order biquad infinite impulse response (IIR)
filter~\cite{bristow-johnson}.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$f$ &	cutoff frequency [Hz] \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	filtered signal \\
\end{tabular}
\subsubsection{Processing}
The processing is done as described in section~\ref{sec:band-pass}, with the
coefficients~$a_i$ and~$b_i$ as calculated in table~\ref{tab:biquad} under~LPF.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$f < 0$ & ``Frequencies must be positive or zero.'' \\
2. &	$f > \frac{f_s}{2}$ & ``Frequencies must be less than or equal to half
of sample frequency.'' \\
\end{tabular}

\subsection{Delay line}
\subsubsection{Description and purpose}
Delay the incoming signal by a certain amount.  Initially the delay line
contains all zeros.

The delay implements integer and fractional delay features~\cite{dattorro}, and
the delay duration parameter can be undulated during simulation.

In order for this feature to work the user must specify the maximum delay
duration $d_{max}$, which will set the length of the internal delay line buffer.
If the maximum delay duration parameter is increased, the tail of the new buffer
is padded with zeros.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$d$ &	delay duration [ms] \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	delayed signal \\
\end{tabular}
\subsubsection{Processing}
The output signal $y$ is defined as
\begin{equation}
y[n] = \left\{
	\begin{array}{ll}
	x[n-s], &	n \geq s \\
	0, &		0 \leq n < s \\
	\end{array} \right.
\end{equation}
where $s = \left\lfloor d f_s \times 10^{-3}\right\rfloor$.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$d < 0$ & ``Delay must be positive or zero.'' \\
2. &	$d$ too large & ``Delay is too large; out of memory.'' \\
3. &	$d > d_{max}$ & ``Delay must be less than or equal to maximum delay.''\\
\end{tabular}

\subsection{File input}
\subsubsection{Description and purpose}
Read in an audio signal file and output the signal in question.  The signal must
be a 1--32-bit \emph{PCM}~audio signal and must contain one channel.  Supported
file formats are \emph{AIFF}~and \emph{RIFF WAV}.  The sampling rate specified
in the signal file is discarded.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Input:}}\\
1. &	$t$ &	file name \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	signal \\
\end{tabular}
\subsubsection{Processing}
An integer PCM~signal $v$ is read from the file $t$ by using a third-party audio
file library called \textsf{libaudiofile}~\cite{libaudiofile}.  The output
signal $y$ is constructed from the integer signal by converting it to a
floating-point representation within $[-1, 1)$:
\begin{equation}
y = \frac{v}{2^{b-1}}
\end{equation}
where $b$ denotes the width (number of bits) of the integer data~$v$.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	file not found & ``Audio file $t$ is not found.'' \\
2. &	file not readable & ``Audio file $t$ is not readable.'' \\
3. &	unknown file format & ``Audio file $t$ is not an AIFF nor a WAV file.'' \\
4. &	non-monaural file & ``Audio file $t$ contains several channels.'' \\
5. &	non-PCM coding & ``Audio file $t$ does not contain linear PCM data.'' \\
\end{tabular}

\subsection{File output}
\subsubsection{Description and purpose}
Write out an audio signal file of the input signal.  The output data is written
as 16-bit PCM~audio.  Output files are written in AIFF~file format.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$t$ &	file name \\
\multicolumn{3}{l}{\emph{No outputs.}} \\
\end{tabular}
\subsubsection{Processing}
The input signal $x$ is quantized to a~16-bit integer representation $v$ with a
similar quantizer as described in section~\ref{sec:quantizer}.  The signal is
clipped to be within $[-1, 1]$ prior to quantization.  The clipped and quantized
signal is written to the file $t$ by using a third-party audio file library
called \textsf{libaudiofile}~\cite{libaudiofile}.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	file not writable & ``Audio file $t$ is not writable.'' \\
\end{tabular}

\subsection{Max/min}
\subsubsection{Description and purpose}
Find the maximum and minimum values of an arbitrary number of input
signals. The values are found fore each sample separetely.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x_1$ &	signal \\
$\vdots$ & $\vdots$ \\
$N.$ &	$x_N$ \\
\multicolumn{3}{l}{\emph{Outputs:}} \\
1. &	$y_{min}$ &	minimum signal \\
2. &	$y_{max}$ &	maximum signal \\
\end{tabular}
\subsubsection{Processing}
The output signals $y_{min}$ and $y_{max}$ are defined as
\begin{equation}
y_{min}[n] = \min_{i} x_i[n]
\end{equation}
\begin{equation}
y_{max}[n] = \max_{i} x_i[n]
\end{equation}
\subsubsection{Error handling}
No specific error situations.

\subsection{Multiplier}
\subsubsection{Description and purpose}
Multiply an arbitrary number of input signals together and output the product.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x_1$ &	signal \\
$\vdots$ & $\vdots$ \\
$N.$ &	$x_N$ \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	product signal \\
\end{tabular}
\subsubsection{Processing}
The output is calculated as the pointwise product of the inputs:
\begin{equation}
y[n] = \prod_{i=1}^N x_i[n]
\end{equation}
\subsubsection{Error handling}
No specific error situations.

\subsection{Negation}
\subsubsection{Description and purpose}
Negate the incoming signal.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Input:}}\\
1. &	$x$ &	signal \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	negated signal \\
\end{tabular}
\subsubsection{Processing}
The output signal $y$ is defined as
\begin{equation}
y[n] = -x[n]
\end{equation}
\subsubsection{Error handling}
No specific error situations.

\subsection{Noise generator}
\subsubsection{Description and purpose}
Generate white noise with a uniform probability density function.  The noise is
a pseudo-random number sequence generated with a \emph{linear congruential
generator}~(LCG).
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Input:}}\\
1. &	$A$ &	amplitude \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	noise signal \\
\end{tabular}
\subsubsection{Processing}
The LCG equation is as follows~\cite[Eq.~7.1.2]{nr}
\begin{equation}
I_{j+1}=aI_j \pmod{m}
\end{equation}
where $a=7^5=16807$ and $m=2^{31}-1=2147483647$~\cite[Eq.~7.1.3]{nr}.  $I_0$~is
the seed value for the pseudo-random number sequence.  The seed is initialized
to a promiscuous(FIXME) value; see~\cite[p.~223]{perl}.
\begin{equation}
\Rightarrow y[n]=2 A[n] \left( \frac{I_{n}}{m-1} - \frac{1}{2} \right)
\end{equation}
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$A < 0$ & ``Amplitude must be non-negative.'' \\
\end{tabular}


\subsection{Quantizer}\label{sec:quantizer}
\subsubsection{Description and purpose}
Quantize the signal by using a uniform quantizer.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$x$ &	signal \\
2. &	$b$ &	number of bits \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	quantized signal \\
\end{tabular}
\subsubsection{Processing}
The uniform midtread quantizer equation is as follows~\cite[Eq.~1]{lipshitz}:
\begin{equation}
y[n] = \Delta \left\lfloor \frac{x[n]}{\Delta} + \frac{1}{2} \right\rfloor
\end{equation}
where $\Delta = 2^{1-b[n]}$.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$(b < 1) \lor (b > 32)$ & ``Number of bits must be between 1 and 32.'' \\
2. &	$b$ non-integer\footnote{usually implemented as $b-\epsilon
\leq \lfloor b \rfloor \leq b+\epsilon$, where $\epsilon$ is the
smallest representable number.} & ``Number of bits must be integer.'' 
\end{tabular}

\subsection{Reciprocal}
\subsubsection{Description and purpose}
Compute the reciprocal signal of a signal.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Input:}}\\
1. &	$x$ &	signal \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	reciprocal signal \\
\end{tabular}
\subsubsection{Processing}
The output is calculated as the pointwise reciprocal of the input:
\begin{equation}
y[n] = \frac{1}{x[n]}
\end{equation}
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$x = 0$ & ``Division by zero.'' \\
\end{tabular}

\subsection{Sine generator}
\subsubsection{Description and purpose}
Generate a sine signal.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{Inputs:}}\\
1. &	$f$ &	frequency [Hz] \\
2. &	$A$ &	amplitude \\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	sine signal \\
\end{tabular}
\subsubsection{Processing}
The output signal $y$ is defined as
\begin{equation}
y[n] = A[n] \sin\phi[n]
\end{equation}
with
\begin{equation}
\phi[n] = \phi[n-1] + \frac{2\pi f[n]}{f_s}
\end{equation}
and $\phi[0]=0$.
\subsubsection{Error handling}
\begin{tabular}{llp{6cm}}
\emph{Number} & \emph{Cause} & \emph{Error text} \\
1. &	$f > \frac{f_s}{2}$ & ``Frequency must be smaller than equal to sample frequency.'' \\
2. &	$A < 0$ & ``Amplitude must be non-negative.'' \\
\end{tabular}

\subsection{Variable}
\subsubsection{Description and purpose}
Generate a signal of selected value.
\subsubsection{Inputs and outputs}
\begin{tabular}{lll}
\multicolumn{3}{l}{\emph{No inputs.}}\\
\multicolumn{3}{l}{\emph{Output:}} \\
1. &	$y$ &	variable signal \\
\end{tabular}
\subsubsection{Processing}
The output signal $y$ is defined as
\begin{equation}
y[n] = k
\end{equation}
where $k$ is desired variable value.
\subsubsection{Error handling}
No specific error situations.

\chapter{EXTERNAL INTERFACES} %ULKOISET LIITTYM��T
\section{Hardware interfaces}
There are no specific pieces of hardware related to the project.

\section{Software interfaces}
The program may use external audio files such as AIFF and WAV files, depending
on the networks used.  The audio files may be used as inputs to the SP network
or as outputs from the network or both.

\section{Communications interfaces}
The program involves no communications interfaces.

\chapter{OTHER FEATURES} %MUUT OMINAISUUDET

% (T��ss�� kerrotaan ne ei-toiminnalliset ominaisuudet.)

\section{Performance requirements} \label{sec:perf req}
The performance of the library as regards the simulation of networks is nearly
completely dependent on the number of included blocks and the performance of
their algorithms.  The library adds a minor overhead to the simulation
performance.

\section{Usability, recovery, security} %k��ytett��vyys, toipuminen, turvallisuus, suojaukset
Not taken into account in the project.

\section{Maintainability}
\paragraph{Addition of new blocks.}  The object-orientated approach allows
new external blocks to be incorporated to the signal processing system.  The
interfaces of the blocks are written in C++ and the blocks are compiled
separately from the library.

The blocks are compatible with certain versions of the SP library.  The block
interface may be different between different library versions, and the blocks
must specify the library version needed.

\paragraph{Library updating.}  The SP library can be updated by installing a
newer version of the library.  An installed library can be removed,
ie.~uninstalled.

\section{Portability and compatibility} %siirrett��vyys/kannettavuus, yhteensopivuus
The source code of the SP library and the blocks is portable between computer
platform which have the GNU~C Compiler~\cite{gcc} and GNU~libtool~\cite{libtool}
installed.  Every platform requires the code to be compiled explicitly, though.

\section{Operating} %operointi
The user is required to incorporate the SP library to his programming
environment.  This may require
\begin{itemize}
\item setting environment variables such as \texttt{LD\_{}LIBRARY\_{}PATH}
\item setting compiler flags during compilation; see eg.~the \emph{GCC}
manual~\cite{gcc} or the manual of the user's compiler
\item other procedures described in the \emph{GNU~libtool} manual~\cite{libtool}
\end{itemize}

\chapter{DESIGN CONSTRAINTS} %SUUNNITTELURAJOITTEET
\label{cha:design constraints}

\section{Standards}
The library is programmed in the C++ programming language~\cite{stroustrup}.

The technical specification document is based on a modified, Finnish version of
ANSI/IEEE~1016--1987~\cite{ansi1016}.

\section{Hardware constraints} %laitteistorajoitteet
The program runs on Pentium-level computers with at least 32~MB of memory.

\section{Software constraints}\label{sec:sw constraints} %ohjelmistorajoitteet
The program must compile with the GNU GCC compiler~\cite{gcc}.  The shared SP
library is built using the GNU libtool~\cite{libtool} software.  The release
package is built using the GNU autoconf~\cite{autoconf} software.

The audio file reading and writing is implemented via the commercial SGI Audio
File library~\cite{libaudiofile} or a public domain implementation of the
library~\cite{tichstuff}.

Supported platforms are Red Hat GNU/Linux 2.0 systems and SGI IRIX 6.2 systems.

\section{Other constraints}
Not taken into account in the project.

\chapter{FURTHER DEVELOPMENT} %JATKOKEHITYSAJATUKSIA

\begin{itemize}
\item hierarchical networks: allow blocks to be either \emph{atomic blocks} or
\emph{networks}:
	\begin{description}
	\item[\emph{Atomic block}]	Blocks which cannot be taken apart by means of
	the program.
	\item[\emph{Block}]	Independent signal processor.  A block may be either
	an \emph{atomic block} or a {\em network}.
	\end{description}
\item real-time networks
\item multirate networks
\item more atomic blocks:
	\begin{itemize}
	\item ADC
	\item DAC
	\item Decimate/interpolate
	\item MATLAB interface
	\item Oscilloscope/spectrum analyzer
	\item Sampler/wavetable synthesis
	\end{itemize}
\item save/load network objects as whole
\item GUI
\end{itemize}

\begin{thebibliography}{99}
\label{cha:bibliography}

\bibliographystyle{alpha} % tai abbrev
\bibitem{ansi830}
ANSI/IEEE~830--1984,
\emph{``Guide to Software Requirements Specifications.''}
1984

\bibitem{ansi1016}
ANSI/IEEE~1016--1987,
\emph{``Recommended Practice for Software Design Descriptions.''}
1987

\bibitem{bristow-johnson}
Bristow-Johnson, Robert,
\emph{``The Equivalence of Various Methods of Computing Biquad Coefficients for
Audio Parametric Equalizers.''}
\textsf{http://www.harmonycentral.com/Effects/Articles/EQ\_{}Coefficients/},
Preprint of the $97^{\textrm{th}}$ AES Convention,
November 1994

\bibitem{dattorro}
Dattorro, Jon,
\emph{``Effect Design --- Part 2: Delay-Line Modulation and Chorus.''}
\emph{Journal of the Audio Engineering Society,}
Vol. 45, No. 10,
pp. 764--788,
October 1997

\bibitem{autoconf}
Free Software Foundation, Inc.,
\emph{``GNU Autoconf.''}
\textsf{http://www.gnu.org/software/autoconf/},
1998

\bibitem{gcc}
Free Software Foundation, Inc.,
\emph{``GNU Compiler.''}
\textsf{http://www.gnu.org/software/gcc/},
1998

\bibitem{lgpl}
Free Software Foundation, Inc.,
\emph{``GNU Library General Public License.''}
Version 2,
\textsf{http://www.gnu.org/copyleft/lgpl.html},
June 1991

\bibitem{libtool}
Free Software Foundation, Inc.,
\emph{``GNU Libtool.''}
\textsf{http://www.gnu.org/software/libtool/},
1998

\bibitem{tichstuff}
Kent, Richard and Phillips, Dave,
\emph{``tichstuff: a collection of headers and libraries needed to port SGI
sound apps to Linux.''}
\textsf{ftp://mustec.bgsu.edu/pub/linux/tichstuff.tar.gz},
March 1998

\bibitem{lipshitz}
Lipshitz, Stanley~P., Wannamaker, Robert~A. and Vanderkooy, John,
\emph{``Quantization and Dither: A Theoretical Survey.''}
\emph{Journal of the Audio Engineering Society,}
Vol. 40, No. 5,
pp. 355--375,
May 1992

\bibitem{oppenheim}
Oppenheim, Alan V. and Schafer, Ronald W.,
\emph{``Discrete-Time Signal Processing.''}
ISBN 0-13-216771-9,
Prentice-Hall, New Jersey,
p.~36,
1989

\bibitem{nr}
Press, William~H., Teukolsky, Saul~A., Vetterling, William~T. and Flannery,
Brian~P.,
\emph{``Numerical Recipes in C: The Art of Scientific Computing.''}
$2^{\textrm{nd}}$ edition,
ISBN 0-521-43108-5,
Cambridge University Press
1992

\bibitem{libaudiofile}
Silicon Graphics, Inc.,
\emph{``\textsf{AFintro(3dm)} --- Introduction to the Silicon Graphics Audio
File Library~(AF).''}
IRIX 6.2 manual page for \textsf{AFintro(3dm)},
1997

\bibitem{stroustrup}
Stroustrup, Bjarne,
\emph{``The C++ Programming Language.''}
$2^{\textrm{nd}}$ edition,
ISBN 0-201-53992-6,
AT\&T Bell Telephone Laboratories, Inc.,
669 pages,
August 1992

\bibitem{perl}
Wall, Larry, Christiansen, Tom and Schwartz, Randal L.,
\emph{``Programming Perl.''}
$2^{\textrm{nd}}$ edition,
ISBN 1-56592-149-6,
O'Reilly \& Associates, Inc.,
November 1996

\end{thebibliography}

\end{document}
