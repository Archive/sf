/* SF_Network class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_NETWORK_HH__
#define __SF_NETWORK_HH__

#include <sf/block.hh>
#include <sf/bubble_terminal.hh>
#include <sf/exception.hh>
#include <sf/list.hh>
#include <sf/typedefs.h>

class SF_Network : public SF_Block
{
public:
    SF_Network (const char* name = 0) throw (SF_Exception);
    virtual ~SF_Network () throw ();
    
    virtual void	print (ostream& stream) const throw ();
    virtual bool	is_functional () const throw ();
    virtual void	initialize () throw (SF_Exception, SF_Network_Sanity_Exception);
    virtual void	execute () throw (SF_Exception);
    virtual void	finish () throw (SF_Exception);
    
    // block handling
    void		add_block (SF_Block& block) throw (SF_Exception);
    void		remove_block (SF_Block& block) throw (SF_Exception);

    void		check () throw (SF_Exception, SF_Network_Sanity_Exception);

    // Functions for the creation and destroying of input and output terminals
    // and their associated input and output bubbles, respectively.
    void		create_input (const char* name) throw (SF_Exception);
    void		create_output (const char* name) throw (SF_Exception);
    void		destroy_input (const char* name) throw (SF_Exception);
    void		destroy_input (int index) const throw (SF_Indexing_Exception);
    void		destroy_output (const char* name) throw (SF_Exception);
    void		destroy_output (int index) const throw (SF_Indexing_Exception);

    // Functions for access to the bubbles from inside the network FIXME: return SF_Bubble_Terminal&?
    SF_Output_Terminal&	get_input_bubble (int index) const throw (SF_Indexing_Exception);
    SF_Output_Terminal&	get_input_bubble (const char* name) const throw (SF_Exception);
    SF_Input_Terminal&	get_output_bubble (int index) const throw (SF_Indexing_Exception);
    SF_Input_Terminal&	get_output_bubble (const char* name) const throw (SF_Exception);

    inline void         set_class_name (const char* class_name) throw ();
    virtual bool	is_producer () const throw ();
    virtual bool	is_consumer () const throw ();

protected:
    // list of blocks in the network (SF_Network_Block_List_Contents)
    SF_List*		block_list;
    // The struct inside block_list
    struct SF_Network_Block_List_Content
    {  
	int		rank;
	bool		is_visited;
	SF_Block*	block;
    };
    // Functions for using block_list
    SF_Network_Block_List_Content*		search_block_list (const SF_Block* block) const throw ();
    inline SF_Network_Block_List_Content*	get_block_node (int index) const throw (SF_Exception);
    inline int					get_num_blocks () const throw ();

    // The depth-first-search algorithm and its subroutine
    void		assign_ranks_downstream (SF_Network_Block_List_Content* start,
						 int rank)
	throw (SF_Assertion_Exception, SF_Network_Sanity_Exception);
    static int		compare_ranks (const void* a, const void* b)
	throw (SF_Assertion_Exception);

    void		unwire_block (SF_Block* block) throw (SF_Assertion_Exception);
    void                partition_network () throw (SF_Exception, SF_Network_Sanity_Exception);
    void		clear_frames (SF_Block* block) throw (SF_Assertion_Exception);
    void		check_functionality () const throw (SF_Exception);
    void		check_bubble_wirings () const throw (SF_Exception);

private:    
    //Disable copy constructor and operator=
    SF_Network (const SF_Network& a);
    SF_Network& operator= (const SF_Network& a);
};

inline SF_Network::SF_Network_Block_List_Content*
SF_Network::get_block_node (int index) const throw (SF_Exception)
    // Fetch the content of the index'th node in the block list.  If the index
    // is out of bounds (must be 0 <= index < num_blocks), throws
    // an exception.

    // Throws exceptions: SF_Exception.
{
    return (SF_Network_Block_List_Content*) (block_list->get_node (index));
}

inline int
SF_Network::get_num_blocks () const throw ()
    // Returns the number of blocks in this network.

    // Doesn't throw exceptions.
{
    return block_list->get_num_nodes ();
}

inline void
SF_Network::set_class_name (const char* class_name) throw ()
{
    class_name = class_name;
}

#endif
/* EOF */
