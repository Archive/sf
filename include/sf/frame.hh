/* SF_Frame declaration
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_FRAME_HH__
#define __SF_FRAME_HH__

#include <sf/defaults.hh>
#include <sf/typedefs.h>

class SF_Frame
{
public:
    SF_Frame (SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE) throw (SF_Exception);
    virtual ~SF_Frame () throw ();

    inline SF_Sample*	get_signal () const throw ();
    inline SF_Length	get_num_rows () const throw ();
    inline SF_Length	get_num_columns () const throw ();	// for future use
    inline SF_Length	get_num_channels () const throw ();	// for future use
    void		reallocate (SF_Frequency sample_rate) throw (SF_Exception);
    void		clear () throw ();

    // for global initialization FIXME only	 (DEFAULT_DURATION defined in cc-file)
    // access to the static data member duration (DO NOT TOUCH (set-function))
    static SF_Time	get_duration () throw ();
    static void		set_duration (SF_Time dur) throw (SF_Exception);

private:
    //Disable copy constructor and operator=
    SF_Frame (const SF_Frame& a);
    SF_Frame& operator= (const SF_Frame& a);

    static SF_Time	duration;  // declaration of a static data member

    SF_Sample*		signal;	  // (max_num_rows*max_num_cols*max_num_chans) samples
    SF_Length		num_rows;
    SF_Length		num_columns;
    SF_Length		num_channels;
    SF_Length		max_num_rows;
    SF_Length		max_num_columns;
    SF_Length		max_num_channels;

};

inline SF_Sample* 
SF_Frame::get_signal () const throw ()
{
    return signal;
}

inline SF_Length
SF_Frame::get_num_rows () const throw ()
{
    return num_rows;
}

inline SF_Length
SF_Frame::get_num_columns () const throw ()
{
    return num_columns;
}

inline SF_Length
SF_Frame::get_num_channels () const throw ()
{
    return num_channels;
}

#endif

/* EOF */
