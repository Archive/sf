/* SF_Input_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_INPUT_TERMINAL_HH__
#define __SF_INPUT_TERMINAL_HH__

#include <sf/defaults.hh>
#include <sf/frame.hh>
#include <sf/list.hh>
#include <sf/output_terminal.hh>
#include <sf/terminal.hh>
#include <sf/typedefs.h>

// In order to prevent an endless loop of `#include's, block.hh is not
// #included.  Instead, a pre-declaration is done:
class SF_Block;

class SF_Input_Terminal : public SF_Terminal
{
public:	      
    SF_Input_Terminal (const char* name,
		       const SF_Block* host,
		       bool multiple_wires,
		       SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE, 
		       double relative_sample_rate = SF_DEFAULT_RELATIVE_SAMPLE_RATE)
	throw (SF_Exception);
    SF_Input_Terminal (const char* name,
		       const SF_Block* host,
		       SF_Sample default_value,
		       bool multiple_wires,
		       SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE, 
		       double relative_sample_rate = SF_DEFAULT_RELATIVE_SAMPLE_RATE)
	throw (SF_Exception);
    virtual ~SF_Input_Terminal () throw ();

    inline SF_Output_Terminal*	get_source_terminal (int index) throw (SF_Indexing_Exception);

    SF_Frame*		get_source_frame (int index) throw (SF_Exception);
    int			get_num_source_frames () throw ();
    SF_Sample		get_default () const throw (SF_Terminal_Exception);
    inline bool		has_default () const throw ();

protected:
    inline bool		is_full () const throw ();	// true if no more connections are allowed
    void		fill_default_frame () throw (SF_Terminal_Exception);

    // SF_Output_Terminal::connect () is a friend of ours in order to be able to
    // call add_input ().  This friendship is justified with input and output
    // terminals being two objects close together.
    virtual void	add_input (const SF_Output_Terminal* in) throw (SF_Memory_Exception, SF_Terminal_Exception);
    friend void		SF_Output_Terminal::connect (SF_Input_Terminal& destination) throw (SF_Exception);

    // SF_Output_Terminal::disconnect () is a friend of ours in order to be able
    // to call remove_input ().  This friendship is justified with input and
    // output terminals being two objects close together.
    virtual void	remove_input (const SF_Output_Terminal* in) throw (SF_Exception);
    friend void		SF_Output_Terminal::disconnect (SF_Input_Terminal& destination) throw (SF_Exception);

    virtual void	only_set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception);

    bool		multiple_wires;
    bool		enable_default;
    SF_Sample		default_value;
    SF_Frame*		default_frame;		// used if there is a default
    
private:    
    //Disable copy constructor and operator=
    SF_Input_Terminal (const SF_Input_Terminal& a);
    SF_Input_Terminal& operator= (const SF_Input_Terminal& a);
};

inline SF_Output_Terminal*
SF_Input_Terminal::get_source_terminal (int index) throw (SF_Indexing_Exception)
    // Fetch the index'th source terminal from the list of connections.  If the
    // index is out of bounds (must be 0 <= index < degree), throws an
    // exception.
{
    return (SF_Output_Terminal*) get_wired_terminal (index);
}

inline bool
SF_Input_Terminal::has_default () const throw ()
{
    return enable_default;
}

inline bool
SF_Input_Terminal::is_full () const throw ()
    // Returns true if no more connections are allowed to this input terminal.

    // Doesn't throw exceptions.
{
    if (!multiple_wires && (get_num_wires () > 0))
    {
	return true;
    }
    return false;
}

#endif
/* EOF */
