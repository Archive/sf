/* default values for various parameters
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_DEFAULTS_HH__
#define __SF_DEFAULTS_HH__

#include <sf/typedefs.h>

// duration of a frame in milliseconds by default
const SF_Time		SF_DEFAULT_FRAME_DURATION = 1; // ms
// sample rate in hertz by default
const SF_Frequency	SF_DEFAULT_SAMPLE_RATE = 44100; // Hz
// sample rate relation by default
const double		SF_DEFAULT_RELATIVE_SAMPLE_RATE = 1;

// for comparison of sample rates
const SF_Frequency	SF_DEFAULT_SAMPLE_RATE_EPSILON = 0.01; // Hz

#endif

/* EOF */
