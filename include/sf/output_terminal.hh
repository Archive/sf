/* SF_Output_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_OUTPUT_TERMINAL__
#define __SF_OUTPUT_TERMINAL__

#include <sf/defaults.hh>
#include <sf/frame.hh>
#include <sf/list.hh>
#include <sf/terminal.hh>
#include <sf/typedefs.h>

// In order to prevent an endless loop of `#include's, block.hh and
// input_terminal.hh are not #included.  Instead, a pre-declaration is done:
class SF_Block;
class SF_Input_Terminal;

class SF_Output_Terminal : public SF_Terminal
{
public:
    SF_Output_Terminal (const char* name,
			const SF_Block* host,
			SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE,
			double relative_sample_rate = SF_DEFAULT_RELATIVE_SAMPLE_RATE)
	throw (SF_Memory_Exception);
    virtual ~SF_Output_Terminal () throw ();

    inline SF_Input_Terminal*	get_destination_terminal (int index) throw (SF_Indexing_Exception);

    virtual void	connect (SF_Input_Terminal& destination) throw (SF_Exception);
    virtual void	disconnect (SF_Input_Terminal& destination) throw (SF_Exception);

    SF_Block&		operator>> (SF_Input_Terminal& dest_terminal) throw (SF_Exception);
    SF_Block&		operator>> (SF_Input_Terminal* dest_terminal) throw (SF_Exception);
    SF_Block&		operator>> (SF_Block& dest_block) throw (SF_Exception);
    virtual inline SF_Frame*	get_frame () const throw ();

protected:
    virtual void	only_set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception);

private:
    // the frame which is filled every round
    SF_Frame*		output_frame;

    //Disable copy constructor and operator=
    SF_Output_Terminal (const SF_Output_Terminal& a);
    SF_Output_Terminal& operator= (const SF_Output_Terminal& a);
};

inline SF_Frame* 
SF_Output_Terminal::get_frame () const throw ()
    // Return a pointer to the frame which takes the data out of this block.
{
    return output_frame;
}

inline SF_Input_Terminal*
SF_Output_Terminal::get_destination_terminal (int index) throw (SF_Indexing_Exception)
    // Fetch the index'th destination terminal from the list of connections.  If
    // the index is out of bounds (must be 0 <= index < degree), throws an
    // exception.

    // Throws exceptions: SF_Exception.
{
    return (SF_Input_Terminal*) get_wired_terminal (index);
}

#endif
/* EOF */
