/* The C language API
   Copyright (C) 1998 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_STUBS_H__
#define __SF_STUBS_H__

#include <sf/typedefs.h>

/* Make the class names known within the C namespace */
#ifndef __cplusplus
typedef int bool;

typedef void SF_Block;
typedef void SF_Input_Terminal;
typedef void SF_Network;
typedef void SF_Output_Terminal;
typedef void SF_Terminal;

typedef void SF_Adder;
typedef void SF_Band_Pass_Filter;
typedef void SF_Band_Stop_Filter;
typedef void SF_Delay_Line;
typedef void SF_File_Input;
typedef void SF_File_Output;
typedef void SF_High_Pass_Filter;
typedef void SF_Impulse_Generator;
typedef void SF_Low_Pass_Filter;
typedef void SF_Max_Min;
typedef void SF_Multiplier;
typedef void SF_Negation;
typedef void SF_Noise_Generator;
typedef void SF_Quantizer;
typedef void SF_Reciprocal;
typedef void SF_Sine_Generator;
typedef void SF_Variable;
#else
#include <sf/sf.hh>
#include <sf/blocks/adder.hh>
#include <sf/blocks/band_pass_filter.hh>
#include <sf/blocks/band_stop_filter.hh>
#include <sf/blocks/biquad_filter.hh>
#include <sf/blocks/delay_line.hh>
#include <sf/blocks/file_input.hh>
#include <sf/blocks/file_output.hh>
#include <sf/blocks/high_pass_filter.hh>
#include <sf/blocks/impulse_generator.hh>
#include <sf/blocks/low_pass_filter.hh>
#include <sf/blocks/max_min.hh>
#include <sf/blocks/multiplier.hh>
#include <sf/blocks/negation.hh>
#include <sf/blocks/noise_generator.hh>
#include <sf/blocks/quantizer.hh>
#include <sf/blocks/reciprocal.hh>
#include <sf/blocks/sine_generator.hh>
#include <sf/blocks/variable.hh>
#endif

typedef void (*SF_Exception_Handler)(const SF_Block* origin,
				     const char* description);

/*	Generic		*/

void            SF_set_exception_handler (SF_Exception_Handler handler);
SF_Exception_Handler SF_get_exception_handler (void);
void            SF_initialize (void);
void            SF_finish (void);

/*	SF_Block	*/

const char*     SF_Block__get_class_name (SF_Block* block);
const char*     SF_Block__get_instance_name (SF_Block* block);
void            SF_Block__set_instance_name (SF_Block* block, const char* instance_name);
SF_Network*     SF_Block__get_host_network (SF_Block* block);
void            SF_Block__set_host_network (SF_Block* block, SF_Network* host);
bool            SF_Block__is_functional (SF_Block* block);
bool            SF_Block__is_producer (SF_Block* block);
bool            SF_Block__is_consumer (SF_Block* block);
unsigned int    SF_Block__get_num_input_terminals (SF_Block* block);
unsigned int    SF_Block__get_num_output_terminals (SF_Block* block);
SF_Input_Terminal* const SF_Block__get_inputs (SF_Block* block);
SF_Output_Terminal* const SF_Block__get_outputs (SF_Block* block);

/*	SF_Output_Terminal	*/

void            SF_Output_Terminal__connect (SF_Output_Terminal *output_terminal, SF_Input_Terminal *destination);
void            SF_Output_Terminal__disconnect (SF_Output_Terminal *output_terminal, SF_Input_Terminal *destination);

/*	SF_Network	*/

SF_Network*     SF_Network__new (void);
void            SF_Network__delete (SF_Network* network);
void            SF_Network__add_block (SF_Network* network, SF_Block* block);
void            SF_Network__remove_block (SF_Network* network, SF_Block* block);
void            SF_Network__check (SF_Network* network);
SF_Time         SF_Network__get_simulation_duration (SF_Network* network);
void            SF_Network__set_simulation_duration (SF_Network* network, SF_Time dur_ms);
void            SF_Network__start_simulation (SF_Network* network);
void            SF_Network__stop_simulation (SF_Network* network);
bool            SF_Network__is_running (SF_Network* network);
bool            SF_Network__is_error (SF_Network* network);

/*	SF_Terminal	*/

const char*     SF_Terminal__get_name (SF_Terminal* terminal);
SF_Block*       SF_Terminal__get_host_block (SF_Terminal* terminal);
SF_Frequency    SF_Terminal__get_sample_rate (SF_Terminal* terminal);
void            SF_Terminal__set_sample_rate (SF_Terminal* terminal, SF_Frequency sample_rate);
double          SF_Terminal__get_relative_sample_rate (SF_Terminal* terminal);
void            SF_Terminal__set_relative_sample_rate (SF_Terminal* terminal, double relative_sample_rate);

/*	All blocks	*/

SF_Adder*		SF_Adder__new (void);
void			SF_Adder__delete (SF_Adder* adder);
SF_Band_Pass_Filter*	SF_Band_Pass_Filter__new (void);
void			SF_Band_Pass_Filter__delete (SF_Band_Pass_Filter* band_pass_filter);
SF_Band_Stop_Filter*	SF_Band_Stop_Filter__new (void);
void			SF_Band_Stop_Filter__delete (SF_Band_Stop_Filter* band_stop_filter);
SF_Delay_Line*		SF_Delay_Line__new (void);
void			SF_Delay_line__delete (SF_Delay_Line* delay_line);
SF_File_Input*		SF_File_Input__new (void);
void			SF_File_Input__delete (SF_File_Input* file_input);
SF_File_Output*		SF_File_Output__new (void);
void			SF_File_Output__delete (SF_File_Output* file_output);
SF_High_Pass_Filter*	SF_High_Pass_Filter__new (void);
void			SF_High_Pass_Filter__delete (SF_High_Pass_Filter* high_pass_filter);
SF_Impulse_Generator*	SF_Impulse_Generator__new (void);
void			SF_Impulse_Generator__delete (SF_Impulse_Generator* impulse_generator);
SF_Low_Pass_Filter*	SF_Low_Pass_Filter__new (void);
void			SF_Low_Pass_Filter__delete (SF_Low_Pass_Filter* low_pass_filter);
SF_Max_Min*		SF_Max_Min__new (void);
void			SF_Max_Min__delete (SF_Max_Min* max_min);
SF_Multiplier*		SF_Multiplier__new (void);
void			SF_Multiplier__delete (SF_Multiplier* multiplier);
SF_Negation*		SF_Negation__new (void);
void			SF_Negation__delete (SF_Negation* negation);
SF_Noise_Generator*	SF_Noise_Generator__new (void);
void			SF_Noise_Generator__delete (SF_Noise_Generator* noise_generator);
SF_Quantizer*		SF_Quantizer__new (void);
void			SF_Quantizer__delete (SF_Quantizer* quantizer);
SF_Reciprocal*		SF_Reciprocal__new (void);
void			SF_Reciprocal__delete (SF_Reciprocal* reciprocal);
SF_Sine_Generator*	SF_Sine_Generator__new (void);
void			SF_Sine_Generator__delete (SF_Sine_Generator* sine_generator);
SF_Variable*		SF_Variable__new (void);
void			SF_Variable__delete (SF_Variable* variable);

#endif
/* EOF */
