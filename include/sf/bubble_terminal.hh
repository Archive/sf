/* SF_Bubble_Terminal
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_BUBBLE_TERMINAL_HH__
#define __SF_BUBBLE_TERMINAL_HH__

#include <sf/defaults.hh>
#include <sf/frame.hh>
#include <sf/input_terminal.hh>
#include <sf/list.hh>
#include <sf/output_terminal.hh>
#include <sf/terminal.hh>
#include <sf/typedefs.h>

//#include "block.hh"
// In order to prevent an endless loop of `#include's, `block.hh' is NOT
// included.  Instead, a pre-declaration is done:
class SF_Block;

// This terminal is both an input terminal and an output terminal.  There are
// two separate instances of the SF_Terminal class within this terminal.
class SF_Bubble_Terminal : public SF_Input_Terminal,
			   public SF_Output_Terminal
{
public:
    SF_Bubble_Terminal (const char* name,
			const SF_Network* host_network,
			bool input,
			SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE, 
			double relative_sample_rate = SF_DEFAULT_RELATIVE_SAMPLE_RATE)
	throw (SF_Exception);
    SF_Bubble_Terminal (const char* name,
			const SF_Network* host_network,
			bool input,
			SF_Sample default_value,
			SF_Frequency sample_rate = SF_DEFAULT_SAMPLE_RATE, 
			double relative_sample_rate = SF_DEFAULT_RELATIVE_SAMPLE_RATE)
	throw (SF_Exception);
    virtual ~SF_Bubble_Terminal () throw ();

    // Overloaded functions in both of the SF_Terminals
    //inline SF_Frequency get_sample_rate () const throw ();
    virtual void	set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception);

    virtual void	set_relative_sample_rate (double relative_sample_rate) throw (SF_Exception);
    inline double	get_relative_sample_rate () const throw ();

    // Overloaded functions in the SF_Input_Terminal side
    //SF_Sample		get_default () const throw (SF_Exception);
    //inline bool		has_default () const throw ();

    // Overloaded functions in the SF_Output_Terminal side
    //virtual void	connect (SF_Input_Terminal& destination) throw (SF_Exception);
    //virtual void	disconnect (SF_Input_Terminal& destination) throw (SF_Exception);

    //inline SF_Input_Terminal*	get_destination_terminal (int index) throw (SF_Indexing_Exception);
    virtual SF_Frame*	get_frame () const throw ();

    // New functionality (FIXME: how to access this?)
    //virtual void	set_default (SF_Sample default) throw ();
    // set the name of both the bubble and the normal terminal:
    //virtual void	set_name (const char* name) throw ();

    inline SF_Network*	get_host_network () const throw ();

protected:
    // We needn't declare SF_Output_Terminal::[dis]connect () as a friend since
    // SF_Bubble_Terminal IS-A SF_Output_Terminal.
    //friend void		SF_Output_Terminal::connect (SF_Input_Terminal& destination) throw (SF_Exception);
    //friend void		SF_Output_Terminal::disconnect (SF_Input_Terminal& destination) throw (SF_Exception);

    // SF_Output_Terminal::connect () is a friend of ours in order to be able to
    // call add_input ().  This friendship is justified with input and output
    // terminals being two objects close together.
    //virtual void	add_input (const SF_Output_Terminal* in) throw (SF_Exception);

    // SF_Output_Terminal::disconnect () is a friend of ours in order to be able
    // to call remove_input ().  This friendship is justified with input and
    // output terminals being two objects close together.
    //virtual void	remove_input (const SF_Output_Terminal* in) throw (SF_Exception);

    // set the flag of both the bubble and the normal terminal:
    //inline virtual void		set_sample_rate_imbalance (bool imbalance) throw ();

    // the network this bubble belongs to; bubbles don't belong to any specific
    // block
    const SF_Network*	host_network;

private:    
    //Disable copy constructor and operator=
    SF_Bubble_Terminal (const SF_Bubble_Terminal& a);
    SF_Bubble_Terminal& operator= (const SF_Bubble_Terminal& a);
};

inline SF_Network* 
SF_Bubble_Terminal::get_host_network () const throw ()
{
    return (SF_Network*) host_network;
}

#endif
/* EOF */
