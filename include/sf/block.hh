/* SF_Block class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_BLOCK_HH__
#define __SF_BLOCK_HH__

#include <iostream.h>

#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/list.hh>
#include <sf/typedefs.h>

// In order to prevent an endless loop of `#include's, `network.hh' is NOT
// included.  Instead, a pre-declaration is done:
class SF_Network;

class SF_Block
{
public:
    SF_Block (bool producer,
	      bool consumer,
	      const char* class_name = 0,
	      const char* name = 0)
    throw (SF_Memory_Exception);
    virtual ~SF_Block () throw ();

    // pre-simulation initializations
    virtual void	initialize () throw (SF_Exception) = 0;
    // one round of simulations
    virtual void	execute () throw (SF_Exception) = 0;
    // post-simulation cleanup
    virtual void	finish () throw (SF_Exception) = 0;
			
    virtual void	print (ostream& stream) const throw ();
    virtual bool	is_functional () const throw ();

    virtual SF_Block&	operator>> (SF_Input_Terminal& dest_terminal) throw (SF_Exception);
    virtual SF_Block&	operator>> (SF_Input_Terminal* dest_terminal) throw (SF_Exception);
    virtual SF_Block&	operator>> (SF_Block& dest_block) throw (SF_Exception);

    SF_Input_Terminal&	get_input (int index) const throw (SF_Indexing_Exception);
    SF_Input_Terminal&	get_input (const char* name) const throw (SF_Exception);
    SF_Output_Terminal&	get_output (int index) const throw (SF_Indexing_Exception);
    SF_Output_Terminal&	get_output (const char* name) const throw (SF_Exception);
    SF_Terminal&	get_terminal (int index) const throw (SF_Indexing_Exception);

    inline const char*	get_class_name () const throw ();
    inline SF_Network*	get_host_network () const throw ();
    inline const char*	get_name () const throw ();
    inline unsigned int get_num_inputs () const throw ();
    inline unsigned int get_num_outputs () const throw ();
    inline unsigned int	get_num_terminals () const throw ();
    inline bool		is_producer () const throw ();
    inline bool		is_consumer () const throw ();
    inline void		set_host_network (SF_Network* host) throw ();
    inline void		set_name (const char* name) throw ();

protected:		
    void 		add_input_terminal (SF_Input_Terminal* input) throw (SF_Exception);
    void 		add_output_terminal (SF_Output_Terminal* output) throw (SF_Exception);
    void 		remove_input_terminal (SF_Input_Terminal* input) throw (SF_Exception);
    void 		remove_output_terminal (SF_Output_Terminal* output) throw (SF_Exception);

    SF_List*		input_terminals;	// list of input terminals
    SF_List*		output_terminals;	// list of output terminals
    const char*		class_name;		// generic name for the kind of this block
    const char*		name;			// an intuitive name for the very block
    SF_Network*		host_network;		// the network this block belongs to
    const bool		producer;		// true if the block can act as a source
    const bool		consumer;		// true if the block can act as a sink

private:
    //Disable copy constructor and operator=
    SF_Block (const SF_Block& a);
    SF_Block& operator= (const SF_Block& a);
};

inline const char* 
SF_Block::get_class_name () const throw ()
    // Get the class name of this block.  Can return 0.

    // Doesn't throw exceptions.
{
    return class_name;
}

inline const char* 
SF_Block::get_name () const throw ()
    // Get the name of this block (instance).  Can return 0.

    // Doesn't throw exceptions.
{
    return name;
}

inline void
SF_Block::set_name (const char* name) throw ()
    // Set the name of this block (instance).

    // Doesn't throw exceptions.
{
    this->name = name;
}

inline SF_Network* 
SF_Block::get_host_network () const throw ()
    // Get a pointer to the (lowest-level) network this block belongs to.  Can
    // return 0.

    // Doesn't throw exceptions.
{
    return host_network;
}

inline void
SF_Block::set_host_network (SF_Network* host) throw ()
    // Set the network this block belongs to.

    // Doesn't throw exceptions.
{
    this->host_network = host;
}

inline bool
SF_Block::is_producer () const throw ()
    // Return whether the block is a producer or not.  A producer can be thought
    // of as a `potential source of a signal stream.'

    // Doesn't throw exceptions.
{
    return producer;
}

inline bool
SF_Block::is_consumer () const throw ()
    // Return whether the block is a consumer or not.  A consumer can be thought
    // of as a `potential sink of a signal stream.'

    // Doesn't throw exceptions.
{
    return consumer;
}
	    
inline unsigned int
SF_Block::get_num_inputs () const throw ()
    // Return the number of input terminals in this block.

    // Doesn't throw exceptions.
{
    return input_terminals->get_num_nodes ();
}

inline unsigned int
SF_Block::get_num_outputs () const throw ()
    // Return the number of output terminals in this block.

    // Doesn't throw exceptions.
{
    return output_terminals->get_num_nodes ();
}

inline unsigned int
SF_Block::get_num_terminals () const throw ()
    // Return the number of all terminals in this block.

    // Doesn't throw exceptions.
{
    return get_num_inputs () + get_num_outputs ();
}

#endif
/* EOF */
