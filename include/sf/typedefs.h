/* Definitions of own data types
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_TYPEDEFS_H__
#define __SF_TYPEDEFS_H__

#ifdef __cplusplus
extern "C" {
#endif

    /* frequency in Hz */
typedef double SF_Frequency;

    /* time in ms (milliseconds) */
typedef double SF_Time;

    /* sample values */
typedef float SF_Sample;

    /* coefficient etc. values */
typedef double SF_Coefficient;

    /* number of samples */
typedef unsigned int SF_Length;

#ifdef __cplusplus
}
#endif

#endif

/* EOF */
