/* Global functions
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_GLOBAL_HH__
#define __SF_GLOBAL_HH__

#include <sf/exception.hh>

void	SF_initialize () throw ();
void	SF_finish () throw ();

// Create an own assertion facility; only for internal use in libsf

#ifndef NDEBUG
#define __SF_ASSERT(expr) ((expr)?((void)0):(throw SF_Assertion_Exception(#expr,__FILE__,__LINE__)))
#else
#undef __SF_ASSERT
#define __SF_ASSERT(expr) ((void)0)
#endif

#endif
/* EOF */
