/* SF_Thread class declaration
   Copyright (C) 1999 Jarno Sepp�nen and Andrew Clausen
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_THREAD_HH__
#define __SF_THREAD_HH__

#include <sf/exception.hh>
#include <sf/network.hh>
#include <sf/typedefs.h>

class SF_Thread
{
public:
    SF_Thread (SF_Network* network);
    virtual ~SF_Thread ();

    // The primary functions to start and stop a simulation thread.  The
    // simulation is carried out asynchronously, i.e. both of these functions
    // return regardless of the simulation.  real_time is used to initiate either
    // a real-time or a "full-speed" simulation run.  The duration parameter can
    // be used to set a definite length (in milliseconds) for the simulation.  A
    // duration value of 0 specifies indefinite length, i.e. the simulation is run
    // until there is a call to stop_simulation ().
    void		start_simulation (bool real_time, SF_Time duration);
    void		stop_simulation ();

    // This function can be called to query if there is a simulation running
    // currently.
    bool		is_simulation_running ();

    // This function returns a value between [0, 1] according to the phase of the
    // simulation.  The progress is calculated based on the duration specified in
    // the start_simulation () call.
    float		get_progress_meter ();

    // The following handler will be called always when the simulation stops for
    // any reason.  Currently there are three reasons: (1) the simulation duration
    // is met, (2) stop_simulation () is called or (3) there is an error in
    // simulation and an exception is thrown.  The exceptions thrown in the
    // simulation thread will be caught and the stop handler will be called with a
    // pointer to the exception.  If the simulation has stopped normally the
    // pointer will be 0.  The handler will be called from the simulation thread.
    typedef void (*SF_Exception_Handler)(const SF_Exception* exception);
    void		set_simulation_stop_handler (SF_Exception_Handler handler);
    SF_Exception_Handler	get_simulation_stop_handler ();

protected:
    // Get the current (system etc.) time in microseconds.  This is a static class
    // member function, i.e. all SF_Thread objects share a single get_time ()
    // function.
    static unsigned int	get_time ();

    // Wait until the (system etc.) time is equal to or greater than usec_time
    // in microseconds.  Suspends the calling thread.  This is a static class
    // member function, i.e. all SF_Thread objects share a single wait ()
    // function.
    static void		wait (unsigned int usec_time);

    void		simulation_thread (bool real_time, int rounds,
					   SF_Exception_Handler handler);
    bool		running;
    bool		stop;

private:    
    //Disable copy constructor and operator=
    SF_Thread (const SF_Thread& a);
    SF_Thread& operator= (const SF_Thread& a);
};



//////////////// Andrew, this is stuff I removed from network.hh.  Note that
//////////////// this is seriously obsolete stuff:



// pid_t and fork ()
#include <sys/types.h>
#include <unistd.h>


public:
    inline SF_Time	get_simulation_duration () const; // ### OBSOLETE ###
    inline void		set_simulation_duration (SF_Time duration); // ### OBSOLETE ###
    void		start_simulation (); // ### OBSOLETE ###
    void		stop_simulation (); // ### OBSOLETE ###
    inline bool		is_running () const; // ### OBSOLETE ###
    inline bool		is_error () const; // ### OBSOLETE ###
    inline SF_Exception* get_simulation_exception () const; // ### OBSOLETE ###
protected:
    SF_Time		simulation_duration;
    void		simulation_child ();	// child process
    void		child_init_signal (int signal);
    void		child_finish_signal (int signal);
    bool		child_get_signal (int signal);
    void		parent_set_signal (int signal);
    pid_t		child_pid;
    bool		simulation_running;
    SF_Exception*	simulation_exception;

inline SF_Time
SF_Network::get_simulation_duration () const
    // return the preset duration for a simulation run (in milliseconds)
{
    return simulation_duration;
}

inline void
SF_Network::set_simulation_duration (SF_Time duration)
    // set the duration of the simulation run (in milliseconds)
{
    simulation_duration = duration;
}

inline bool
SF_Network::is_running () const
    // Return true if the asynchronous simulation process for this network is
    // running at the moment.
{
    return simulation_running;
}

inline bool
SF_Network::is_error () const
    // Return true if there has been an error in the asynch. simulation
    // process.
{
    return simulation_exception != 0;
}

inline SF_Exception*
SF_Network::get_simulation_exception () const
    // Return the exception describing the error in the asynch. simulation
    // process.	 DOESN'T WORK AT THE MOMENT.  FIXME
{
    return simulation_exception;
}

#endif
/* EOF */
