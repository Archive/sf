/* Exceptions and warnings
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_EXCEPTION_HH__
#define __SF_EXCEPTION_HH__

//#include <sf/block.hh>
//#include <sf/terminal.hh>
class SF_Block;
class SF_Terminal;

class SF_Warning;
typedef void (*SF_Warning_Handler)(const SF_Warning&);

class SF_Exception
{
public:
    SF_Exception (const SF_Block* origin,
		  const char* description);
    virtual ~SF_Exception ();
		    
    const SF_Block*	origin;		// the initiator of the exception or NULL
    const char*		description;	// textual information
};

class SF_Warning
{
public:
    SF_Warning (const SF_Block* origin,
		const char* message);
    virtual ~SF_Warning ();
		    
    const SF_Block*	origin;		// the initiator of the warning or NULL
    const char*		message;	// textual information

    void		print ();

    static bool		get_suppression ();
    static void		set_suppression (bool suppress);
    static SF_Warning_Handler get_handler ();
    // FIXME: it would seem that the name "set_handler" cannot be used with
    // MipsPro CC. 
    static void		set_handler (SF_Warning_Handler handler);

protected:
    static SF_Warning_Handler warning_handler;
    static bool		suppress_warnings;
};

class SF_Network_Sanity_Exception : public SF_Exception
{
public:
    SF_Network_Sanity_Exception (const SF_Block* origin,
				 const char* description);
    virtual ~SF_Network_Sanity_Exception ();
};

class SF_Math_Exception : public SF_Exception
{
public:
    SF_Math_Exception (const SF_Block* origin,
		       const char* description);
    virtual ~SF_Math_Exception ();
};

class SF_Indexing_Exception : public SF_Exception
{
public:
    SF_Indexing_Exception (const SF_Block* origin,
			   const char* description);
    virtual ~SF_Indexing_Exception ();
};

class SF_Memory_Exception : public SF_Exception
{
public:
    SF_Memory_Exception (const char* description);
    virtual ~SF_Memory_Exception ();
};

class SF_Terminal_Exception : public SF_Exception
{
public:
    SF_Terminal_Exception (const SF_Terminal* terminal,
			   const char* description);
    virtual ~SF_Terminal_Exception ();
    const SF_Terminal*	terminal;	// the terminal associated or NULL
};

class SF_Assertion_Exception : public SF_Exception
{
public:
    SF_Assertion_Exception (const char* expression,
			    const char* source_file,
			    const int source_line);
    virtual ~SF_Assertion_Exception ();
    const char*		expression;
    const char*		source_file;
    const int		source_line;
};

#endif

/* EOF */
