/* SF_Audio_Output class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This audio is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_AUDIO_OUTPUT_HH__
#define __SF_AUDIO_OUTPUT_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Audio_Output : public SF_Block
{
public:
    SF_Audio_Output (const char* name = 0);
    virtual ~SF_Audio_Output ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

    void		set_num_channels (int num);
    inline int		get_num_channels ();

protected:
    // warnings for abnormal cases:
    bool		clipping_warning_printed;
    bool		nonfinity_warning_printed;

    // Array for holding the channel terminals
    SF_Input_Terminal**	channels;
    int			num_channels;

private:
    //Disable copy constructor and operator=
    SF_Audio_Output (const SF_Audio_Output& a);
    SF_Audio_Output& operator= (const SF_Audio_Output& a);
};


inline int
SF_Audio_Output::get_num_channels ()
{
    return num_channels;
}

#endif

/* EOF */
