/* SF_Band_Pass_Filter class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_BAND_PASS_FILTER_HH__
#define __SF_BAND_PASS_FILTER_HH__

#include <math.h>

#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/biquad_filter.hh>
   
class SF_Band_Pass_Filter : public SF_Biquad_Filter
{  
public:  
    SF_Band_Pass_Filter (const char* name = 0);
    virtual ~SF_Band_Pass_Filter ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

protected:
    inline void calculate_coefficients (SF_Frequency f_s, SF_Sample f_1, SF_Sample f_2);
    SF_Sample*		s_f1;
    SF_Sample*		s_f2;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_f1;
    SF_Input_Terminal*	in_f2;

private:
    //Disable copy constructor and operator=
    SF_Band_Pass_Filter (const SF_Band_Pass_Filter& a);
    SF_Band_Pass_Filter& operator= (const SF_Band_Pass_Filter& a);
};

inline void
SF_Band_Pass_Filter::calculate_coefficients (SF_Frequency f_s, SF_Sample f_1, SF_Sample f_2)
{
    //Calculate acording to table 4.1 of functional specification.

    // f_c and bwc are calculated when needed (BPF, BSF):
    SF_Sample f_c = (f_1 + f_2) / 2;
    SF_Sample bwc = log (f_2 / f_1) / 2; // natural logarithm
    
    SF_Sample omega = 2 * M_PI * f_c / f_s;
    SF_Sample snw = sin (omega);
    SF_Sample csw = cos (omega);
    
    SF_Sample alpha = snw * sinh (bwc * omega / snw); // BPF, BSF

    a_0 = 1 + alpha;
    a_1 = -2 * csw;
    a_2 = 1 - alpha;
    b_0 = alpha;
    b_1 = 0;
    b_2 = -alpha;
}

#endif

/* EOF */
