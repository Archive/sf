/* SF_Audio_Input class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This audio is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_AUDIO_INPUT_HH__
#define __SF_AUDIO_INPUT_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Audio_Input : public SF_Block
{
public:
    SF_Audio_Input (const char* name = 0);
    virtual ~SF_Audio_Input ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

    void		set_num_channels (int num);
    inline int		get_num_channels ();

protected:
    // Array for holding the channel terminals
    SF_Output_Terminal**channels;
    int			num_channels;
    SF_Sample		coef;		// used in int->float conversion

private:
    //Disable copy constructor and operator=
    SF_Audio_Input (const SF_Audio_Input& a);
    SF_Audio_Input& operator= (const SF_Audio_Input& a);
};

inline int
SF_Audio_Input::get_num_channels ()
{
    return num_channels;
}

#endif

/* EOF */
