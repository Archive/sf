/* SF_Rms_Estimator class declaration
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_RMS_ESTIMATOR_HH__
#define __SF_RMS_ESTIMATOR_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Rms_Estimator : public SF_Block
{
public:
    SF_Rms_Estimator (const char* name = 0);
    virtual ~SF_Rms_Estimator ();
    virtual void finish ();
    virtual void initialize ();
    virtual void execute ();

protected:
    // RMS averaging IIR filter coefficients
    SF_Coefficient	b0, a1;
    // last output value from the averaging IIR filter
    SF_Coefficient	past_y;

    // Pointers to the signals in the frames of the input and output terminals
    SF_Sample* s_x;
    SF_Sample* s_rms;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal* in_x;
    SF_Output_Terminal* out_rms;

private:
    //Disable copy constructor and operator=
    SF_Rms_Estimator (const SF_Rms_Estimator& a);
    SF_Rms_Estimator& operator= (const SF_Rms_Estimator& a);
};

#endif

/* EOF */
