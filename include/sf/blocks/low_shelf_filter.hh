/* SF_Low_Shelf_Filter class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_LOW_SHELF_FILTER_HH__
#define __SF_LOW_SHELF_FILTER_HH__

#include <math.h>

#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

#include <sf/blocks/biquad_filter.hh>
   
class SF_Low_Shelf_Filter : public SF_Biquad_Filter
{  
public:  
    SF_Low_Shelf_Filter (const char* name = 0);
    virtual ~SF_Low_Shelf_Filter ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

protected:
    inline void		calculate_coefficients (SF_Frequency f_s, SF_Sample f_c,
						SF_Sample gain, SF_Sample S);
    SF_Sample*		s_freq;
    SF_Sample*		s_gain;
    SF_Sample*		s_slope;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_freq;
    SF_Input_Terminal*	in_gain;
    SF_Input_Terminal*	in_slope;

private:
    //Disable copy constructor and operator=
    SF_Low_Shelf_Filter (const SF_Low_Shelf_Filter& a);
    SF_Low_Shelf_Filter& operator= (const SF_Low_Shelf_Filter& a);
};

inline void
SF_Low_Shelf_Filter::calculate_coefficients (SF_Frequency f_s, SF_Sample f_c,
					     SF_Sample gain, SF_Sample S)
{
    SF_Sample omega = 2 * M_PI * f_c / f_s;
    SF_Sample snw = sin (omega);
    SF_Sample csw = cos (omega);
    
    SF_Sample A = pow (10, (gain / 40));
    SF_Sample beta = sqrt ((pow (A, 2) + 1) / S - pow (A - 1, 2));

    a_0 = (A + 1) + (A - 1) * csw + beta * snw;
    a_1 = -2 * ((A - 1) + (A + 1) * csw);
    a_2 = (A + 1) + (A - 1) * csw - beta * snw;
    b_0 = A * ((A + 1) - (A - 1) * csw + beta * snw);
    b_1 = 2 * A * ((A - 1) - (A + 1) * csw);
    b_2 = A * ((A + 1) - (A - 1) * csw - beta * snw);
}

#endif

/* EOF */
