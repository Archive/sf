/* SF_Variable class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_VARIABLE_HH__
#define __SF_VARIABLE_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

class SF_Variable : public SF_Block
{
public:
    SF_Variable (const char* name = 0);
    virtual ~SF_Variable ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

    inline SF_Sample	get_value ();
    inline void		set_value (SF_Sample value);

protected:
    SF_Sample		value;
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Variable (const SF_Variable& a);
    SF_Variable& operator= (const SF_Variable& a);
};

inline SF_Sample
SF_Variable::get_value ()
    // Get the current constant value of this block.
{
    return value;
}

inline void
SF_Variable::set_value (SF_Sample value)
    // Set the constant value within this block; not to be called during
    // execution!
{
    this->value = value;
}

#endif
/* EOF */
