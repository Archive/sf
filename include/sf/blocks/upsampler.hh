/* SF_Upsampler class declaration
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_UPSAMPLER_HH__
#define __SF_UPSAMPLER_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

class SF_Upsampler : public SF_Block
{
public:
    SF_Upsampler (const char* name = 0);
    virtual ~SF_Upsampler ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

    inline void		set_ratio (int r);
    inline int		get_ratio ();

protected:
    // Upsampling ratio
    int			ratio;

    // Pointer to the signal in the frame of the input and output terminals "x"
    // and "y"
    SF_Sample*		s_x;
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Upsampler (const SF_Upsampler& a);
    SF_Upsampler& operator= (const SF_Upsampler& a);
};

inline void
SF_Upsampler::set_ratio (int r)
{
    in_x->set_relative_sample_rate (1);
    out_y->set_relative_sample_rate (r);
    ratio = r;
}

inline int
SF_Upsampler::get_ratio ()
{
    return ratio;
}

#endif

/* EOF */
