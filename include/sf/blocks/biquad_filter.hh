/* SF_Biquad_Filter class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_BIQUAD_FILTER_HH__
#define __SF_BIQUAD_FILTER_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>
   
class SF_Biquad_Filter : public SF_Block
{  
public:  
    SF_Biquad_Filter (bool producer,
		      bool consumer,
		      const char* class_name = 0,
		      const char* name = 0);
    virtual ~SF_Biquad_Filter ();

    virtual void	initialize ();
    virtual void	execute ();
    virtual void	finish ();

protected:
    inline SF_Sample	filter (SF_Sample x);
    SF_Sample		a_0, a_1, a_2;
    SF_Sample		b_0, b_1, b_2;
    SF_Sample		past_x[2], past_y[2];
    SF_Frequency	fs;
    SF_Sample*		s_x;
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Biquad_Filter (const SF_Biquad_Filter& a);
    SF_Biquad_Filter& operator= (const SF_Biquad_Filter& a);
};

inline SF_Sample
SF_Biquad_Filter::filter (SF_Sample x)
{
    SF_Sample y;
    y = b_2 * past_x[1] + b_1 * past_x[0] + b_0 * x - a_2 * past_y[1] - a_1 * past_y[0];
    y = y / a_0;
    past_y[1] = past_y[0];
    past_y[0] = y;
    past_x[1] = past_x[0];
    past_x[0] = x;
    return y;
}

#endif

/* EOF */
