/* SF_Reciprocal class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_RECIPROCAL_HH__
#define __SF_RECIPROCAL_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Reciprocal : public SF_Block
{
public:
    SF_Reciprocal (const char* name = 0);
    virtual ~SF_Reciprocal ();
    virtual void finish ();
    virtual void initialize ();
    virtual void execute ();
	
protected:
    SF_Sample* s_y;
    SF_Sample* s_x;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Reciprocal (const SF_Reciprocal& a);
    SF_Reciprocal& operator= (const SF_Reciprocal& a);
};

#endif

/* EOF */
