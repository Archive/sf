/* SF_Adder class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_ADDER_HH__
#define __SF_ADDER_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Adder : public SF_Block
{
public:
    SF_Adder (const char* name = 0);
    virtual ~SF_Adder ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

    inline void		set_initial_value (SF_Sample a);
    inline SF_Sample	get_initial_value ();

protected:
    // Constant value which is added to the sum; 0 by default
    SF_Sample		initial_value;

    // Pointer to the signal in the frame of the output terminal "y"
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Adder (const SF_Adder& a);
    SF_Adder& operator= (const SF_Adder& a);
};

inline void
SF_Adder::set_initial_value (SF_Sample a)
{
    initial_value = a;
}

inline SF_Sample
SF_Adder::get_initial_value ()
{
    return initial_value;
}

#endif

/* EOF */
