/* SF_Attack_Release class declaration
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_ATTACK_RELEASE_HH__
#define __SF_ATTACK_RELEASE_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Attack_Release : public SF_Block
{
public:
    SF_Attack_Release (const char* name = 0);
    virtual ~SF_Attack_Release ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

protected:
    // this is y[n - 1] for each x[n] (saved between frames)
    SF_Sample		prev_y;
    // factor to transform milliseconds into samples
    SF_Coefficient	time_k;

    // Pointer to the signals in the frames of the input and output terminals
    SF_Sample*		s_x;
    SF_Sample*		s_attack;
    SF_Sample*		s_release;
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Input_Terminal*	in_attack;
    SF_Input_Terminal*	in_release;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Attack_Release (const SF_Attack_Release& a);
    SF_Attack_Release& operator= (const SF_Attack_Release& a);
};

#endif

/* EOF */
