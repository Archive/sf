/* SF_Max_Min class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_MAX_MIN_HH__
#define __SF_MAX_MIN_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

class SF_Max_Min : public SF_Block
{
public:
    SF_Max_Min (const char* name = 0);
    virtual ~SF_Max_Min ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

protected:
    inline SF_Sample max (SF_Sample a, SF_Sample b);
    inline SF_Sample min (SF_Sample a, SF_Sample b);

    SF_Sample* s_max;
    SF_Sample* s_min;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Output_Terminal*	out_max;
    SF_Output_Terminal*	out_min;

private:
    //Disable copy constructor and operator=
    SF_Max_Min (const SF_Max_Min& a);
    SF_Max_Min& operator= (const SF_Max_Min& a);
};

inline SF_Sample
SF_Max_Min::max (SF_Sample a, SF_Sample b)
{
	if (a > b) return a;
	else return b;
}
inline SF_Sample
SF_Max_Min::min (SF_Sample a, SF_Sample b)
{
	if (a < b) return a;
	else return b;
}

#endif

/* EOF */
