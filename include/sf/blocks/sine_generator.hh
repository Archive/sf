/* SF_Sine_Generator class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_SINE_GENERATOR_HH__
#define __SF_SINE_GENERATOR_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Sine_Generator : public SF_Block
{
public:
    SF_Sine_Generator (const char* name = 0);
    virtual ~SF_Sine_Generator ();
    virtual void finish ();
    virtual void initialize ();
    virtual void execute ();

protected:
    SF_Sample* s_y;
    SF_Sample* s_freq;
    SF_Sample* s_amp;
    SF_Frequency fs;
    double phi;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_freq;
    SF_Input_Terminal*	in_amp;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Sine_Generator (const SF_Sine_Generator& a);
    SF_Sine_Generator& operator= (const SF_Sine_Generator& a);
};

#endif

/* EOF */
