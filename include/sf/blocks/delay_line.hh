/* SF_Delay_Line class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_DELAY_LINE_HH__
#define __SF_DELAY_LINE_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_Delay_Line : public SF_Block
{
public:
    SF_Delay_Line (const char* name = 0);
    virtual ~SF_Delay_Line ();
    virtual void finish ();
    virtual void initialize ();
    virtual void execute ();

    inline void		set_length (SF_Time length);
    inline SF_Time	get_length () const;

protected:
    SF_Sample		interpolate_delay_line (SF_Time tap);

    SF_Sample		fractional_delay_filter_state;
    SF_Sample*		delay_line;
    SF_Time		delay_memory;
    unsigned int	delay_memory_length;
    unsigned int	current_index;

    SF_Frequency	fs;
    SF_Sample*		s_x;
    SF_Sample*		s_d;
    SF_Sample*		s_y;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
    SF_Input_Terminal*	in_d;
    SF_Output_Terminal*	out_y;

private:
    //Disable copy constructor and operator=
    SF_Delay_Line (const SF_Delay_Line& a);
    SF_Delay_Line& operator= (const SF_Delay_Line& a);
};

inline void
SF_Delay_Line::set_length (SF_Time length)
{
    delay_memory = length;
}

inline SF_Time
SF_Delay_Line::get_length () const
{
    return delay_memory;
}

#endif
/* EOF */
