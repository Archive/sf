/* SF_File_Output class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_FILE_OUTPUT_HH__
#define __SF_FILE_OUTPUT_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>

class SF_File_Output : public SF_Block
{
public:
    SF_File_Output (const char* name = 0);
    virtual ~SF_File_Output ();
    virtual void finish ();
    virtual void initialize ();
    virtual void execute ();

    const char*		get_file_name ();
    void		set_file_name (const char* file_name);

protected:
    const char*		file_name;
    int			fd;		// file descriptor (sndlib)
    int*		buffer;		// write buffer
    int			buflen;		// size of the above buffer
    int			samples_written;// the length of the output data
    bool		clipping_warning_printed;
  
    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_x;
  
private:
    //Disable copy constructor and operator=
    SF_File_Output (const SF_File_Output& a);
    SF_File_Output& operator= (const SF_File_Output& a);
};


#endif

/* EOF */
