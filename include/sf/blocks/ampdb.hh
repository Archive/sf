/* SF_Ampdb class declaration
   Copyright (C) 1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_AMPDB_HH__
#define __SF_AMPDB_HH__

#include <sf/block.hh>
#include <sf/input_terminal.hh>
#include <sf/output_terminal.hh>
#include <sf/typedefs.h>

class SF_Ampdb : public SF_Block
{
public:
    SF_Ampdb (const char* name = 0);
    virtual ~SF_Ampdb ();
    virtual void initialize ();
    virtual void execute ();
    virtual void finish ();

protected:
    // Pointer to the signal in the frame of the input and output terminals
    SF_Sample*		s_amp;
    SF_Sample*		s_db;

    // Pointers to the input and output terminals; the terminals are addressed
    // through these pointers in the execute () call instead of fetching the
    // pointers by name
    SF_Input_Terminal*	in_db;
    SF_Output_Terminal*	out_amp;

private:
    //Disable copy constructor and operator=
    SF_Ampdb (const SF_Ampdb& a);
    SF_Ampdb& operator= (const SF_Ampdb& a);
};

#endif

/* EOF */
