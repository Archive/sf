/* SF_List
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_LIST_HH__
#define __SF_LIST_HH__

//#include <sf/exception.hh>
class SF_Exception;
class SF_Indexing_Exception;
class SF_Memory_Exception;

class SF_List
{
public:
    SF_List () throw (SF_Memory_Exception);
    virtual ~SF_List () throw ();
    void		add (const void* content) throw (SF_Memory_Exception);
    void		remove (const void* content) throw (SF_Exception);
    bool		find (const void* content) throw ();
    void*		get_node (int index) throw (SF_Indexing_Exception);
    inline int		get_num_nodes () const throw ();
    inline bool		is_empty () const throw ();
    void		sort (int (*comparison_function) (const void* a, const void* b)) throw (SF_Exception);

private:
    //Disable copy constructor and operator=
    SF_List (const SF_List& a);
    SF_List& operator= (const SF_List& a);

    struct SF_List_Node
    {
	const void*	content;
	SF_List_Node*	next;
	SF_List_Node*	prev;
    };

    SF_List_Node*	search (const void* content) throw ();
    SF_List_Node*	nil;
    SF_List_Node*	current;
    int			current_index;
    int			num_nodes;
};

inline int
SF_List::get_num_nodes () const throw ()
    // Return the number of nodes currently on the list.

    // Doesn't throw exceptions.
{
    return num_nodes;
}

inline bool
SF_List::is_empty () const throw ()
    // Returns true if the list is empty and otherwise false.

    // Doesn't throw exceptions.
{
    return (num_nodes == 0);
}

#endif

/* EOF */
