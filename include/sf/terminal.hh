/* SF_Terminal class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_TERMINAL_HH__
#define __SF_TERMINAL_HH__

#include <sf/defaults.hh>
#include <sf/exception.hh>
#include <sf/list.hh>
#include <sf/queue.hh>
#include <sf/typedefs.h>

// In order to prevent an endless loop of `#include's, `block.hh' is NOT
// included.  Instead, a pre-declaration is done:
class SF_Block;

class SF_Terminal
{
public:
    SF_Terminal (const char* name,
		 const SF_Block* host_block,
		 SF_Frequency sample_rate,
		 double relative_sample_rate)
	throw (SF_Memory_Exception, SF_Terminal_Exception);
    virtual ~SF_Terminal () throw ();

    virtual void	set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception);
    virtual void	set_relative_sample_rate (double relative_sample_rate) throw (SF_Exception);
    virtual inline double	get_relative_sample_rate () const throw ();
    inline int		get_num_wires () const throw (); // degree
    inline const char*	get_name () const throw ();
    inline SF_Block*	get_host_block () const throw ();
    inline SF_Frequency get_sample_rate () const throw ();
    inline bool		is_sample_rate_imbalance () const throw ();

protected:
    virtual SF_Terminal*get_wired_terminal (int index) throw (SF_Indexing_Exception);
    inline void		set_sample_rate_imbalance (bool imbalance) throw ();
    virtual void	propagate_wire_sample_rate (SF_Frequency sample_rate,
						    SF_Queue& traversal_q,
						    SF_Queue& cleanup_q) throw ();
    void		update_imbalance_flags (const SF_Block& b) throw ();
    virtual void	only_set_sample_rate (SF_Frequency sample_rate) throw (SF_Exception);
    virtual void	only_set_relative_sample_rate (double relative_sample_rate) throw (SF_Exception);

    // name of this terminal; must be unique within the block
    const char*		name;
    // the block this terminal belongs to; 0 implies that this terminal is a
    // bubble
    const SF_Block*	host_block;
    // the sample rate of the data passing through this terminal
    SF_Frequency	sample_rate;
    // the relation of the sample rate of this terminal to the sample rates of
    // all the other terminals in the block
    double		relative_sample_rate;
    // true or false depending on whether the sample_rate is according to the
    // relative_sample_rate with respect to all the sample rates and relative
    // sample rates of all the other terminals in the block
    bool		sample_rate_imbalance;
    // list of pointers to terminals to which we are connected and from/to which
    // to get/feed data
    SF_List*		wires;
    // A flag needed in the propagation of sample rates (with the DFS
    // algorithm).
    bool		propagation_visited;

private:
    //Disable copy constructor and operator=
    SF_Terminal (const SF_Terminal& a);
    SF_Terminal& operator= (const SF_Terminal& a);
};

inline int
SF_Terminal::get_num_wires () const throw ()
    // Returns the number of connections, i.e. the degree, of this terminal.

    // Doesn't throw exceptions.
{
    return wires->get_num_nodes ();
}

inline const char* 
SF_Terminal::get_name () const throw ()
{
    return name;
}

inline SF_Block* 
SF_Terminal::get_host_block () const throw ()
{
    return (SF_Block*) host_block;
}

inline SF_Frequency
SF_Terminal::get_sample_rate () const throw ()
{
    return sample_rate;
}

inline double
SF_Terminal::get_relative_sample_rate () const throw ()
{
    return relative_sample_rate;
}

inline bool
SF_Terminal::is_sample_rate_imbalance () const throw ()
{
    return sample_rate_imbalance;
}

inline void
SF_Terminal::set_sample_rate_imbalance (bool imbalance) throw ()
{
    sample_rate_imbalance = imbalance;
}

#endif

/* EOF */
