/* SF_Queue class declaration
   Copyright (C) 1998--1999 Jarno Sepp�nen and Sami Kananoja
   $Id$

   This file is part of Sonic Flow.

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */

#ifndef __SF_QUEUE_HH__
#define __SF_QUEUE_HH__

//#include <sf/exception.hh>
class SF_Exception;
class SF_Memory_Exception;

class SF_Queue
{
public:
    SF_Queue () throw ();
    ~SF_Queue () throw ();
    void		enqueue (const void* content) throw (SF_Memory_Exception);
    void*		dequeue () throw (SF_Exception);
    inline bool		is_empty () const throw ();

private:
    //Disable copy constructor and operator=
    SF_Queue (const SF_Queue& a);
    SF_Queue& operator= (const SF_Queue& a);

    struct SF_Queue_Node
    {
	const void*	content;
	SF_Queue_Node*	next;
    };

    SF_Queue_Node*	head;
    SF_Queue_Node*	tail;
};

inline bool
SF_Queue::is_empty () const throw ()
    //Return true if queue is empty, false otherwise.
{
    return (head == 0);
}

#endif
/* EOF */
